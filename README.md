Jiffy
=====

A JPEG (JFIF) encoder/decoder and accelerator.

This is Trey Boehm and Jordan Pamatmat's final project for EE 382N-4 (Advanced
Embedded MCU Systems) with Dr. Mark McDermott, spring 2021.

Organization
------------

Currently, we have everything we need to build the application software in the
Git repo. We still need to add the relevant Vivado files.

```
.
|-- .githooks               GIT HOOKS (pre-commit auto-format)
|-- hw                      HARDWARE
|   |-- misc                    Python notebooks
|   |-- src                     Verilog source
|   `-- tests                   Test benches for the accelerator
|       |-- jiffy_interface         Test bench for the AXI interface
|       `-- verilator               Verilator test benches for various modules
|-- scripts                 SCRIPTS (Auto-expand/format for Verilog)
`-- sw                      SOFTWARE
    |-- bmps                    Example BMP images
    |-- images                  Example JPG images
    |-- kmod                    Kernel modules
    |   |-- cdma                    CDMA kernel module
    |   `-- jiffy                   Jiffy kernel module
    |-- src                     Application source
    `-- tests                   Application tests
        |-- data                    Experimental data, plotting script, and plots
        |-- scripts                 Script for generating MCUs and profiling
        `-- src                     Test source code
```

Building
--------

First, check the `USE_ACCELERATOR` variable at the top of the Makefile. If you
are building this on an Ultra96 board and plan to use the accelerator, make sure
it is set to 1. If building elsewhere (e.g. for software development), set to 0.

**Note**: there are still bugs in the interface between the hardware and
software, so we do not receive the expected image data from the accelerator.
However, we are quite confident that the accelerator is still doing the work
internally, and present timing results that we believe reflect the amount of time
that execution with the accelerator would take to *decode* data. For encoding,
the Huffman step follows the math portion of the algorithm, and the amount of
time it takes is highly dependent on the contents of the block it receives.

To build the software, run `make -s all` in the `sw` directory. This will
build the application binary (`sw/jiffy`) and the tests. If `USE_ACCELERATOR` is
set to 1, it will also build and load the two kernel modules (`cdma_int.ko` and
`jiffy_int.ko`). The load step runs a few commands with `sudo`.

To change the logging level, modify the `LOGGING_LEVEL` variable at the top of
the Makefile and recompile with `make -Bs`. The levels are:

```
0. No logging information.
1. Fatal errors only.
2. Warnings.
3. Profiling information (times).
4. General info messages.
5. Debug messages.
6. Highly verbose debug messages.
```

Usage
-----

All of the following commands assume you are in the `sw` directory.

To decode an image, use the `-d` (or `--decode`) flag:

```bash
./jiffy -d images/swiz128.jpg decoded.bmp
```

And to encode an image, just use the `-e` (or `--encode`) flag:

```bash
./jiffy -e bmps/swiz128.bmp encoded.jpg
```

If `jiffy` does not receive an output file name, it will simply change the file
extension from .bmp to .jpg (or vice versa) and place the output in the same
directory as the input.

Limitations
-----------

If you would like to use your own JPEGs, you will quickly run into Jiffy's
limitations.

1. Subsampling. The sampling ratio must be 1 for all channels.
2. Extra application headers. Jiffy's JFIF parser is limited and only expects an
   APP0 header.
3. Thumbnails. I have not tested Jiffy on images with thumbnails, but if it sees
   that an image contains thumbnails it should print an error message and exit.
4. Dimensions. Jiffy does not support images whose dimensions are not multiples
   of 8. Allowing this is a matter of adding padding around the image.

We set these limitations because we wanted to limit the scope of our project to:
(1) creating a JPEG implementation from scratch, and (2) accelerating the math
involved with JPEG encoding/decoding. Omitting parts of the standard is what
allowed us to *nearly* achieve this goal in the time we had.

The best way to supply your own image is to start with a PNG. JPEGs that were
not created at 100% quality tend to use subsampling, which we do not support.
Once you have the image, determine the number of pixels in both dimensions.
Then, round these down to the nearest multiples of 8. Call these N and M.

```bash
convert -resize NxM -quality 100 input.png output.jpg
mogrify -strip output.jpg
```

The first command removes the need to pad the edges of the image and ensures
there will not be any subsampling in the output. The second command strips
unnecessary headers so the JFIF parser can handle it.

For BMPs, I actually recommend you just start with a different format, convert
to JPEG, then use Jiffy to convert it to a BMP. There doesn't seem to be a
single ImageMagick command that always creates a workable BMP. Again, the focus
of the project was on hardware acceleration and correctness, not complete
standard compliance :)

Git Hooks
---------

To enable the pre-commit hook, execute the following:

```bash
mkdir -p .git/hooks && pushd .git/hooks && ln -s ../../.githooks/pre-commit . && popd
```
