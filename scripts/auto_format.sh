#!/usr/bin/env bash

# auto_format.sh: Format and expand autos in Verilog files.
#
# Opens a set of files in emacs and executes the auto-format-verilog function,
# which is defined in auto_format_verilog.el.
#
# Copyright (c) 2021 Trey Boehm & Jordan Pamatmat
#
# This file is part of the JFIF encoder/decoder for EE 382N-4 at UT Austin
# with Dr. Mark McDermott.

this_file=$(realpath "$0")
this_dir=$(dirname "$this_file")
format_script=$this_dir/auto_format_verilog.el

if [ $# -eq 0 ]; then
   printf '%b' "$0 requires at least one argument.\n" 1>&2
   printf '%b' "Usage: $0 <files-to-indent>\n" 1>&2
   exit 1
fi

while [ $# -ge 1 ]; do
   # Check for existence of file:
   if ! [ -f "$1" ]; then
       printf '%b' "File '$1' does not exist.\n" 1>&2
       exit 1
   fi

   printf '%b' "auto-format-verilog $1... "
   # Remove the '2> /dev/null' if the script doesn't seem to be working
   if ! emacs --batch "$1" -l "$format_script" -f auto-format-verilog 2> /dev/null; then
       printf '%b' "\033[31;1mfailed.\033[0m\n"
       exit 1
   else
       printf '%b' "done.\n"
   fi

   shift 1
done

exit 0
