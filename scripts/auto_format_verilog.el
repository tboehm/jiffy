;;; Package -- Format Verilog files for EE 382N-4.
;;; Commentary:
;;;  - Relies on verilog-mode.

(require 'verilog-mode)

;;; Code:
(defun auto-format-verilog ()
  "Format a Verilog buffer and expand autos."
  (setq verilog-indent-level 4
        verilog-indent-level-behavioral 4
        verilog-indent-level-declaration 4
        verilog-indent-level-module 4
        verilog-case-indent 4
        verilog-cexp-indent 4
        verilog-align-ifelse t
        verilog-auto-delete-trailing-whitespace t
        verilog-auto-inst-param-value t
        verilog-auto-inst-vector nil
        verilog-auto-lineup (quote all)
        verilog-auto-newline nil
        verilog-auto-save-policy nil
        verilog-auto-template-warn-unused t
        verilog-highlight-grouping-keywords t
        verilog-highlight-modules t
        verilog-tab-to-comment t)
  (verilog-indent-buffer)
  (verilog-auto)
  (untabify (point-min) (point-max))
  (save-buffer))

;;; auto_format_verilog.el ends here
