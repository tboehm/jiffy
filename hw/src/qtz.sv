// qtz.v: Quantization
//
// Copyright (c) 2021 Trey Boehm & Jordan Pamatmat
//
// This file is part of the JFIF encoder/decoder for EE 382N-4 at UT Austin
// with Dr. Mark McDermott.

`include "configuration.vh"

module qtz_encode
    (
     // Outputs
     output reg [`QTZ_CHANNEL_RNG] qtz_omem_y_channel [`PIXEL_BUS_RNG],
     output reg [`QTZ_CHANNEL_RNG] qtz_omem_cb_channel [`PIXEL_BUS_RNG],
     output reg [`QTZ_CHANNEL_RNG] qtz_omem_cr_channel [`PIXEL_BUS_RNG],
     output reg out_ready,

     // Inputs
     input wire [`DCT_CHANNEL_RNG] dct_qtz_y_channel [`PIXEL_BUS_RNG],
     input wire [`DCT_CHANNEL_RNG] dct_qtz_cb_channel [`PIXEL_BUS_RNG],
     input wire [`DCT_CHANNEL_RNG] dct_qtz_cr_channel [`PIXEL_BUS_RNG],
     input wire [`QTZ_MTX_RNG]     qtz_mtx_y  [`PIXEL_BUS_RNG][`PIXEL_BUS_RNG],
     input wire [`QTZ_MTX_RNG]     qtz_mtx_cb [`PIXEL_BUS_RNG][`PIXEL_BUS_RNG],
     input wire [`QTZ_MTX_RNG]     qtz_mtx_cr [`PIXEL_BUS_RNG][`PIXEL_BUS_RNG],
     input wire                    start,
     input wire                    clk,
     input wire                    rst
     );

genvar i;
integer pxl;

reg [3:0] row_sel;

// Input channel: 1 sign bit, 12 integer bits, 8 fraction bits
// Quantinization matrix inputs are shifted up by 8 bits.
// Output channel shift down 16 (8 for the down shift, 8 to round out the fraction bits)
wire [`QTZ_CHANNEL_RNG] qtz_Y [`PIXEL_BUS_RNG];
wire [`QTZ_CHANNEL_RNG] qtz_Cb [`PIXEL_BUS_RNG];
wire [`QTZ_CHANNEL_RNG] qtz_Cr [`PIXEL_BUS_RNG];

for(i = 0; i < `PIXEL_BUS_CNT; i = i + 1) begin
    assign qtz_Y[i]  = row_sel[3] ? 0 :(dct_qtz_y_channel[i] * qtz_mtx_y[row_sel][i]) >>> 16;
    assign qtz_Cb[i] = row_sel[3] ? 0 :(dct_qtz_cb_channel[i] * qtz_mtx_cb[row_sel][i]) >>> 16;
    assign qtz_Cr[i] = row_sel[3] ? 0 :(dct_qtz_cr_channel[i] * qtz_mtx_cr[row_sel][i]) >>> 16;
end


always @(posedge clk) begin
    if (!rst) begin
        for(pxl = 0; pxl < `PIXEL_BUS_CNT; pxl = pxl + 1) begin
            qtz_omem_y_channel[pxl] <= 0;
            qtz_omem_cb_channel[pxl] <= 0;
            qtz_omem_cr_channel[pxl] <= 0;
        end
        row_sel <= 0;
        out_ready <= 1'b0;
    end 
    else begin
        if(start & !row_sel[3]) begin
            for(pxl = 0; pxl < `PIXEL_BUS_CNT; pxl = pxl + 1) begin
                qtz_omem_y_channel[pxl]  <= qtz_Y[pxl];
                qtz_omem_cb_channel[pxl] <= qtz_Cb[pxl];
                qtz_omem_cr_channel[pxl] <= qtz_Cr[pxl];
            end 
            row_sel <= row_sel + 1;
            out_ready <= 1'b1;
        end
        else begin
            row_sel <= 0;
            out_ready <= 1'b0;
        end
    end
end

endmodule // qtz


module qtz_decode
    (
     // Outputs
    output reg [`DCT_CHANNEL_RNG] qtz_dct_y_channel  [`PIXEL_BUS_RNG],
    output reg [`DCT_CHANNEL_RNG] qtz_dct_cb_channel [`PIXEL_BUS_RNG],
    output reg [`DCT_CHANNEL_RNG] qtz_dct_cr_channel [`PIXEL_BUS_RNG],
    output reg out_ready,

     // Inputs
    input wire [`QTZ_CHANNEL_RNG] imem_qtz_y_channel [`PIXEL_BUS_RNG],
    input wire [`QTZ_CHANNEL_RNG] imem_qtz_cb_channel [`PIXEL_BUS_RNG],
    input wire [`QTZ_CHANNEL_RNG] imem_qtz_cr_channel [`PIXEL_BUS_RNG],
    input wire [`QTZ_MTX_RNG]     qtz_mtx_y  [`PIXEL_BUS_RNG][`PIXEL_BUS_RNG],
    input wire [`QTZ_MTX_RNG]     qtz_mtx_cb [`PIXEL_BUS_RNG][`PIXEL_BUS_RNG],
    input wire [`QTZ_MTX_RNG]     qtz_mtx_cr [`PIXEL_BUS_RNG][`PIXEL_BUS_RNG],
    input wire                    start,
    input wire                    clk,
    input wire                    rst
    );

wire signed [`DCT_CHANNEL_RNG] qtz_Y [`PIXEL_BUS_RNG];
wire signed [`DCT_CHANNEL_RNG] qtz_Cb [`PIXEL_BUS_RNG];
wire signed [`DCT_CHANNEL_RNG] qtz_Cr [`PIXEL_BUS_RNG];

genvar i;
integer pxl;

reg [3:0] row_sel;

for(i = 0; i < `PIXEL_BUS_CNT; i = i + 1) begin
    assign qtz_Y[i]  = row_sel[3] ? 0 :signed'(imem_qtz_y_channel[i] * qtz_mtx_y[row_sel][i]);
    assign qtz_Cb[i] = row_sel[3] ? 0 :signed'(imem_qtz_cb_channel[i] * qtz_mtx_cb[row_sel][i]);
    assign qtz_Cr[i] = row_sel[3] ? 0 :signed'(imem_qtz_cr_channel[i] * qtz_mtx_cr[row_sel][i]);
end

always @(posedge clk) begin
    if (!rst) begin
        for(pxl = 0; pxl < `PIXEL_BUS_CNT; pxl = pxl + 1) begin
            qtz_dct_y_channel[pxl] <= 0;
            qtz_dct_cb_channel[pxl] <= 0;
            qtz_dct_cr_channel[pxl] <= 0;
        end
        row_sel <= 0;
        out_ready <= 1'b0;
    end 
    else begin
        if(start & !row_sel[3]) begin
            for(pxl = 0; pxl < `PIXEL_BUS_CNT; pxl = pxl + 1) begin
                qtz_dct_y_channel[pxl]  <= qtz_Y[pxl] << 8;
                qtz_dct_cb_channel[pxl] <= qtz_Cb[pxl] << 8;
                qtz_dct_cr_channel[pxl] <= qtz_Cr[pxl] << 8;
            end 
            row_sel <= row_sel + 1;
            out_ready <= 1'b1;
        end
        else begin
            row_sel <= 0;
            out_ready <= 1'b0;
        end
    end
end


endmodule