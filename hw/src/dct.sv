// dct.v: Discrete Cosine Transform
//
// Copyright (c) 2021 Trey Boehm & Jordan Pamatmat
//
// This file is part of the JFIF encoder/decoder for EE 382N-4 at UT Austin
// with Dr. Mark McDermott.

`include "configuration.vh"

module dct
    (
     // Outputs
     output reg [`DCT_CHANNEL_RNG] dct_out_y_channel [`PIXEL_BUS_RNG],
     output reg [`DCT_CHANNEL_RNG] dct_out_cb_channel[`PIXEL_BUS_RNG],
     output reg [`DCT_CHANNEL_RNG] dct_out_cr_channel[`PIXEL_BUS_RNG],
     output reg out_ready,

     // Inputs
     input wire [`DCT_CHANNEL_RNG] dct_in_y_channel [`PIXEL_BUS_RNG],
     input wire [`DCT_CHANNEL_RNG] dct_in_cb_channel[`PIXEL_BUS_RNG],
     input wire [`DCT_CHANNEL_RNG] dct_in_cr_channel[`PIXEL_BUS_RNG],
     input wire                  code_mode,
     input wire                  start,
     input wire                  clk,
     input wire                  rst
     );

reg [`DCT_CHANNEL_RNG] y_reg  [`PIXEL_BUS_RNG][`PIXEL_BUS_RNG];
reg [`DCT_CHANNEL_RNG] cb_reg [`PIXEL_BUS_RNG][`PIXEL_BUS_RNG];
reg [`DCT_CHANNEL_RNG] cr_reg [`PIXEL_BUS_RNG][`PIXEL_BUS_RNG];

reg dct_start;

reg [`DCT_CHANNEL_RNG] dct_in_channel [`PIXEL_BUS_RNG];
wire [`DCT_CHANNEL_RNG] dct_out_channel [`PIXEL_BUS_RNG];
wire dct_out_ready;

reg [4:0] dct_sm;
reg [2:0] row_sel;

integer i;

always@(posedge clk) begin
    if(!rst) begin
        for(i = 0; i < `PIXEL_BUS_CNT; i = i + 1) begin
            y_reg[i]  <= '{0, 0, 0, 0, 0, 0, 0, 0};
            cb_reg[i] <= '{0, 0, 0, 0, 0, 0, 0, 0};
            cr_reg[i] <= '{0, 0, 0, 0, 0, 0, 0, 0};
        end                
        dct_in_channel <= '{0, 0, 0, 0, 0, 0, 0, 0};
        dct_out_y_channel  <= '{0, 0, 0, 0, 0, 0, 0, 0};
        dct_out_cb_channel <= '{0, 0, 0, 0, 0, 0, 0, 0};
        dct_out_cr_channel <= '{0, 0, 0, 0, 0, 0, 0, 0};
        dct_sm <= 0;
        out_ready <= 0;
        dct_start <= 0;
    end
    else begin
       case (dct_sm)
            0: begin
                for(i = 0; i < `PIXEL_BUS_CNT; i = i + 1) begin
                    y_reg[i]  <= '{0, 0, 0, 0, 0, 0, 0, 0};
                    cb_reg[i] <= '{0, 0, 0, 0, 0, 0, 0, 0};
                    cr_reg[i] <= '{0, 0, 0, 0, 0, 0, 0, 0};
                end                
                dct_in_channel <= '{0, 0, 0, 0, 0, 0, 0, 0};
                dct_out_y_channel  <= '{0, 0, 0, 0, 0, 0, 0, 0};
                dct_out_cb_channel <= '{0, 0, 0, 0, 0, 0, 0, 0};
                dct_out_cr_channel <= '{0, 0, 0, 0, 0, 0, 0, 0};
                dct_start <= 0;
                out_ready <= 0;
                row_sel <= 0;
                dct_sm <= 1;
            end 
            1: begin
                dct_start <= start;
                dct_in_channel <= start ? dct_in_y_channel : dct_in_channel;
                cb_reg[row_sel] <= start ? dct_in_cb_channel : cb_reg[row_sel];
                cr_reg[row_sel] <= start ? dct_in_cr_channel : cr_reg[row_sel];
                row_sel <= start ? 1 : 0;
                dct_sm <= 2;
            end
            2: begin
                dct_start <= 1;
                dct_in_channel <= dct_in_y_channel;
                cb_reg[row_sel] <= dct_in_cb_channel;
                cr_reg[row_sel] <= dct_in_cr_channel;
                row_sel <= row_sel + 1;
                dct_sm <= (row_sel == 3'b111) ? 3 : 2;
            end
            3: begin
                dct_in_channel <= '{0, 0, 0, 0, 0, 0, 0, 0};
                row_sel <= 0;
                dct_sm <= 4;
            end
            4: begin
                row_sel <= dct_out_ready ? 1 : 0;
                y_reg[row_sel] <= dct_out_ready ? dct_out_channel : y_reg[row_sel];
                dct_sm <= dct_out_ready ? 5 : 4;
            end
            5: begin
                dct_start <= 0;
                y_reg[row_sel] <= dct_out_channel;
                row_sel <= row_sel + 1;
                dct_sm <= (row_sel == 3'b111) ? 6 : 5;
            end
            6: begin
                row_sel <= 0;
                dct_sm <= dct_out_ready ? 6 : 7;
            end
            7: begin
                dct_start <= 1;
                dct_in_channel <= cb_reg[row_sel];
                row_sel <= row_sel + 1;
                dct_sm <= (row_sel == 3'b111) ? 8 : 7;
            end
            8: begin
                dct_in_channel <= '{0, 0, 0, 0, 0, 0, 0, 0};
                row_sel <= 0;
                dct_sm <= 9;
            end
            9: begin
                row_sel <= dct_out_ready ? 1 : 0;
                cb_reg[row_sel] <= dct_out_ready ? dct_out_channel : cb_reg[row_sel];
                dct_sm <= dct_out_ready ? 10 : 9;                
            end
            10: begin
                dct_start <= 0;
                cb_reg[row_sel] <= dct_out_channel;
                row_sel <= row_sel + 1;
                dct_sm <= (row_sel == 3'b111) ? 11 : 10;
            end
            11: begin
                row_sel <= 0;
                dct_sm <= dct_out_ready ? 11 : 12;
            end
            12: begin
                dct_start <= 1;
                dct_in_channel <= cr_reg[row_sel];
                row_sel <= row_sel + 1;
                dct_sm <= row_sel == 3'b111 ? 13 : 12;
            end
            13: begin
                dct_in_channel <= '{0, 0, 0, 0, 0, 0, 0, 0};
                row_sel <= 0;
                dct_sm <= 14;
            end
            14: begin
                row_sel <= dct_out_ready ? 1 : 0;
                cr_reg[row_sel] <= dct_out_ready ? dct_out_channel : cr_reg[row_sel];
                dct_sm <= dct_out_ready ? 15 : 14;                
            end
            15: begin
                dct_start <= 0;
                cr_reg[row_sel] <= dct_out_channel;
                row_sel <= row_sel + 1;
                dct_sm <= (row_sel == 3'b111) ? 16 : 15;
            end
            16: begin
                row_sel <= 0;
                dct_sm <= dct_out_ready ? 16: 17;
            end
            17: begin
                out_ready <= 1;
                row_sel <= row_sel + 1;
                dct_out_y_channel <= y_reg[row_sel];
                dct_out_cb_channel <= cb_reg[row_sel];
                dct_out_cr_channel <= cr_reg[row_sel];
                dct_sm <= (row_sel == 3'b111) ? 18 : 17;
            end
            18: begin
                out_ready <= 0;
                row_sel <= 0;
                dct_sm <= 0;
            end
       endcase 
    end
end

dct_sa dct_sa0(
    .out_channel(dct_out_channel),
    .out_ready(dct_out_ready),
    .in_channel(dct_in_channel),
    .code_mode(code_mode),
    .start(dct_start),
    .clk(clk),
    .rst(rst)
);

endmodule // dct
