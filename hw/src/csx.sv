// csx.v: Colorspace transformation
//
// Copyright (c) 2021 Trey Boehm & Jordan Pamatmat
//
// This file is part of the JFIF encoder/decoder for EE 382N-4 at UT Austin
// with Dr. Mark McDermott.

`include "configuration.vh"

module csx_encode
    (
     // Outputs
     output reg [`YCBCR_CHANNEL_RNG] csx_dct_y_channel [`PIXEL_BUS_RNG],
     output reg [`YCBCR_CHANNEL_RNG] csx_dct_cb_channel [`PIXEL_BUS_RNG],
     output reg [`YCBCR_CHANNEL_RNG] csx_dct_cr_channel [`PIXEL_BUS_RNG],
     output reg out_ready,

     // Inputs
     input [`RGB_CHANNEL_RNG]        imem_csx_r_channel [`PIXEL_BUS_RNG],
     input [`RGB_CHANNEL_RNG]        imem_csx_g_channel [`PIXEL_BUS_RNG],
     input [`RGB_CHANNEL_RNG]        imem_csx_b_channel [`PIXEL_BUS_RNG],
     input                           start,
     input                           clk,
     input                           rst
     );

// Y =  (0.299*R+(0.587*G)+(0.114*B)
// Cb = (-0.1687*R)-(0.3313*G)+(0.5*B)+128
// Cr = (0.5*R)-(0.4187*G)-(0.0813*B)+128

genvar i;
integer pxl;
wire [22:0] csx_Y  [`PIXEL_BUS_RNG];
wire [22:0] csx_Cb [`PIXEL_BUS_RNG];
wire [22:0] csx_Cr [`PIXEL_BUS_RNG];

// Shift everything up by 14 bits. Do Arithmetic. Shift down by 14 bits.

// Coefficients Shifted up by 14 bits (multipled by 16384)
wire [14:0] Y_R;
wire [14:0] Y_G;
wire [14:0] Y_B;

wire [14:0] Cb_R;
wire [14:0] Cb_G;
wire [14:0] Cb_B;

wire [14:0] Cr_R;
wire [14:0] Cr_G;
wire [14:0] Cr_B;

assign  Y_R = 15'h1323;
assign  Y_G = 15'h2591;
assign  Y_B = 15'h074C;

assign Cb_R = 15'h0ACC;
assign Cb_G = 15'h1534;
assign Cb_B = 15'h2000;

assign Cr_R = 15'h2000;
assign Cr_G = 15'h1ACC;
assign Cr_B = 15'h0534;

for(i = 0; i < `PIXEL_BUS_CNT; i=i+1) begin
    assign csx_Y[i]  = (Y_R * imem_csx_r_channel[i] + Y_G * imem_csx_g_channel[i] + Y_B * imem_csx_b_channel[i]) >> 14;
    assign csx_Cb[i] = ((128 << 14) - Cb_R * imem_csx_r_channel[i] - Cb_G * imem_csx_g_channel[i] + Cb_B * imem_csx_b_channel[i]) >> 14;
    assign csx_Cr[i] = ((128 << 14) + Cr_R * imem_csx_r_channel[i] - Cr_G * imem_csx_g_channel[i] - Cr_B * imem_csx_b_channel[i]) >> 14;
end

always@(posedge clk) begin
    if (!rst) begin
        for(pxl = 0; pxl < `PIXEL_BUS_CNT; pxl = pxl + 1) begin
            csx_dct_y_channel[pxl] <= 0;
            csx_dct_cb_channel[pxl] <= 0;
            csx_dct_cr_channel[pxl] <= 0;
        end
        out_ready <= 1'b0;
    end
    else begin
        if(start) begin
            for(pxl = 0; pxl < `PIXEL_BUS_CNT; pxl = pxl + 1) begin
                csx_dct_y_channel[pxl] <= csx_Y[pxl];
                csx_dct_cb_channel[pxl] <= csx_Cb[pxl];
                csx_dct_cr_channel[pxl] <= csx_Cr[pxl];
            end
            out_ready <= 1'b1;
        end
        else begin
            out_ready <= 1'b0;
        end
    end
end

endmodule // csx

module csx_decode 
     (
     // Outputs    
     output reg [`RGB_CHANNEL_RNG] csx_omem_r_channel [`PIXEL_BUS_RNG],
     output reg [`RGB_CHANNEL_RNG] csx_omem_g_channel [`PIXEL_BUS_RNG],
     output reg [`RGB_CHANNEL_RNG] csx_omem_b_channel [`PIXEL_BUS_RNG],
     output reg out_ready,

     // Inputs
     input wire [`YCBCR_CHANNEL_RNG] dct_csx_y_channel [`PIXEL_BUS_RNG],
     input wire [`YCBCR_CHANNEL_RNG] dct_csx_cb_channel [`PIXEL_BUS_RNG],
     input wire [`YCBCR_CHANNEL_RNG] dct_csx_cr_channel [`PIXEL_BUS_RNG],
     input wire start,
     input wire clk,
     input wire rst
     );

// r = y + 1.402f * (cr - 128);
// g = y - 0.344136f * (cb - 128) - 0.714136f * (cr - 128);
// b = y + 1.772f * (cb - 128);

genvar i;
integer pxl;
wire [9:0] csx_R [`PIXEL_BUS_RNG];
wire [9:0] csx_G [`PIXEL_BUS_RNG];
wire [9:0] csx_B [`PIXEL_BUS_RNG];

// Shift everything up by 14 bits. Do Arithmetic. Shift down by 14 bits.

// Coefficients Shifted up by 14 bits (multipled by 16384)
wire [15:0] R_Cr;
wire [15:0] G_Cb;
wire [15:0] G_Cr;
wire [15:0] B_Cb;

assign R_Cr = 16'h59BA;
assign G_Cb = 16'h1606;
assign G_Cr = 16'h2DB4;
assign B_Cb = 16'h7168;

for(i = 0; i < `PIXEL_BUS_CNT; i=i+1) begin
    assign csx_R[i] = 24'(( (dct_csx_y_channel[i] << 14) + R_Cr * (dct_csx_cr_channel[i] - 128) )) >> 14;
    assign csx_G[i] = 24'(( (dct_csx_y_channel[i] << 14) - G_Cb * (dct_csx_cb_channel[i] - 128) - G_Cr * (dct_csx_cr_channel[i] - 128) )) >> 14;
    assign csx_B[i] = 24'(( (dct_csx_y_channel[i] << 14) + B_Cb * (dct_csx_cb_channel[i] - 128) )) >> 14;
end

always@(posedge clk) begin
    if (!rst) begin
        for(pxl = 0; pxl < `PIXEL_BUS_CNT; pxl = pxl + 1) begin
            csx_omem_r_channel[pxl] <= 0;
            csx_omem_g_channel[pxl] <= 0;
            csx_omem_b_channel[pxl] <= 0;
        end
        out_ready <= 1'b0;
    end
    else begin
        if(start) begin
            for(pxl = 0; pxl < `PIXEL_BUS_CNT; pxl = pxl + 1) begin
                csx_omem_r_channel[pxl] <= (csx_R[pxl][9] == 1'b1) ? 0 : csx_R[pxl] > 24'h0FF ? 8'hFF : csx_R[pxl];
                csx_omem_g_channel[pxl] <= (csx_G[pxl][9] == 1'b1) ? 0 : csx_G[pxl] > 24'h0FF ? 8'hFF : csx_G[pxl];
                csx_omem_b_channel[pxl] <= (csx_B[pxl][9] == 1'b1) ? 0 : csx_B[pxl] > 24'h0FF ? 8'hFF : csx_B[pxl];
            end
            out_ready <= 1'b1;
        end
        else begin
            out_ready <= 1'b0;
        end
    end
end


endmodule
