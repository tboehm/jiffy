`include "configuration.vh"

module dct_sa(
    output reg [`DCT_CHANNEL_RNG] out_channel [`PIXEL_BUS_RNG],
    output reg out_ready,

    input wire [`DCT_CHANNEL_RNG] in_channel [`PIXEL_BUS_RNG],
    input wire code_mode,
    input wire start,
    input wire clk,
    input wire rst
);

integer i, j;
wire sm_start;
reg [1:0] sm_pulse;
reg [3:0] in_ram_row_sel;
reg [3:0] out_ram_row_sel;
reg out_ram_ready;

// RAM to hold DCT input/output values
reg [`DCT_CHANNEL_RNG] in_RAM [`PIXEL_BUS_RNG][`PIXEL_BUS_RNG];
reg [`DCT_CHANNEL_RNG] out_RAM [`PIXEL_BUS_RNG][`PIXEL_BUS_RNG];

// input buffer to systolic array
reg [`DCT_CHANNEL_RNG] Vin [`PIXEL_BUS_RNG];
reg [`DCT_CHANNEL_RNG] Hin [`PIXEL_BUS_RNG];
reg [`DCT_CHANNEL_RNG] Vout [`PIXEL_BUS_RNG];

reg [`PIXEL_BUS_RNG] reg_mode [`PIXEL_BUS_RNG];
reg [5:0] dct_sm;

wire [`DCT_CHANNEL_RNG] COEF_MTX [`PIXEL_BUS_RNG][`PIXEL_BUS_RNG];

wire dct_sa_clear_l = rst && start;
// wire dct_sa_clear_l = !(rst && start);
// wire dct_sa_clear_l = rst;

coef_mat coef_inst(
    .COEF_MTX(COEF_MTX),
    .code_mode(code_mode)
);

sys_array sa_inst(
    .Vout(Vout),
    .Hin(Hin),
    .Vin(Vin),
    .reg_mode(reg_mode),
    .clk(clk),
    .rst(dct_sa_clear_l)
);

localparam
    INIT = 0,
    S0 = 1,
    S1 = 2,
    S2 = 3,
    S3 = 4,
    S4 = 5,
    S5 = 6,
    S6 = 7,
    S7 = 8,
    S8 = 9,
    S9 = 10,
    S10 = 11,
    S11 = 12,
    S12 = 13,
    S13 = 14,
    S14 = 15,
    S15 = 16,
    S16 = 17,
    S17 = 18,
    S18 = 19,
    S19 = 20,
    S20 = 21,
    S21 = 22,
    S22 = 23,
    S23 = 24,
    S24 = 25,
    S25 = 26,
    S26 = 27,
    S27 = 28,
    S28 = 29,
    S29 = 30,
    S30 = 31,
    S31 = 32,
    S32 = 33
;

    localparam
        SM_PULSE_RESET = 0,
        SM_PULSE_START = 1,
        SM_PULSE_WAIT  = 2;

    assign sm_start = sm_pulse == SM_PULSE_START;

    // Control the start of the state machine (convert `start` to a pulse)
    always @(posedge clk) begin
        if (!rst) begin
            sm_pulse <= SM_PULSE_RESET;
        end else begin
            case (sm_pulse)
                SM_PULSE_RESET: begin
                    if (start) begin
                        sm_pulse <= SM_PULSE_START;
                    end
                    else begin
                        sm_pulse <= SM_PULSE_RESET;
                    end
                end
                SM_PULSE_START: begin
                    sm_pulse <= SM_PULSE_WAIT;
                end
                SM_PULSE_WAIT: begin
                    if (start) begin
                        sm_pulse <= SM_PULSE_WAIT;
                    end
                    else begin
                        sm_pulse <= SM_PULSE_RESET;
                    end
                end
                default: begin
                    sm_pulse <= SM_PULSE_RESET;
                end
            endcase // case (sm_pulse)
        end
    end


// RAM Controller

// Input
always @(posedge clk) begin
    if(!rst) begin
        for(i = 0; i < `PIXEL_ROW_SIZE; i = i + 1) begin
            for(j = 0; j < `PIXEL_ROW_SIZE; j = j + 1) begin
                in_RAM[i][j] <= 0;
            end
        end
        in_ram_row_sel <= 0;
    end
    else begin
        if(start) begin
            if(!in_ram_row_sel[3]) begin
                in_RAM[in_ram_row_sel] <= in_channel;
                in_ram_row_sel <= in_ram_row_sel + 1;
            end
        end
        else begin
            for(i = 0; i < `PIXEL_ROW_SIZE; i = i + 1) begin
                for(j = 0; j < `PIXEL_ROW_SIZE; j = j + 1) begin
                    in_RAM[i][j] <= 0;
                end
            end
            in_ram_row_sel <= 0;
        end
    end
end

// Output
always @(posedge clk) begin
    if(!rst) begin
        for(i = 0; i < `PIXEL_ROW_SIZE; i = i + 1) begin
            out_channel[i] <= 0;
        end
        out_ready <= 1'b0;
        out_ram_row_sel <= 0;
    end
    else begin
        if(out_ram_ready) begin
            if(!out_ram_row_sel[3]) begin
                out_channel <= out_RAM[out_ram_row_sel];
                out_ram_row_sel <= out_ram_row_sel + 1;
            end
            out_ready <= 1'b1;
        end
        else begin
            for(i = 0; i < `PIXEL_ROW_SIZE; i = i + 1) begin
                out_channel[i] <= 0;
            end
            out_ready <= 1'b0;
            out_ram_row_sel <= 0;
        end
    end
end

// DCT Systolic Array Controller
always @(posedge clk) begin
    if(!rst) begin
        for(i = 0; i < `PIXEL_BUS_CNT; i = i + 1) begin
            Vin[i] <= 0;
            Hin[i] <= 0;
            reg_mode[i] <= 0;
            for(j = 0; j < `PIXEL_BUS_CNT; j = j + 1) begin
                out_RAM[i][j] <= 0;
            end

        end
        out_ram_ready <= 0;
        dct_sm <= INIT;

    end
    else begin
        case (dct_sm)
            INIT: begin
                for(i = 0; i < `PIXEL_BUS_CNT; i = i + 1) begin
                    Vin[i] <= 0;
                    Hin[i] <= 0;
                    reg_mode[i] <= 0;
                    for(j = 0; j < `PIXEL_BUS_CNT; j = j + 1) begin
                        out_RAM[i][j] <= 0;
                    end
                end
                out_ram_ready <= 0;
                dct_sm <= sm_start ? S0 : INIT;
            end
            S0: begin
                Hin[0] <= in_RAM[0][0];
                Hin[1] <= 0;
                Hin[2] <= 0;
                Hin[3] <= 0;
                Hin[4] <= 0;
                Hin[5] <= 0;
                Hin[6] <= 0;
                Hin[7] <= 0;

                Vin[0] <= COEF_MTX[0][0];
                Vin[1] <= 0;
                Vin[2] <= 0;
                Vin[3] <= 0;
                Vin[4] <= 0;
                Vin[5] <= 0;
                Vin[6] <= 0;
                Vin[7] <= 0;

                dct_sm <= S1;
            end

            S1: begin
                Hin[0] <= in_RAM[0][1];
                Hin[1] <= in_RAM[1][0];
                Hin[2] <= 0;
                Hin[3] <= 0;
                Hin[4] <= 0;
                Hin[5] <= 0;
                Hin[6] <= 0;
                Hin[7] <= 0;

                Vin[0] <= COEF_MTX[1][0];
                Vin[1] <= COEF_MTX[0][1];
                Vin[2] <= 0;
                Vin[3] <= 0;
                Vin[4] <= 0;
                Vin[5] <= 0;
                Vin[6] <= 0;
                Vin[7] <= 0;

                dct_sm <= S2;
            end

            S2: begin
                Hin[0] <= in_RAM[0][2];
                Hin[1] <= in_RAM[1][1];
                Hin[2] <= in_RAM[2][0];
                Hin[3] <= 0;
                Hin[4] <= 0;
                Hin[5] <= 0;
                Hin[6] <= 0;
                Hin[7] <= 0;

                Vin[0] <= COEF_MTX[2][0];
                Vin[1] <= COEF_MTX[1][1];
                Vin[2] <= COEF_MTX[0][2];
                Vin[3] <= 0;
                Vin[4] <= 0;
                Vin[5] <= 0;
                Vin[6] <= 0;
                Vin[7] <= 0;

                dct_sm <= S3;
            end

            S3: begin
                Hin[0] <= in_RAM[0][3];
                Hin[1] <= in_RAM[1][2];
                Hin[2] <= in_RAM[2][1];
                Hin[3] <= in_RAM[3][0];
                Hin[4] <= 0;
                Hin[5] <= 0;
                Hin[6] <= 0;
                Hin[7] <= 0;

                Vin[0] <= COEF_MTX[3][0];
                Vin[1] <= COEF_MTX[2][1];
                Vin[2] <= COEF_MTX[1][2];
                Vin[3] <= COEF_MTX[0][3];
                Vin[4] <= 0;
                Vin[5] <= 0;
                Vin[6] <= 0;
                Vin[7] <= 0;

                dct_sm <= S4;
            end

            S4: begin
                Hin[0] <= in_RAM[0][4];
                Hin[1] <= in_RAM[1][3];
                Hin[2] <= in_RAM[2][2];
                Hin[3] <= in_RAM[3][1];
                Hin[4] <= in_RAM[4][0];
                Hin[5] <= 0;
                Hin[6] <= 0;
                Hin[7] <= 0;

                Vin[0] <= COEF_MTX[4][0];
                Vin[1] <= COEF_MTX[3][1];
                Vin[2] <= COEF_MTX[2][2];
                Vin[3] <= COEF_MTX[1][3];
                Vin[4] <= COEF_MTX[0][4];
                Vin[5] <= 0;
                Vin[6] <= 0;
                Vin[7] <= 0;

                dct_sm <= S5;
            end

            S5: begin
                Hin[0] <= in_RAM[0][5];
                Hin[1] <= in_RAM[1][4];
                Hin[2] <= in_RAM[2][3];
                Hin[3] <= in_RAM[3][2];
                Hin[4] <= in_RAM[4][1];
                Hin[5] <= in_RAM[5][0];
                Hin[6] <= 0;
                Hin[7] <= 0;

                Vin[0] <= COEF_MTX[5][0];
                Vin[1] <= COEF_MTX[4][1];
                Vin[2] <= COEF_MTX[3][2];
                Vin[3] <= COEF_MTX[2][3];
                Vin[4] <= COEF_MTX[1][4];
                Vin[5] <= COEF_MTX[0][5];
                Vin[6] <= 0;
                Vin[7] <= 0;

                dct_sm <= S6;
            end

            S6: begin
                Hin[0] <= in_RAM[0][6];
                Hin[1] <= in_RAM[1][5];
                Hin[2] <= in_RAM[2][4];
                Hin[3] <= in_RAM[3][3];
                Hin[4] <= in_RAM[4][2];
                Hin[5] <= in_RAM[5][1];
                Hin[6] <= in_RAM[6][0];
                Hin[7] <= 0;

                Vin[0] <= COEF_MTX[6][0];
                Vin[1] <= COEF_MTX[5][1];
                Vin[2] <= COEF_MTX[4][2];
                Vin[3] <= COEF_MTX[3][3];
                Vin[4] <= COEF_MTX[2][4];
                Vin[5] <= COEF_MTX[1][5];
                Vin[6] <= COEF_MTX[0][6];
                Vin[7] <= 0;

                dct_sm <= S7;
            end

            S7: begin
                Hin[0] <= in_RAM[0][7];
                Hin[1] <= in_RAM[1][6];
                Hin[2] <= in_RAM[2][5];
                Hin[3] <= in_RAM[3][4];
                Hin[4] <= in_RAM[4][3];
                Hin[5] <= in_RAM[5][2];
                Hin[6] <= in_RAM[6][1];
                Hin[7] <= in_RAM[7][0];

                Vin[0] <= COEF_MTX[7][0];
                Vin[1] <= COEF_MTX[6][1];
                Vin[2] <= COEF_MTX[5][2];
                Vin[3] <= COEF_MTX[4][3];
                Vin[4] <= COEF_MTX[3][4];
                Vin[5] <= COEF_MTX[2][5];
                Vin[6] <= COEF_MTX[1][6];
                Vin[7] <= COEF_MTX[0][7];
                dct_sm <= S8;
            end

            S8: begin
                Hin[0] <= COEF_MTX[0][0];
                Hin[1] <= in_RAM[1][7];
                Hin[2] <= in_RAM[2][6];
                Hin[3] <= in_RAM[3][5];
                Hin[4] <= in_RAM[4][4];
                Hin[5] <= in_RAM[5][3];
                Hin[6] <= in_RAM[6][2];
                Hin[7] <= in_RAM[7][1];

                Vin[0] <= 0;
                Vin[1] <= COEF_MTX[7][1];
                Vin[2] <= COEF_MTX[6][2];
                Vin[3] <= COEF_MTX[5][3];
                Vin[4] <= COEF_MTX[4][4];
                Vin[5] <= COEF_MTX[3][5];
                Vin[6] <= COEF_MTX[2][6];
                Vin[7] <= COEF_MTX[1][7];

                reg_mode[0][0] <= 1'b1;

                dct_sm <= S9;
            end

            S9: begin
                Hin[0] <= COEF_MTX[0][1];
                Hin[1] <= COEF_MTX[1][0];
                Hin[2] <= in_RAM[2][7];
                Hin[3] <= in_RAM[3][6];
                Hin[4] <= in_RAM[4][5];
                Hin[5] <= in_RAM[5][4];
                Hin[6] <= in_RAM[6][3];
                Hin[7] <= in_RAM[7][2];

                Vin[0] <= 0;
                Vin[1] <= 0;
                Vin[2] <= COEF_MTX[7][2];
                Vin[3] <= COEF_MTX[6][3];
                Vin[4] <= COEF_MTX[5][4];
                Vin[5] <= COEF_MTX[4][5];
                Vin[6] <= COEF_MTX[3][6];
                Vin[7] <= COEF_MTX[2][7];

                reg_mode[0][1] <= 1'b1;
                reg_mode[1][0] <= 1'b1;

                dct_sm <= S10;
            end

            S10: begin
                Hin[0] <= COEF_MTX[0][2];
                Hin[1] <= COEF_MTX[1][1];
                Hin[2] <= COEF_MTX[2][0];
                Hin[3] <= in_RAM[3][7];
                Hin[4] <= in_RAM[4][6];
                Hin[5] <= in_RAM[5][5];
                Hin[6] <= in_RAM[6][4];
                Hin[7] <= in_RAM[7][3];

                Vin[0] <= 0;
                Vin[1] <= 0;
                Vin[2] <= 0;
                Vin[3] <= COEF_MTX[7][3];
                Vin[4] <= COEF_MTX[6][4];
                Vin[5] <= COEF_MTX[5][5];
                Vin[6] <= COEF_MTX[4][6];
                Vin[7] <= COEF_MTX[3][7];

                reg_mode[0][2] <= 1'b1;
                reg_mode[1][1] <= 1'b1;
                reg_mode[2][0] <= 1'b1;

                dct_sm <= S11;
            end

            S11: begin
                Hin[0] <= COEF_MTX[0][3];
                Hin[1] <= COEF_MTX[1][2];
                Hin[2] <= COEF_MTX[2][1];
                Hin[3] <= COEF_MTX[3][0];
                Hin[4] <= in_RAM[4][7];
                Hin[5] <= in_RAM[5][6];
                Hin[6] <= in_RAM[6][5];
                Hin[7] <= in_RAM[7][4];

                Vin[0] <= 0;
                Vin[1] <= 0;
                Vin[2] <= 0;
                Vin[3] <= 0;
                Vin[4] <= COEF_MTX[7][4];
                Vin[5] <= COEF_MTX[6][5];
                Vin[6] <= COEF_MTX[5][6];
                Vin[7] <= COEF_MTX[4][7];

                reg_mode[0][3] <= 1'b1;
                reg_mode[1][2] <= 1'b1;
                reg_mode[2][1] <= 1'b1;
                reg_mode[3][0] <= 1'b1;


                dct_sm <= S12;
            end

            S12: begin
                Hin[0] <= COEF_MTX[0][4];
                Hin[1] <= COEF_MTX[1][3];
                Hin[2] <= COEF_MTX[2][2];
                Hin[3] <= COEF_MTX[3][1];
                Hin[4] <= COEF_MTX[4][0];
                Hin[5] <= in_RAM[5][7];
                Hin[6] <= in_RAM[6][6];
                Hin[7] <= in_RAM[7][5];

                Vin[0] <= 0;
                Vin[1] <= 0;
                Vin[2] <= 0;
                Vin[3] <= 0;
                Vin[4] <= 0;
                Vin[5] <= COEF_MTX[7][5];
                Vin[6] <= COEF_MTX[6][6];
                Vin[7] <= COEF_MTX[5][7];

                reg_mode[0][4] <= 1'b1;
                reg_mode[1][3] <= 1'b1;
                reg_mode[2][2] <= 1'b1;
                reg_mode[3][1] <= 1'b1;
                reg_mode[4][0] <= 1'b1;

                dct_sm <= S13;
            end

            S13: begin
                Hin[0] <= COEF_MTX[0][5];
                Hin[1] <= COEF_MTX[1][4];
                Hin[2] <= COEF_MTX[2][3];
                Hin[3] <= COEF_MTX[3][2];
                Hin[4] <= COEF_MTX[4][1];
                Hin[5] <= COEF_MTX[5][0];
                Hin[6] <= in_RAM[6][7];
                Hin[7] <= in_RAM[7][6];

                Vin[0] <= 0;
                Vin[1] <= 0;
                Vin[2] <= 0;
                Vin[3] <= 0;
                Vin[4] <= 0;
                Vin[5] <= 0;
                Vin[6] <= COEF_MTX[7][6];
                Vin[7] <= COEF_MTX[6][7];

                reg_mode[0][5] <= 1'b1;
                reg_mode[1][4] <= 1'b1;
                reg_mode[2][3] <= 1'b1;
                reg_mode[3][2] <= 1'b1;
                reg_mode[4][1] <= 1'b1;
                reg_mode[5][0] <= 1'b1;

                dct_sm <= S14;
            end

            S14: begin
                Hin[0] <= COEF_MTX[0][6];
                Hin[1] <= COEF_MTX[1][5];
                Hin[2] <= COEF_MTX[2][4];
                Hin[3] <= COEF_MTX[3][3];
                Hin[4] <= COEF_MTX[4][2];
                Hin[5] <= COEF_MTX[5][1];
                Hin[6] <= COEF_MTX[6][0];
                Hin[7] <= in_RAM[7][7];

                Vin[0] <= 0;
                Vin[1] <= 0;
                Vin[2] <= 0;
                Vin[3] <= 0;
                Vin[4] <= 0;
                Vin[5] <= 0;
                Vin[6] <= 0;
                Vin[7] <= COEF_MTX[7][7];

                reg_mode[0][6] <= 1'b1;
                reg_mode[1][5] <= 1'b1;
                reg_mode[2][4] <= 1'b1;
                reg_mode[3][3] <= 1'b1;
                reg_mode[4][2] <= 1'b1;
                reg_mode[5][1] <= 1'b1;
                reg_mode[6][0] <= 1'b1;

                dct_sm <= S15;
            end

            S15: begin
                Hin[0] <= COEF_MTX[0][7];
                Hin[1] <= COEF_MTX[1][6];
                Hin[2] <= COEF_MTX[2][5];
                Hin[3] <= COEF_MTX[3][4];
                Hin[4] <= COEF_MTX[4][3];
                Hin[5] <= COEF_MTX[5][2];
                Hin[6] <= COEF_MTX[6][1];
                Hin[7] <= COEF_MTX[7][0];

                Vin[0] <= 0;
                Vin[1] <= 0;
                Vin[2] <= 0;
                Vin[3] <= 0;
                Vin[4] <= 0;
                Vin[5] <= 0;
                Vin[6] <= 0;
                Vin[7] <= 0;

                reg_mode[0][7] <= 1'b1;
                reg_mode[1][6] <= 1'b1;
                reg_mode[2][5] <= 1'b1;
                reg_mode[3][4] <= 1'b1;
                reg_mode[4][3] <= 1'b1;
                reg_mode[5][2] <= 1'b1;
                reg_mode[6][1] <= 1'b1;
                reg_mode[7][0] <= 1'b1;

                dct_sm <= S16;
            end

            S16: begin
                Hin[0] <= 0;
                Hin[1] <= COEF_MTX[1][7];
                Hin[2] <= COEF_MTX[2][6];
                Hin[3] <= COEF_MTX[3][5];
                Hin[4] <= COEF_MTX[4][4];
                Hin[5] <= COEF_MTX[5][3];
                Hin[6] <= COEF_MTX[6][2];
                Hin[7] <= COEF_MTX[7][1];



                reg_mode[1][7] <= 1'b1;
                reg_mode[2][6] <= 1'b1;
                reg_mode[3][5] <= 1'b1;
                reg_mode[4][4] <= 1'b1;
                reg_mode[5][3] <= 1'b1;
                reg_mode[6][2] <= 1'b1;
                reg_mode[7][1] <= 1'b1;

                dct_sm <= S17;
            end

            S17: begin
                Hin[0] <= 0;
                Hin[1] <= 0;
                Hin[2] <= COEF_MTX[2][7];
                Hin[3] <= COEF_MTX[3][6];
                Hin[4] <= COEF_MTX[4][5];
                Hin[5] <= COEF_MTX[5][4];
                Hin[6] <= COEF_MTX[6][3];
                Hin[7] <= COEF_MTX[7][2];
                out_RAM[0][0] <= Vout[0];


                reg_mode[2][7] <= 1'b1;
                reg_mode[3][6] <= 1'b1;
                reg_mode[4][5] <= 1'b1;
                reg_mode[5][4] <= 1'b1;
                reg_mode[6][3] <= 1'b1;
                reg_mode[7][2] <= 1'b1;

                dct_sm <= S18;
            end

            S18: begin
                Hin[0] <= 0;
                Hin[1] <= 0;
                Hin[2] <= 0;
                Hin[3] <= COEF_MTX[3][7];
                Hin[4] <= COEF_MTX[4][6];
                Hin[5] <= COEF_MTX[5][5];
                Hin[6] <= COEF_MTX[6][4];
                Hin[7] <= COEF_MTX[7][3];
                out_RAM[0][1] <= Vout[0];
                out_RAM[1][0] <= Vout[1];


                reg_mode[3][7] <= 1'b1;
                reg_mode[4][6] <= 1'b1;
                reg_mode[5][5] <= 1'b1;
                reg_mode[6][4] <= 1'b1;
                reg_mode[7][3] <= 1'b1;

                dct_sm <= S19;
            end

            S19: begin
                Hin[0] <= 0;
                Hin[1] <= 0;
                Hin[2] <= 0;
                Hin[3] <= 0;
                Hin[4] <= COEF_MTX[4][7];
                Hin[5] <= COEF_MTX[5][6];
                Hin[6] <= COEF_MTX[6][5];
                Hin[7] <= COEF_MTX[7][4];
                out_RAM[0][2] <= Vout[0];
                out_RAM[1][1] <= Vout[1];
                out_RAM[2][0] <= Vout[2];


                reg_mode[4][7] <= 1'b1;
                reg_mode[5][6] <= 1'b1;
                reg_mode[6][5] <= 1'b1;
                reg_mode[7][4] <= 1'b1;

                dct_sm <= S20;
            end

            S20: begin
                Hin[0] <= 0;
                Hin[1] <= 0;
                Hin[2] <= 0;
                Hin[3] <= 0;
                Hin[4] <= 0;
                Hin[5] <= COEF_MTX[5][7];
                Hin[6] <= COEF_MTX[6][6];
                Hin[7] <= COEF_MTX[7][5];
                out_RAM[0][3] <= Vout[0];
                out_RAM[1][2] <= Vout[1];
                out_RAM[2][1] <= Vout[2];
                out_RAM[3][0] <= Vout[3];


                reg_mode[5][7] <= 1'b1;
                reg_mode[6][6] <= 1'b1;
                reg_mode[7][5] <= 1'b1;

                dct_sm <= S21;
            end

            S21: begin
                Hin[0] <= 0;
                Hin[1] <= 0;
                Hin[2] <= 0;
                Hin[3] <= 0;
                Hin[4] <= 0;
                Hin[5] <= 0;
                Hin[6] <= COEF_MTX[6][7];
                Hin[7] <= COEF_MTX[7][6];
                out_RAM[0][4] <= Vout[0];
                out_RAM[1][3] <= Vout[1];
                out_RAM[2][2] <= Vout[2];
                out_RAM[3][1] <= Vout[3];
                out_RAM[4][0] <= Vout[4];


                reg_mode[6][7] <= 1'b1;
                reg_mode[7][6] <= 1'b1;

                dct_sm <= S22;
            end

            S22: begin
                Hin[0] <= 0;
                Hin[1] <= 0;
                Hin[2] <= 0;
                Hin[3] <= 0;
                Hin[4] <= 0;
                Hin[5] <= 0;
                Hin[6] <= 0;
                Hin[7] <= COEF_MTX[7][7];
                out_RAM[0][5] <= Vout[0];
                out_RAM[1][4] <= Vout[1];
                out_RAM[2][3] <= Vout[2];
                out_RAM[3][2] <= Vout[3];
                out_RAM[4][1] <= Vout[4];
                out_RAM[5][0] <= Vout[5];


                reg_mode[7][7] <= 1'b1;

                dct_sm <= S23;
            end

            S23: begin
                Hin[0] <= 0;
                Hin[1] <= 0;
                Hin[2] <= 0;
                Hin[3] <= 0;
                Hin[4] <= 0;
                Hin[5] <= 0;
                Hin[6] <= 0;
                Hin[7] <= 0;
                out_RAM[0][6] <= Vout[0];
                out_RAM[1][5] <= Vout[1];
                out_RAM[2][4] <= Vout[2];
                out_RAM[3][3] <= Vout[3];
                out_RAM[4][2] <= Vout[4];
                out_RAM[5][1] <= Vout[5];
                out_RAM[6][0] <= Vout[6];

                dct_sm <= S24;
            end
            S24: begin
                out_RAM[0][7] <= Vout[0];
                out_RAM[1][6] <= Vout[1];
                out_RAM[2][5] <= Vout[2];
                out_RAM[3][4] <= Vout[3];
                out_RAM[4][3] <= Vout[4];
                out_RAM[5][2] <= Vout[5];
                out_RAM[6][1] <= Vout[6];
                out_RAM[7][0] <= Vout[7];

                out_ram_ready <= 1'b1;
                dct_sm <= S25;
            end
            S25: begin
                out_RAM[1][7] <= Vout[1];
                out_RAM[2][6] <= Vout[2];
                out_RAM[3][5] <= Vout[3];
                out_RAM[4][4] <= Vout[4];
                out_RAM[5][3] <= Vout[5];
                out_RAM[6][2] <= Vout[6];
                out_RAM[7][1] <= Vout[7];

                dct_sm <= S26;
            end
            S26: begin
                 out_RAM[2][7] <= Vout[2];
                out_RAM[3][6] <= Vout[3];
                out_RAM[4][5] <= Vout[4];
                out_RAM[5][4] <= Vout[5];
                out_RAM[6][3] <= Vout[6];
                out_RAM[7][2] <= Vout[7];

                dct_sm <= S27;
            end
            S27: begin
                out_RAM[3][7] <= Vout[3];
                out_RAM[4][6] <= Vout[4];
                out_RAM[5][5] <= Vout[5];
                out_RAM[6][4] <= Vout[6];
                out_RAM[7][3] <= Vout[7];

                dct_sm <= S28;
            end
            S28: begin
                out_RAM[4][7] <= Vout[4];
                out_RAM[5][6] <= Vout[5];
                out_RAM[6][5] <= Vout[6];
                out_RAM[7][4] <= Vout[7];

                dct_sm <= S29;
            end
            S29: begin
                out_RAM[5][7] <= Vout[5];
                out_RAM[6][6] <= Vout[6];
                out_RAM[7][5] <= Vout[7];

                dct_sm <= S30;
            end
            S30: begin
                out_RAM[6][7] <= Vout[6];
                out_RAM[7][6] <= Vout[7];

                dct_sm <= S31;
            end
            S31: begin
                out_RAM[7][7] <= Vout[7];
                dct_sm <= S32;
            end
            S32: begin
                dct_sm <= INIT;
            end
            // default: begin
            // end
        endcase
    end
end


endmodule
