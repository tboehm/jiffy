`ifndef __CONFIGURATION_VH__
 `define __CONFIGURATION_VH__

// `timescale 1 ns / 1 ps
 `define QTZ_ADDRESS 32'h0800
 `define QTZ_TRANSFER_CNT 48
 `define QTZ_ADDRESS_RNG `QTZ_TRANSFER_CNT - 1:0
 `define A_MAGIC_ADDR 32'h0380
 `define B_MAGIC_ADDR 32'h0780
 `define A_READ_ADDR 32'h0200
 `define B_READ_ADDR 32'h0600
 `define A_WRITE_ADDR 32'h0000
 `define B_WRITE_ADDR 32'h0400

// How many pixels to process in parallel
 `define PIXEL_BUS_CNT 8
 `define PIXEL_BUS_RNG `PIXEL_BUS_CNT - 1:0

 `define BLOCK_SIZE 64
 `define BLOCK_RNG `BLOCK_SIZE - 1:0

 `define FULL_BLOCK_SIZE `BLOCK_SIZE*3
 `define FULL_BLOCK_RNG `FULL_BLOCK_SIZE - 1:0

 `define MEM_WIDTH 32
 `define MEM_WIDTH_RNG `MEM_WIDTH - 1:0
 `define MEM_DEPTH 96 // need 48 memory locations
 `define MEM_DEPTH_RNG `MEM_DEPTH - 1:0

 `define TOTAL_TRANSFER 96

 `define MAGIC_NUMBER 240
 
 `define ADDR_INCR 4

// Size of each channel
 `define RGB_CHANNEL_CNT 8
 `define RGB_CHANNEL_RNG `RGB_CHANNEL_CNT - 1:0

 `define YCBCR_CHANNEL_CNT 8
 `define YCBCR_CHANNEL_RNG `YCBCR_CHANNEL_CNT - 1:0
 `define YCBCR_ROW_CNT `PIXEL_BUS_CNT * `YCBCR_CHANNEL_CNT
 `define YCBCR_ROW_RNG `YCBCR_ROW_CNT - 1:0

// Bits per channel after DCT (fixed point again)
 `define DCT_CHANNEL_CNT 21
 `define DCT_CHANNEL_RNG `DCT_CHANNEL_CNT - 1:0

// Bits per channel after quantization (integers)
 `define QTZ_CHANNEL_CNT 16
 `define QTZ_CHANNEL_RNG `QTZ_CHANNEL_CNT - 1:0
 `define QTZ_MTX_CNT 8
 `define QTZ_MTX_RNG `QTZ_MTX_CNT - 1:0
 
// Type of downsampling:
// - No downsampling (4:4:4)
// - 1/2 horizontal, full vertical (4:2:2)
// - 1/2 horizontal, 1/2 vertical (4:2:1)
 `define DOWNSAMPLE_TYPE_RNG 1:0
 `define DOWNSAMPLE_TYPE_444 0
 `define DOWNSAMPLE_TYPE_422 1
 `define DOWNSAMPLE_TYPE_421 2
 `define DOWNSAMPLE_TYPE_INVALID 3

// Lookup Table Sizing
 `define LUT_ADDR_BITS 6
 `define LUT_ADDR_RNG `LUT_ADDR_BITS - 1:0
 `define LUT_DATA_SIZE 16
 `define LUT_DATA_RNG `LUT_DATA_SIZE - 1:0

// Pixel Controller
 `define TOTAL_PIXEL_CNT 64
 `define TOTAL_PIXEL_RNG `TOTAL_PIXEL_CNT - 1:0
 `define PIXEL_ROW_SIZE 8
 `define PIXEL_ROW_RNG `PIXEL_ROW_SIZE - 1:0

// DCT Controller
 `define DCT_SM_BITS 6
 `define DCT_SM_RNG `DCT_SM_BITS - 1:0

 `define DCT_ROW_BITS `PIXEL_BUS_CNT * `DCT_CHANNEL_CNT
 `define DCT_ROW_RNG `DCT_ROW_BITS - 1:0

`endif // __CONFIGURATION_VH__
