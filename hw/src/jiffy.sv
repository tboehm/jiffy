`include "configuration.vh"
module jiffy (

    // Encode
    input wire [`RGB_CHANNEL_RNG]   imem_encode_r [`PIXEL_BUS_RNG],
    input wire [`RGB_CHANNEL_RNG]   imem_encode_g [`PIXEL_BUS_RNG],
    input wire [`RGB_CHANNEL_RNG]   imem_encode_b [`PIXEL_BUS_RNG],

    output wire [`QTZ_CHANNEL_RNG]  omem_encode_y  [`PIXEL_BUS_RNG],
    output wire [`QTZ_CHANNEL_RNG]  omem_encode_cb [`PIXEL_BUS_RNG],
    output wire [`QTZ_CHANNEL_RNG]  omem_encode_cr [`PIXEL_BUS_RNG],

    // Decode
    input wire [`QTZ_CHANNEL_RNG]   imem_decode_y  [`PIXEL_BUS_RNG],
    input wire [`QTZ_CHANNEL_RNG]   imem_decode_cb [`PIXEL_BUS_RNG],
    input wire [`QTZ_CHANNEL_RNG]   imem_decode_cr [`PIXEL_BUS_RNG],

    output reg [`RGB_CHANNEL_RNG]   omem_decode_r [`PIXEL_BUS_RNG],
    output reg [`RGB_CHANNEL_RNG]   omem_decode_g [`PIXEL_BUS_RNG],
    output reg [`RGB_CHANNEL_RNG]   omem_decode_b [`PIXEL_BUS_RNG],
    
    input wire [31:0] qtz_mtx [`QTZ_ADDRESS_RNG],

    input   code_mode,
    input   start,
    output  out_ready,
    input   clk,
    input   rst
);

genvar i, j;

wire encode_start;
wire decode_start;
wire encode_out_ready;
wire decode_out_ready;
reg [3:0] row_sel;

assign out_ready = encode_out_ready | decode_out_ready;
assign encode_start = start & ~code_mode & !row_sel[3];
assign decode_start = start & code_mode & !row_sel[3];

// Encode Wires
wire [`YCBCR_CHANNEL_RNG] csx_dct_y_channel  [`PIXEL_BUS_RNG];
wire [`YCBCR_CHANNEL_RNG] csx_dct_cb_channel [`PIXEL_BUS_RNG];
wire [`YCBCR_CHANNEL_RNG] csx_dct_cr_channel [`PIXEL_BUS_RNG];
wire [`DCT_CHANNEL_RNG]   dct_qtz_y_channel  [`PIXEL_BUS_RNG];
wire [`DCT_CHANNEL_RNG]   dct_qtz_cb_channel [`PIXEL_BUS_RNG];
wire [`DCT_CHANNEL_RNG]   dct_qtz_cr_channel [`PIXEL_BUS_RNG];
wire csx_dct_out_ready;
wire dct_qtz_out_ready;

wire [`DCT_CHANNEL_RNG] csx_dct_y_norm  [`PIXEL_BUS_RNG];
wire [`DCT_CHANNEL_RNG] csx_dct_cb_norm [`PIXEL_BUS_RNG];
wire [`DCT_CHANNEL_RNG] csx_dct_cr_norm [`PIXEL_BUS_RNG];

// Decode Wires
wire [`DCT_CHANNEL_RNG] qtz_dct_y_channel  [`PIXEL_BUS_RNG];
wire [`DCT_CHANNEL_RNG] qtz_dct_cb_channel [`PIXEL_BUS_RNG];
wire [`DCT_CHANNEL_RNG] qtz_dct_cr_channel [`PIXEL_BUS_RNG];
wire [`DCT_CHANNEL_RNG] dct_csx_y_channel  [`PIXEL_BUS_RNG];
wire [`DCT_CHANNEL_RNG] dct_csx_cb_channel [`PIXEL_BUS_RNG];
wire [`DCT_CHANNEL_RNG] dct_csx_cr_channel [`PIXEL_BUS_RNG];
wire qtz_dct_out_ready;
wire dct_csx_out_ready;

wire [`YCBCR_CHANNEL_RNG] dct_csx_y_norm  [`PIXEL_BUS_RNG];
wire [`YCBCR_CHANNEL_RNG] dct_csx_cb_norm [`PIXEL_BUS_RNG];
wire [`YCBCR_CHANNEL_RNG] dct_csx_cr_norm [`PIXEL_BUS_RNG];


// QTZ Matrices
wire [`QTZ_MTX_RNG] qtz_mtx_y  [`PIXEL_ROW_RNG][`PIXEL_ROW_RNG];
wire [`QTZ_MTX_RNG] qtz_mtx_cb [`PIXEL_ROW_RNG][`PIXEL_ROW_RNG];
wire [`QTZ_MTX_RNG] qtz_mtx_cr [`PIXEL_ROW_RNG][`PIXEL_ROW_RNG];

assign {qtz_mtx_y [0][7], qtz_mtx_y [0][6], qtz_mtx_y [0][5], qtz_mtx_y [0][4], qtz_mtx_y [0][3], qtz_mtx_y [0][2], qtz_mtx_y [0][1], qtz_mtx_y [0][0]} = {qtz_mtx[1],  qtz_mtx[0]};
assign {qtz_mtx_y [1][7], qtz_mtx_y [1][6], qtz_mtx_y [1][5], qtz_mtx_y [1][4], qtz_mtx_y [1][3], qtz_mtx_y [1][2], qtz_mtx_y [1][1], qtz_mtx_y [1][0]} = {qtz_mtx[3],  qtz_mtx[2]};
assign {qtz_mtx_y [2][7], qtz_mtx_y [2][6], qtz_mtx_y [2][5], qtz_mtx_y [2][4], qtz_mtx_y [2][3], qtz_mtx_y [2][2], qtz_mtx_y [2][1], qtz_mtx_y [2][0]} = {qtz_mtx[5],  qtz_mtx[4]};
assign {qtz_mtx_y [3][7], qtz_mtx_y [3][6], qtz_mtx_y [3][5], qtz_mtx_y [3][4], qtz_mtx_y [3][3], qtz_mtx_y [3][2], qtz_mtx_y [3][1], qtz_mtx_y [3][0]} = {qtz_mtx[7],  qtz_mtx[6]};
assign {qtz_mtx_y [4][7], qtz_mtx_y [4][6], qtz_mtx_y [4][5], qtz_mtx_y [4][4], qtz_mtx_y [4][3], qtz_mtx_y [4][2], qtz_mtx_y [4][1], qtz_mtx_y [4][0]} = {qtz_mtx[9],  qtz_mtx[8]};
assign {qtz_mtx_y [5][7], qtz_mtx_y [5][6], qtz_mtx_y [5][5], qtz_mtx_y [5][4], qtz_mtx_y [5][3], qtz_mtx_y [5][2], qtz_mtx_y [5][1], qtz_mtx_y [5][0]} = {qtz_mtx[11], qtz_mtx[10]};
assign {qtz_mtx_y [6][7], qtz_mtx_y [6][6], qtz_mtx_y [6][5], qtz_mtx_y [6][4], qtz_mtx_y [6][3], qtz_mtx_y [6][2], qtz_mtx_y [6][1], qtz_mtx_y [6][0]} = {qtz_mtx[13], qtz_mtx[12]};
assign {qtz_mtx_y [7][7], qtz_mtx_y [7][6], qtz_mtx_y [7][5], qtz_mtx_y [7][4], qtz_mtx_y [7][3], qtz_mtx_y [7][2], qtz_mtx_y [7][1], qtz_mtx_y [7][0]} = {qtz_mtx[15], qtz_mtx[14]};
assign {qtz_mtx_cb[0][7], qtz_mtx_cb[0][6], qtz_mtx_cb[0][5], qtz_mtx_cb[0][4], qtz_mtx_cb[0][3], qtz_mtx_cb[0][2], qtz_mtx_cb[0][1], qtz_mtx_cb[0][0]} = {qtz_mtx[17], qtz_mtx[16]};
assign {qtz_mtx_cb[1][7], qtz_mtx_cb[1][6], qtz_mtx_cb[1][5], qtz_mtx_cb[1][4], qtz_mtx_cb[1][3], qtz_mtx_cb[1][2], qtz_mtx_cb[1][1], qtz_mtx_cb[1][0]} = {qtz_mtx[19], qtz_mtx[18]};
assign {qtz_mtx_cb[2][7], qtz_mtx_cb[2][6], qtz_mtx_cb[2][5], qtz_mtx_cb[2][4], qtz_mtx_cb[2][3], qtz_mtx_cb[2][2], qtz_mtx_cb[2][1], qtz_mtx_cb[2][0]} = {qtz_mtx[21], qtz_mtx[20]};
assign {qtz_mtx_cb[3][7], qtz_mtx_cb[3][6], qtz_mtx_cb[3][5], qtz_mtx_cb[3][4], qtz_mtx_cb[3][3], qtz_mtx_cb[3][2], qtz_mtx_cb[3][1], qtz_mtx_cb[3][0]} = {qtz_mtx[23], qtz_mtx[22]};
assign {qtz_mtx_cb[4][7], qtz_mtx_cb[4][6], qtz_mtx_cb[4][5], qtz_mtx_cb[4][4], qtz_mtx_cb[4][3], qtz_mtx_cb[4][2], qtz_mtx_cb[4][1], qtz_mtx_cb[4][0]} = {qtz_mtx[25], qtz_mtx[24]};
assign {qtz_mtx_cb[5][7], qtz_mtx_cb[5][6], qtz_mtx_cb[5][5], qtz_mtx_cb[5][4], qtz_mtx_cb[5][3], qtz_mtx_cb[5][2], qtz_mtx_cb[5][1], qtz_mtx_cb[5][0]} = {qtz_mtx[27], qtz_mtx[26]};
assign {qtz_mtx_cb[6][7], qtz_mtx_cb[6][6], qtz_mtx_cb[6][5], qtz_mtx_cb[6][4], qtz_mtx_cb[6][3], qtz_mtx_cb[6][2], qtz_mtx_cb[6][1], qtz_mtx_cb[6][0]} = {qtz_mtx[29], qtz_mtx[28]};
assign {qtz_mtx_cb[7][7], qtz_mtx_cb[7][6], qtz_mtx_cb[7][5], qtz_mtx_cb[7][4], qtz_mtx_cb[7][3], qtz_mtx_cb[7][2], qtz_mtx_cb[7][1], qtz_mtx_cb[7][0]} = {qtz_mtx[31], qtz_mtx[30]};
assign {qtz_mtx_cr[0][7], qtz_mtx_cr[0][6], qtz_mtx_cr[0][5], qtz_mtx_cr[0][4], qtz_mtx_cr[0][3], qtz_mtx_cr[0][2], qtz_mtx_cr[0][1], qtz_mtx_cr[0][0]} = {qtz_mtx[33], qtz_mtx[32]};
assign {qtz_mtx_cr[1][7], qtz_mtx_cr[1][6], qtz_mtx_cr[1][5], qtz_mtx_cr[1][4], qtz_mtx_cr[1][3], qtz_mtx_cr[1][2], qtz_mtx_cr[1][1], qtz_mtx_cr[1][0]} = {qtz_mtx[35], qtz_mtx[34]};
assign {qtz_mtx_cr[2][7], qtz_mtx_cr[2][6], qtz_mtx_cr[2][5], qtz_mtx_cr[2][4], qtz_mtx_cr[2][3], qtz_mtx_cr[2][2], qtz_mtx_cr[2][1], qtz_mtx_cr[2][0]} = {qtz_mtx[37], qtz_mtx[36]};
assign {qtz_mtx_cr[3][7], qtz_mtx_cr[3][6], qtz_mtx_cr[3][5], qtz_mtx_cr[3][4], qtz_mtx_cr[3][3], qtz_mtx_cr[3][2], qtz_mtx_cr[3][1], qtz_mtx_cr[3][0]} = {qtz_mtx[39], qtz_mtx[38]};
assign {qtz_mtx_cr[4][7], qtz_mtx_cr[4][6], qtz_mtx_cr[4][5], qtz_mtx_cr[4][4], qtz_mtx_cr[4][3], qtz_mtx_cr[4][2], qtz_mtx_cr[4][1], qtz_mtx_cr[4][0]} = {qtz_mtx[41], qtz_mtx[40]};
assign {qtz_mtx_cr[5][7], qtz_mtx_cr[5][6], qtz_mtx_cr[5][5], qtz_mtx_cr[5][4], qtz_mtx_cr[5][3], qtz_mtx_cr[5][2], qtz_mtx_cr[5][1], qtz_mtx_cr[5][0]} = {qtz_mtx[43], qtz_mtx[42]};
assign {qtz_mtx_cr[6][7], qtz_mtx_cr[6][6], qtz_mtx_cr[6][5], qtz_mtx_cr[6][4], qtz_mtx_cr[6][3], qtz_mtx_cr[6][2], qtz_mtx_cr[6][1], qtz_mtx_cr[6][0]} = {qtz_mtx[45], qtz_mtx[44]};
assign {qtz_mtx_cr[7][7], qtz_mtx_cr[7][6], qtz_mtx_cr[7][5], qtz_mtx_cr[7][4], qtz_mtx_cr[7][3], qtz_mtx_cr[7][2], qtz_mtx_cr[7][1], qtz_mtx_cr[7][0]} = {qtz_mtx[47], qtz_mtx[46]};

// Turn off start after feeding in all input data

always @(posedge clk) begin
    if (!rst) begin
        row_sel <= 0;
    end 
    else begin
        if(start) begin
            if(!row_sel[3]) begin
                row_sel <= row_sel + 1;
            end
        end
        else begin
            row_sel <= 0;
        end
    end
end

// Encode

csx_encode csx_encode0(
    .csx_dct_y_channel(csx_dct_y_channel),
    .csx_dct_cb_channel(csx_dct_cb_channel),
    .csx_dct_cr_channel(csx_dct_cr_channel),
    .imem_csx_r_channel(imem_encode_r),
    .imem_csx_g_channel(imem_encode_g),
    .imem_csx_b_channel(imem_encode_b),
    .start(encode_start),
    .out_ready(csx_dct_out_ready),
    .clk(clk),
    .rst(rst)
);

for (i = 0; i < `PIXEL_BUS_CNT; i = i + 1) begin
    assign csx_dct_y_norm[i]  = (csx_dct_y_channel[i] - 128) << 8;
    assign csx_dct_cb_norm[i] = (csx_dct_cb_channel[i] - 128) << 8;
    assign csx_dct_cr_norm[i] = (csx_dct_cr_channel[i] - 128) << 8;
end

dct dct_encode0(
    .dct_out_y_channel(dct_qtz_y_channel),
    .dct_out_cb_channel(dct_qtz_cb_channel),
    .dct_out_cr_channel(dct_qtz_cr_channel),
    .out_ready(dct_qtz_out_ready),
    .dct_in_y_channel(csx_dct_y_norm),
    .dct_in_cb_channel(csx_dct_cb_norm),
    .dct_in_cr_channel(csx_dct_cr_norm),
    .code_mode(code_mode),
    .start(csx_dct_out_ready),
    .clk(clk),
    .rst(rst)
);

qtz_encode qtz_encode0(
    .qtz_omem_y_channel(omem_encode_y),
    .qtz_omem_cb_channel(omem_encode_cb),
    .qtz_omem_cr_channel(omem_encode_cr),
    .out_ready(encode_out_ready),
    .dct_qtz_y_channel(dct_qtz_y_channel),
    .dct_qtz_cb_channel(dct_qtz_cb_channel),
    .dct_qtz_cr_channel(dct_qtz_cr_channel),
    .qtz_mtx_y(qtz_mtx_y),
    .qtz_mtx_cb(qtz_mtx_cb),
    .qtz_mtx_cr(qtz_mtx_cr),
    .start(dct_qtz_out_ready),
    .clk(clk),
    .rst(rst)
);

// Decode

qtz_decode qtz_decode0(
    .qtz_dct_y_channel(qtz_dct_y_channel),
    .qtz_dct_cb_channel(qtz_dct_cb_channel),
    .qtz_dct_cr_channel(qtz_dct_cr_channel),
    .out_ready(qtz_dct_out_ready),
    .imem_qtz_y_channel(imem_decode_y),
    .imem_qtz_cb_channel(imem_decode_cb),
    .imem_qtz_cr_channel(imem_decode_cr),
    .qtz_mtx_y(qtz_mtx_y),
    .qtz_mtx_cb(qtz_mtx_cb),
    .qtz_mtx_cr(qtz_mtx_cr),
    .start(decode_start),
    .clk(clk),
    .rst(rst)
);

dct dct_decode0(
    .dct_out_y_channel(dct_csx_y_channel),
    .dct_out_cb_channel(dct_csx_cb_channel),
    .dct_out_cr_channel(dct_csx_cr_channel),
    .out_ready(dct_csx_out_ready),
    .dct_in_y_channel(qtz_dct_y_channel),
    .dct_in_cb_channel(qtz_dct_cb_channel),
    .dct_in_cr_channel(qtz_dct_cr_channel),
    .code_mode(code_mode),
    .start(qtz_dct_out_ready),
    .clk(clk),
    .rst(rst)
);

for(j = 0; j < `PIXEL_BUS_CNT; j = j + 1) begin
    assign dct_csx_y_norm[j]  = 8'((dct_csx_y_channel[j]  >> 8) + 128);
    assign dct_csx_cb_norm[j] = 8'((dct_csx_cb_channel[j] >> 8) + 128);
    assign dct_csx_cr_norm[j] = 8'((dct_csx_cr_channel[j] >> 8) + 128);
end


csx_decode csx_decode0(
    .csx_omem_r_channel(omem_decode_r),
    .csx_omem_g_channel(omem_decode_g),
    .csx_omem_b_channel(omem_decode_b),
    .dct_csx_y_channel(dct_csx_y_norm),
    .dct_csx_cb_channel(dct_csx_cb_norm),
    .dct_csx_cr_channel(dct_csx_cr_norm),
    .start(dct_csx_out_ready),
    .out_ready(decode_out_ready),
    .clk(clk),
    .rst(rst)
);


endmodule