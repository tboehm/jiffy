`include "configuration.vh"

module sa_pe( //Systolic Array Processing Element
    output reg [`DCT_CHANNEL_RNG] Vout,
    output reg [`DCT_CHANNEL_RNG] Hout,

    input wire signed [`DCT_CHANNEL_RNG] Vin,
    input wire signed [`DCT_CHANNEL_RNG] Hin,
    input wire reg_mode,
    input wire clk,
    input wire rst
);
    // 1 sign bit, 12 integer bits, 8 fraction bits
    reg signed [31:0] R;

    always @(posedge clk) begin
        if (~rst) begin
            R <= 0;
            Hout <= 0;
            Vout <= 0;
        end
        else begin
            R <= reg_mode ? R : (R + ((Vin * Hin) >>> 8));
            Vout <= reg_mode ? (Vin + ((Hin * R) >>> 8)) : Vin;
            Hout <= Hin;
        end
    end

endmodule


module sys_array(
    output wire [`DCT_CHANNEL_RNG] Vout [`PIXEL_BUS_RNG],
    input wire  [`DCT_CHANNEL_RNG] Hin  [`PIXEL_BUS_RNG],
    input wire  [`DCT_CHANNEL_RNG] Vin  [`PIXEL_BUS_RNG],
    input wire  [`PIXEL_BUS_RNG] reg_mode [`PIXEL_BUS_RNG],
    input wire clk,
    input wire rst
);

wire [`DCT_CHANNEL_RNG] V0_1 [`PIXEL_BUS_RNG];
wire [`DCT_CHANNEL_RNG] V1_2 [`PIXEL_BUS_RNG];
wire [`DCT_CHANNEL_RNG] V2_3 [`PIXEL_BUS_RNG];
wire [`DCT_CHANNEL_RNG] V3_4 [`PIXEL_BUS_RNG];
wire [`DCT_CHANNEL_RNG] V4_5 [`PIXEL_BUS_RNG];
wire [`DCT_CHANNEL_RNG] V5_6 [`PIXEL_BUS_RNG];
wire [`DCT_CHANNEL_RNG] V6_5 [`PIXEL_BUS_RNG];
wire [`DCT_CHANNEL_RNG] V6_7 [`PIXEL_BUS_RNG];

wire [`DCT_CHANNEL_RNG] H0 [`PIXEL_BUS_RNG];
wire [`DCT_CHANNEL_RNG] H1 [`PIXEL_BUS_RNG];
wire [`DCT_CHANNEL_RNG] H2 [`PIXEL_BUS_RNG];
wire [`DCT_CHANNEL_RNG] H3 [`PIXEL_BUS_RNG];
wire [`DCT_CHANNEL_RNG] H4 [`PIXEL_BUS_RNG];
wire [`DCT_CHANNEL_RNG] H5 [`PIXEL_BUS_RNG];
wire [`DCT_CHANNEL_RNG] H6 [`PIXEL_BUS_RNG];
wire [`DCT_CHANNEL_RNG] H7 [`PIXEL_BUS_RNG];

sa_pe sa_pe00 (.Vout(V0_1[0]), .Hout(H0[0]), .Vin(Vin[0]), .Hin(Hin[0]), .reg_mode(reg_mode[0][0]), .clk(clk), .rst(rst));
sa_pe sa_pe01 (.Vout(V0_1[1]), .Hout(H0[1]), .Vin(Vin[1]), .Hin(H0[0]),  .reg_mode(reg_mode[0][1]), .clk(clk), .rst(rst));
sa_pe sa_pe02 (.Vout(V0_1[2]), .Hout(H0[2]), .Vin(Vin[2]), .Hin(H0[1]),  .reg_mode(reg_mode[0][2]), .clk(clk), .rst(rst));
sa_pe sa_pe03 (.Vout(V0_1[3]), .Hout(H0[3]), .Vin(Vin[3]), .Hin(H0[2]),  .reg_mode(reg_mode[0][3]), .clk(clk), .rst(rst));
sa_pe sa_pe04 (.Vout(V0_1[4]), .Hout(H0[4]), .Vin(Vin[4]), .Hin(H0[3]),  .reg_mode(reg_mode[0][4]), .clk(clk), .rst(rst));
sa_pe sa_pe05 (.Vout(V0_1[5]), .Hout(H0[5]), .Vin(Vin[5]), .Hin(H0[4]),  .reg_mode(reg_mode[0][5]), .clk(clk), .rst(rst));
sa_pe sa_pe06 (.Vout(V0_1[6]), .Hout(H0[6]), .Vin(Vin[6]), .Hin(H0[5]),  .reg_mode(reg_mode[0][6]), .clk(clk), .rst(rst));
sa_pe sa_pe07 (.Vout(V0_1[7]), .Hout(H0[7]), .Vin(Vin[7]), .Hin(H0[6]),  .reg_mode(reg_mode[0][7]), .clk(clk), .rst(rst));

sa_pe sa_pe10 (.Vout(V1_2[0]), .Hout(H1[0]), .Vin(V0_1[0]), .Hin(Hin[1]), .reg_mode(reg_mode[1][0]), .clk(clk), .rst(rst));
sa_pe sa_pe11 (.Vout(V1_2[1]), .Hout(H1[1]), .Vin(V0_1[1]), .Hin(H1[0]),  .reg_mode(reg_mode[1][1]), .clk(clk), .rst(rst));
sa_pe sa_pe12 (.Vout(V1_2[2]), .Hout(H1[2]), .Vin(V0_1[2]), .Hin(H1[1]),  .reg_mode(reg_mode[1][2]), .clk(clk), .rst(rst));
sa_pe sa_pe13 (.Vout(V1_2[3]), .Hout(H1[3]), .Vin(V0_1[3]), .Hin(H1[2]),  .reg_mode(reg_mode[1][3]), .clk(clk), .rst(rst));
sa_pe sa_pe14 (.Vout(V1_2[4]), .Hout(H1[4]), .Vin(V0_1[4]), .Hin(H1[3]),  .reg_mode(reg_mode[1][4]), .clk(clk), .rst(rst));
sa_pe sa_pe15 (.Vout(V1_2[5]), .Hout(H1[5]), .Vin(V0_1[5]), .Hin(H1[4]),  .reg_mode(reg_mode[1][5]), .clk(clk), .rst(rst));
sa_pe sa_pe16 (.Vout(V1_2[6]), .Hout(H1[6]), .Vin(V0_1[6]), .Hin(H1[5]),  .reg_mode(reg_mode[1][6]), .clk(clk), .rst(rst));
sa_pe sa_pe17 (.Vout(V1_2[7]), .Hout(H1[7]), .Vin(V0_1[7]), .Hin(H1[6]),  .reg_mode(reg_mode[1][7]), .clk(clk), .rst(rst));

sa_pe sa_pe20 (.Vout(V2_3[0]), .Hout(H2[0]), .Vin(V1_2[0]), .Hin(Hin[2]), .reg_mode(reg_mode[2][0]), .clk(clk), .rst(rst));
sa_pe sa_pe21 (.Vout(V2_3[1]), .Hout(H2[1]), .Vin(V1_2[1]), .Hin(H2[0]),  .reg_mode(reg_mode[2][1]), .clk(clk), .rst(rst));
sa_pe sa_pe22 (.Vout(V2_3[2]), .Hout(H2[2]), .Vin(V1_2[2]), .Hin(H2[1]),  .reg_mode(reg_mode[2][2]), .clk(clk), .rst(rst));
sa_pe sa_pe23 (.Vout(V2_3[3]), .Hout(H2[3]), .Vin(V1_2[3]), .Hin(H2[2]),  .reg_mode(reg_mode[2][3]), .clk(clk), .rst(rst));
sa_pe sa_pe24 (.Vout(V2_3[4]), .Hout(H2[4]), .Vin(V1_2[4]), .Hin(H2[3]),  .reg_mode(reg_mode[2][4]), .clk(clk), .rst(rst));
sa_pe sa_pe25 (.Vout(V2_3[5]), .Hout(H2[5]), .Vin(V1_2[5]), .Hin(H2[4]),  .reg_mode(reg_mode[2][5]), .clk(clk), .rst(rst));
sa_pe sa_pe26 (.Vout(V2_3[6]), .Hout(H2[6]), .Vin(V1_2[6]), .Hin(H2[5]),  .reg_mode(reg_mode[2][6]), .clk(clk), .rst(rst));
sa_pe sa_pe27 (.Vout(V2_3[7]), .Hout(H2[7]), .Vin(V1_2[7]), .Hin(H2[6]),  .reg_mode(reg_mode[2][7]), .clk(clk), .rst(rst));

sa_pe sa_pe30 (.Vout(V3_4[0]), .Hout(H3[0]), .Vin(V2_3[0]), .Hin(Hin[3]), .reg_mode(reg_mode[3][0]), .clk(clk), .rst(rst));
sa_pe sa_pe31 (.Vout(V3_4[1]), .Hout(H3[1]), .Vin(V2_3[1]), .Hin(H3[0]),  .reg_mode(reg_mode[3][1]), .clk(clk), .rst(rst));
sa_pe sa_pe32 (.Vout(V3_4[2]), .Hout(H3[2]), .Vin(V2_3[2]), .Hin(H3[1]),  .reg_mode(reg_mode[3][2]), .clk(clk), .rst(rst));
sa_pe sa_pe33 (.Vout(V3_4[3]), .Hout(H3[3]), .Vin(V2_3[3]), .Hin(H3[2]),  .reg_mode(reg_mode[3][3]), .clk(clk), .rst(rst));
sa_pe sa_pe34 (.Vout(V3_4[4]), .Hout(H3[4]), .Vin(V2_3[4]), .Hin(H3[3]),  .reg_mode(reg_mode[3][4]), .clk(clk), .rst(rst));
sa_pe sa_pe35 (.Vout(V3_4[5]), .Hout(H3[5]), .Vin(V2_3[5]), .Hin(H3[4]),  .reg_mode(reg_mode[3][5]), .clk(clk), .rst(rst));
sa_pe sa_pe36 (.Vout(V3_4[6]), .Hout(H3[6]), .Vin(V2_3[6]), .Hin(H3[5]),  .reg_mode(reg_mode[3][6]), .clk(clk), .rst(rst));
sa_pe sa_pe37 (.Vout(V3_4[7]), .Hout(H3[7]), .Vin(V2_3[7]), .Hin(H3[6]),  .reg_mode(reg_mode[3][7]), .clk(clk), .rst(rst));

sa_pe sa_pe40 (.Vout(V4_5[0]), .Hout(H4[0]), .Vin(V3_4[0]), .Hin(Hin[4]), .reg_mode(reg_mode[4][0]), .clk(clk), .rst(rst));
sa_pe sa_pe41 (.Vout(V4_5[1]), .Hout(H4[1]), .Vin(V3_4[1]), .Hin(H4[0]),  .reg_mode(reg_mode[4][1]), .clk(clk), .rst(rst));
sa_pe sa_pe42 (.Vout(V4_5[2]), .Hout(H4[2]), .Vin(V3_4[2]), .Hin(H4[1]),  .reg_mode(reg_mode[4][2]), .clk(clk), .rst(rst));
sa_pe sa_pe43 (.Vout(V4_5[3]), .Hout(H4[3]), .Vin(V3_4[3]), .Hin(H4[2]),  .reg_mode(reg_mode[4][3]), .clk(clk), .rst(rst));
sa_pe sa_pe44 (.Vout(V4_5[4]), .Hout(H4[4]), .Vin(V3_4[4]), .Hin(H4[3]),  .reg_mode(reg_mode[4][4]), .clk(clk), .rst(rst));
sa_pe sa_pe45 (.Vout(V4_5[5]), .Hout(H4[5]), .Vin(V3_4[5]), .Hin(H4[4]),  .reg_mode(reg_mode[4][5]), .clk(clk), .rst(rst));
sa_pe sa_pe46 (.Vout(V4_5[6]), .Hout(H4[6]), .Vin(V3_4[6]), .Hin(H4[5]),  .reg_mode(reg_mode[4][6]), .clk(clk), .rst(rst));
sa_pe sa_pe47 (.Vout(V4_5[7]), .Hout(H4[7]), .Vin(V3_4[7]), .Hin(H4[6]),  .reg_mode(reg_mode[4][7]), .clk(clk), .rst(rst));

sa_pe sa_pe50 (.Vout(V5_6[0]), .Hout(H5[0]), .Vin(V4_5[0]), .Hin(Hin[5]), .reg_mode(reg_mode[5][0]), .clk(clk), .rst(rst));
sa_pe sa_pe51 (.Vout(V5_6[1]), .Hout(H5[1]), .Vin(V4_5[1]), .Hin(H5[0]),  .reg_mode(reg_mode[5][1]), .clk(clk), .rst(rst));
sa_pe sa_pe52 (.Vout(V5_6[2]), .Hout(H5[2]), .Vin(V4_5[2]), .Hin(H5[1]),  .reg_mode(reg_mode[5][2]), .clk(clk), .rst(rst));
sa_pe sa_pe53 (.Vout(V5_6[3]), .Hout(H5[3]), .Vin(V4_5[3]), .Hin(H5[2]),  .reg_mode(reg_mode[5][3]), .clk(clk), .rst(rst));
sa_pe sa_pe54 (.Vout(V5_6[4]), .Hout(H5[4]), .Vin(V4_5[4]), .Hin(H5[3]),  .reg_mode(reg_mode[5][4]), .clk(clk), .rst(rst));
sa_pe sa_pe55 (.Vout(V5_6[5]), .Hout(H5[5]), .Vin(V4_5[5]), .Hin(H5[4]),  .reg_mode(reg_mode[5][5]), .clk(clk), .rst(rst));
sa_pe sa_pe56 (.Vout(V5_6[6]), .Hout(H5[6]), .Vin(V4_5[6]), .Hin(H5[5]),  .reg_mode(reg_mode[5][6]), .clk(clk), .rst(rst));
sa_pe sa_pe57 (.Vout(V5_6[7]), .Hout(H5[7]), .Vin(V4_5[7]), .Hin(H5[6]),  .reg_mode(reg_mode[5][7]), .clk(clk), .rst(rst));

sa_pe sa_pe60 (.Vout(V6_7[0]), .Hout(H6[0]), .Vin(V5_6[0]), .Hin(Hin[6]), .reg_mode(reg_mode[6][0]), .clk(clk), .rst(rst));
sa_pe sa_pe61 (.Vout(V6_7[1]), .Hout(H6[1]), .Vin(V5_6[1]), .Hin(H6[0]),  .reg_mode(reg_mode[6][1]), .clk(clk), .rst(rst));
sa_pe sa_pe62 (.Vout(V6_7[2]), .Hout(H6[2]), .Vin(V5_6[2]), .Hin(H6[1]),  .reg_mode(reg_mode[6][2]), .clk(clk), .rst(rst));
sa_pe sa_pe63 (.Vout(V6_7[3]), .Hout(H6[3]), .Vin(V5_6[3]), .Hin(H6[2]),  .reg_mode(reg_mode[6][3]), .clk(clk), .rst(rst));
sa_pe sa_pe64 (.Vout(V6_7[4]), .Hout(H6[4]), .Vin(V5_6[4]), .Hin(H6[3]),  .reg_mode(reg_mode[6][4]), .clk(clk), .rst(rst));
sa_pe sa_pe65 (.Vout(V6_7[5]), .Hout(H6[5]), .Vin(V5_6[5]), .Hin(H6[4]),  .reg_mode(reg_mode[6][5]), .clk(clk), .rst(rst));
sa_pe sa_pe66 (.Vout(V6_7[6]), .Hout(H6[6]), .Vin(V5_6[6]), .Hin(H6[5]),  .reg_mode(reg_mode[6][6]), .clk(clk), .rst(rst));
sa_pe sa_pe67 (.Vout(V6_7[7]), .Hout(H6[7]), .Vin(V5_6[7]), .Hin(H6[6]),  .reg_mode(reg_mode[6][7]), .clk(clk), .rst(rst));

sa_pe sa_pe70 (.Vout(Vout[0]), .Hout(H7[0]), .Vin(V6_7[0]), .Hin(Hin[7]), .reg_mode(reg_mode[7][0]), .clk(clk), .rst(rst));
sa_pe sa_pe71 (.Vout(Vout[1]), .Hout(H7[1]), .Vin(V6_7[1]), .Hin(H7[0]),  .reg_mode(reg_mode[7][1]), .clk(clk), .rst(rst));
sa_pe sa_pe72 (.Vout(Vout[2]), .Hout(H7[2]), .Vin(V6_7[2]), .Hin(H7[1]),  .reg_mode(reg_mode[7][2]), .clk(clk), .rst(rst));
sa_pe sa_pe73 (.Vout(Vout[3]), .Hout(H7[3]), .Vin(V6_7[3]), .Hin(H7[2]),  .reg_mode(reg_mode[7][3]), .clk(clk), .rst(rst));
sa_pe sa_pe74 (.Vout(Vout[4]), .Hout(H7[4]), .Vin(V6_7[4]), .Hin(H7[3]),  .reg_mode(reg_mode[7][4]), .clk(clk), .rst(rst));
sa_pe sa_pe75 (.Vout(Vout[5]), .Hout(H7[5]), .Vin(V6_7[5]), .Hin(H7[4]),  .reg_mode(reg_mode[7][5]), .clk(clk), .rst(rst));
sa_pe sa_pe76 (.Vout(Vout[6]), .Hout(H7[6]), .Vin(V6_7[6]), .Hin(H7[5]),  .reg_mode(reg_mode[7][6]), .clk(clk), .rst(rst));
sa_pe sa_pe77 (.Vout(Vout[7]), .Hout(H7[7]), .Vin(V6_7[7]), .Hin(H7[6]),  .reg_mode(reg_mode[7][7]), .clk(clk), .rst(rst));

endmodule
