`include "configuration.vh"
`include "test_util.vh"
module sa_tb();

integer i;
wire [`DCT_CHANNEL_RNG] Vout [`PIXEL_BUS_RNG];
reg  [`DCT_CHANNEL_RNG] Hin  [`PIXEL_BUS_RNG];
reg  [`DCT_CHANNEL_RNG] Vin  [`PIXEL_BUS_RNG];
reg  [`PIXEL_BUS_RNG] reg_mode [`PIXEL_BUS_RNG];
reg  clk;
reg  rst;

sys_array sa_inst(
    .Vout(Vout),
    .Vin(Vin),
    .Hin(Hin),
    .reg_mode(reg_mode),
    .clk(clk),
    .rst(rst)
);


initial begin
    clk = 0;
end

always #(`P/2) clk = ~clk;

initial begin
    // Initialize
    rst = 0;
    for(i = 0; i < `PIXEL_BUS_CNT; i = i + 1) begin
        Vin[i] = 0;
        Hin[i] = 0;
        reg_mode[i] = 0;
    end
    #(`P);
    rst = 1;
    #(`P*2);
    Vin = '{7, 6, 5, 4, 3, 2, 1, 0};
    Hin = '{7, 6, 5, 4, 3, 2, 1, 0};
    while (Vout[7] == 0) #(`P);
    #(`P*4);
    $display("COMPLETE");
    $finish;
end

endmodule