`include "../src/dct_sa.v"
`include "../src/configuration.vh"
`include "test_util.vh"
module dct_sa_tb();


wire [`DCT_ROW_RNG] out_channel;
wire [`PIXEL_BUS_RNG] ready;
reg [`YCBCR_ROW_RNG] in_channel;
reg [`PIXEL_BUS_RNG] pixel_row_sel;
reg clk;
reg rst;
wire [`DCT_CHANNEL_RNG] VoutA [`PIXEL_BUS_RNG];
assign {VoutA[7], VoutA[6], VoutA[5], VoutA[4], VoutA[3], VoutA[2], VoutA[1], VoutA[0]} = out_channel;

dct_sa dct_sa_inst(
    .out_channel(out_channel),
    .ready(ready),
    .in_channel(in_channel),
    .pixel_row_sel(pixel_row_sel),
    .clk(clk),
    .rst(rst)
);

initial begin
    clk = 0;
end

always #(`P/2) clk = ~clk;
// static int Block[MCU_DIM] = {
//     { { 52 }, { 55 }, { 61 }, { 66  }, { 70  }, { 61  }, { 64 }, { 73 } },
//     { { 63 }, { 59 }, { 55 }, { 90  }, { 109 }, { 85  }, { 69 }, { 72 } },
//     { { 62 }, { 59 }, { 68 }, { 113 }, { 144 }, { 104 }, { 66 }, { 73 } },
//     { { 63 }, { 58 }, { 71 }, { 122 }, { 154 }, { 106 }, { 70 }, { 69 } },
//     { { 67 }, { 61 }, { 68 }, { 104 }, { 126 }, { 88  }, { 68 }, { 70 } },
//     { { 79 }, { 65 }, { 60 }, { 70  }, { 77  }, { 68  }, { 58 }, { 75 } },
//     { { 85 }, { 71 }, { 64 }, { 59  }, { 55  }, { 61  }, { 65 }, { 83 } },
//     { { 87 }, { 79 }, { 69 }, { 68  }, { 65  }, { 76  }, { 78 }, { 94 } },
// };
initial begin
    $dumpfile("tb.vcd");
    $dumpvars(0, dct_sa_tb);
    // Initialize
    rst = 0;
    pixel_row_sel = 0;
    in_channel = 0;
    #(`P);
    rst = 1;
    #(`P*2);
    pixel_row_sel[0] = 1;
    in_channel = { 16'd73, 16'd64, 16'd61, 16'd70, 16'd66, 16'd61, 16'd55, 16'd52 };
    #(`P);
    pixel_row_sel[0] = 0;
    pixel_row_sel[1] = 1;
    in_channel = { 16'd72, 16'd69, 16'd85, 16'd109, 16'd90, 16'd55, 16'd59, 16'd63 };
    #(`P);
    pixel_row_sel[1] = 0;
    pixel_row_sel[2] = 1;
    in_channel = { 16'd73, 16'd66, 16'd104, 16'd144, 16'd113, 16'd68, 16'd59, 16'd62 };
    #(`P);
    pixel_row_sel[2] = 0;
    pixel_row_sel[3] = 1;
    in_channel = { 16'd69, 16'd70, 16'd106, 16'd154, 16'd122, 16'd71, 16'd58, 16'd63 };
    #(`P);
    pixel_row_sel[3] = 0;
    pixel_row_sel[4] = 1;
    in_channel = { 16'd70, 16'd68, 16'd88, 16'd126, 16'd104, 16'd68, 16'd61, 16'd67 };
    #(`P);
    pixel_row_sel[4] = 0;
    pixel_row_sel[5] = 1;
    in_channel = { 16'd75, 16'd58, 16'd68, 16'd77, 16'd70, 16'd60, 16'd65, 16'd79 };
    #(`P);
    pixel_row_sel[5] = 0;
    pixel_row_sel[6] = 1;
    in_channel = { 16'd83, 16'd65, 16'd61, 16'd55, 16'd59, 16'd64, 16'd71, 16'd85 };
    #(`P);
    pixel_row_sel[6] = 0;
    pixel_row_sel[7] = 1;
    in_channel = { 16'd94, 16'd78, 16'd76, 16'd65, 16'd68, 16'd69, 16'd79, 16'd87 };
    #(`P);
    pixel_row_sel[7] = 0;
    in_channel = 0;
    while(ready != 1'b1) #(`P);
    $display("COMPLETE");
    $finish;
end

endmodule