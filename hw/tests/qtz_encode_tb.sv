`include "test_util.vh"
`include "configuration.vh"

module qtz_encode_tb ();

integer i, j;
// Outputs
output reg [`QTZ_CHANNEL_RNG] qtz_omem_y_channel [`PIXEL_BUS_RNG],
output reg [`QTZ_CHANNEL_RNG] qtz_omem_cb_channel [`PIXEL_BUS_RNG],
output reg [`QTZ_CHANNEL_RNG] qtz_omem_cr_channel [`PIXEL_BUS_RNG],
output reg out_ready,

// Inputs
input wire [`DCT_CHANNEL_RNG] dct_qtz_y_channel [`PIXEL_BUS_RNG],
input wire [`DCT_CHANNEL_RNG] dct_qtz_cb_channel [`PIXEL_BUS_RNG],
input wire [`DCT_CHANNEL_RNG] dct_qtz_cr_channel [`PIXEL_BUS_RNG],
input wire [`QTZ_MTX_RNG]     qtz_mtx  [`PIXEL_BUS_RNG][`PIXEL_BUS_RNG],
input wire                    start,
input wire                    clk,
input wire                    rst

qtz_decode qtz_decode_tb(
    .qtz_omem_y_channel(qtz_omem_y_channel),
    .qtz_omem_cb_channel(qtz_omem_cb_channel),
    .qtz_omem_cr_channel(qtz_omem_cr_channel),
    .out_ready(out_ready),
    .dct_qtz_y_channel(dct_qtz_y_channel),
    .dct_qtz_cb_channel(dct_qtz_cb_channel),
    .dct_qtz_cr_channel(dct_qtz_cr_channel),
    .qtz_mtx_y(qtz_mtx),
    .qtz_mtx_cb(qtz_mtx),
    .qtz_mtx_cr(qtz_mtx),
    .start(start),
    .clk(clk),
    .rst(rst)
);

initial begin
    clk = 0;
end

always #(`P/2) clk = ~clk;

initial begin
    rst = 0;
    for(i = 0; i < `PIXEL_BUS_CNT; i = i + 1) begin
        dct_qtz_y_channel[i] = 0;
        dct_qtz_cb_channel[i] = 0;
        dct_qtz_cr_channel[i] = 0;
        for(j = 0; j < `PIXEL_BUS_CNT; j = j + 1) begin
            qtz_mtx[i][j] = 0;
        end
    end
    start = 0;
    #(`P*2);
    rst = 1;
    #(`P*2);
    start = 1;
    qtz_mtx = '{
    '{ 16, 11, 10, 16, 24,  40,  51,  61  },
    '{ 12, 12, 14, 19, 26,  58,  60,  55  },
    '{ 14, 13, 16, 24, 40,  57,  69,  56  },
    '{ 14, 17, 22, 29, 51,  87,  80,  62  },
    '{ 18, 22, 37, 56, 68,  109, 103, 77  },
    '{ 24, 35, 55, 64, 81,  104, 113, 92  },
    '{ 49, 64, 78, 87, 103, 121, 120, 101 },
    '{ 72, 92, 95, 98, 112, 100, 103, 99  }
    };
    dct_qtz_y_channel  = '{0, 0, 0, 0, 0, 0, 0, 0};
    dct_qtz_cb_channel = '{0, 0, 0, 0, 0, 0, 0, 0};
    dct_qtz_cr_channel = '{0, 0, 0, 0, 0, 0, 0, 0};
    #(`P);
    dct_qtz_y_channel  = '{0, 0, 0, 0, 0, 0, 0, 0};
    dct_qtz_cb_channel = '{0, 0, 0, 0, 0, 0, 0, 0};
    dct_qtz_cr_channel = '{0, 0, 0, 0, 0, 0, 0, 0};
    #(`P);
    dct_qtz_y_channel  = '{0, 0, 0, 0, 0, 0, 0, 0};
    dct_qtz_cb_channel = '{0, 0, 0, 0, 0, 0, 0, 0};
    dct_qtz_cr_channel = '{0, 0, 0, 0, 0, 0, 0, 0};
    #(`P);
    dct_qtz_y_channel  = '{1, 0, 0, 0, 0, 0, 0, 0};
    dct_qtz_cb_channel = '{1, 0, 0, 0, 0, 0, 0, 0};
    dct_qtz_cr_channel = '{1, 0, 0, 0, 0, 0, 0, 0};
    #(`P);
    dct_qtz_y_channel  = '{-3, 1, 2, -1, 0, 0, 0, 0};
    dct_qtz_cb_channel = '{-3, 1, 2, -1, 0, 0, 0, 0};
    dct_qtz_cr_channel = '{-3, 1, 2, -1, 0, 0, 0, 0};
    #(`P);
    dct_qtz_y_channel  = '{-3, 1, 5, -1, -1, 0, 0, 0};
    dct_qtz_cb_channel = '{-3, 1, 5, -1, -1, 0, 0, 0};
    dct_qtz_cr_channel = '{-3, 1, 5, -1, -1, 0, 0, 0};
    #(`P);
    dct_qtz_y_channel  = '{0, -2, -4, 1, 1, 0, 0, 0};
    dct_qtz_cb_channel = '{0, -2, -4, 1, 1, 0, 0, 0};
    dct_qtz_cr_channel = '{0, -2, -4, 1, 1, 0, 0, 0};
    #(`P);
    dct_qtz_y_channel  = '{-26, -3, -6, 2, 2, -1, 0, 0};
    dct_qtz_cb_channel = '{-26, -3, -6, 2, 2, -1, 0, 0};
    dct_qtz_cr_channel = '{-26, -3, -6, 2, 2, -1, 0, 0};
    #(`P);

    $finish;
end

endmodule
