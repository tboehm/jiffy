`include "../src/sys_array.v"
`include "../src/configuration.vh"
`include "test_util.vh"
module sa_pe_tb();

wire [`DCT_CHANNEL_RNG] Vout;
wire [`DCT_CHANNEL_RNG] Hout;

reg [`DCT_CHANNEL_RNG] Vin;    
reg [`DCT_CHANNEL_RNG] Hin;
reg reg_mode; 
reg clk;
reg rst;

sa_pe sa_pe_inst(
    .Vout(Vout),
    .Hout(Hout),
    .Vin(Vin),
    .Hin(Hin),
    .reg_mode(reg_mode),
    .clk(clk),
    .rst(rst)
);


initial begin
    clk = 0;
end

always #(`P/2) clk = ~clk;

initial begin
    $dumpfile("tb.vcd");
    $dumpvars(0, sa_pe_tb);
    // Initialize
    rst = 0;
    Vin = 0;
    Hin = 0;
    reg_mode = 0;
    #(`P);
    rst = 1;
    #(`P*2);
    Vin = 23170;
    Hin = 52;
    #(`P);
    Hin=55;
    #(`P);
    Hin=61;
    #(`P);
    Hin=66;
    #(`P);
    Hin=70;
    #(`P);
    Hin=61;
    #(`P);
    Hin=64;
    #(`P);
    Hin=73;
    #(`P);
    Vin = 0;
    reg_mode = 1;
    Hin=23170;
    
    #(`P*2);
    $display("COMPLETE");
    $finish;
end

endmodule