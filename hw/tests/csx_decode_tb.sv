`include "test_util.vh"
`include "../src/configuration.vh"

module csx_decode_tb ();

integer i;
// Outputs
wire [`YCBCR_CHANNEL_RNG] csx_omem_r_channel [`PIXEL_BUS_RNG];
wire [`YCBCR_CHANNEL_RNG] csx_omem_g_channel [`PIXEL_BUS_RNG];
wire [`YCBCR_CHANNEL_RNG] csx_omem_b_channel [`PIXEL_BUS_RNG];
wire out_ready;

// Inputs
reg [`RGB_CHANNEL_RNG] dct_csx_y_channel  [`PIXEL_BUS_RNG];
reg [`RGB_CHANNEL_RNG] dct_csx_cb_channel [`PIXEL_BUS_RNG];
reg [`RGB_CHANNEL_RNG] dct_csx_cr_channel [`PIXEL_BUS_RNG];
reg start;
reg clk;
reg rst;
 
csx_decode csx_decode_tb(
    .csx_omem_r_channel(csx_omem_r_channel),
    .csx_omem_g_channel(csx_omem_g_channel),
    .csx_omem_b_channel(csx_omem_b_channel),
    .dct_csx_y_channel(dct_csx_y_channel),
    .dct_csx_cb_channel(dct_csx_cb_channel),
    .dct_csx_cr_channel(dct_csx_cr_channel),
    .start(start),
    .out_ready(out_ready),
    .clk(clk),
    .rst(rst)
);

initial begin
    clk = 0;
end

always #(`P/2) clk = ~clk;

initial begin
    // Initialize
    rst = 0;
    for(i = 0; i < `PIXEL_BUS_CNT; i = i + 1) begin
        dct_csx_y_channel[i] = 0;
        dct_csx_cb_channel[i] = 0;
        dct_csx_cr_channel[i] = 0;
    end
    start = 0;
    #`P*2;
    rst = 1;
    #`P*2;
    dct_csx_y_channel  = {0, 1, 2, 3, 4, 5, 6, 7};
    dct_csx_cb_channel = {0, 1, 2, 3, 4, 5, 6, 7};
    dct_csx_cr_channel = {0, 1, 2, 3, 4, 5, 6, 7};
    start = 1;
    #`P;
    dct_csx_y_channel  = {0, 255, 0, 255, 0, 255, 0, 255};
    dct_csx_cb_channel = {0, 255, 0, 255, 0, 255, 0, 255};
    dct_csx_cr_channel = {0, 255, 0, 255, 0, 255, 0, 255};
    #`P;
    dct_csx_y_channel  = {0, 0, 0, 0, 0, 0, 0, 0};
    dct_csx_cb_channel = {0, 0, 0, 0, 0, 0, 0, 0};
    dct_csx_cr_channel = {0, 0, 0, 0, 0, 0, 0, 0};
    #`P;
    dct_csx_y_channel  = {255, 255, 255, 255, 255, 255, 255, 255};
    dct_csx_cb_channel = {255, 255, 255, 255, 255, 255, 255, 255};
    dct_csx_cr_channel = {255, 255, 255, 255, 255, 255, 255, 255};
    #`P;
    dct_csx_y_channel  = {0, 1, 2, 3, 4, 5, 6, 7};
    dct_csx_cb_channel = {8, 9, 10, 11, 12, 13, 14, 15};
    dct_csx_cr_channel = {16, 17, 18, 19, 20, 21, 22, 23};
    #`P;
    dct_csx_y_channel  = {0, 0, 0, 0, 0, 0, 0, 0};
    dct_csx_cb_channel = {128, 128, 128, 128, 128, 128, 128, 128};
    dct_csx_cr_channel = {255, 255, 255, 255, 255, 255, 255, 255};
    #`P;
    dct_csx_y_channel  = {0, 0, 0, 0, 0, 0, 0, 0};
    dct_csx_cb_channel = {0, 0, 0, 0, 0, 0, 0, 0};
    dct_csx_cr_channel = {0, 0, 0, 0, 0, 0, 0, 0};
    #`P;
    dct_csx_y_channel  = {0, 0, 0, 0, 0, 0, 0, 0};
    dct_csx_cb_channel = {0, 0, 0, 0, 0, 0, 0, 0};
    dct_csx_cr_channel = {0, 0, 0, 0, 0, 0, 0, 0};
    #`P*4;
    $finish;
end

endmodule

