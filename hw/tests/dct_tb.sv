`include "test_util.vh"
`include "configuration.vh"

module dct_tb ();

integer i;

// Outputs
wire [`DCT_CHANNEL_RNG] dct_out_y_channel [`PIXEL_BUS_RNG];
wire [`DCT_CHANNEL_RNG] dct_out_cb_channel[`PIXEL_BUS_RNG];
wire [`DCT_CHANNEL_RNG] dct_out_cr_channel[`PIXEL_BUS_RNG];
wire out_ready;

// Inputs
reg [`DCT_CHANNEL_RNG] dct_in_y_channel [`PIXEL_BUS_RNG];
reg [`DCT_CHANNEL_RNG] dct_in_cb_channel[`PIXEL_BUS_RNG];
reg [`DCT_CHANNEL_RNG] dct_in_cr_channel[`PIXEL_BUS_RNG];
reg code_mode;
reg start;
reg clk;
reg rst;
reg [`DCT_CHANNEL_RNG] val;
dct dct_inst
(
    .dct_out_y_channel(dct_out_y_channel),
    .dct_out_cb_channel(dct_out_cb_channel),
    .dct_out_cr_channel(dct_out_cr_channel),
    .out_ready(out_ready),
    .dct_in_y_channel(dct_in_y_channel),
    .dct_in_cb_channel(dct_in_cb_channel),
    .dct_in_cr_channel(dct_in_cr_channel),
    .code_mode(code_mode),
    .start(start),
    .clk(clk),
    .rst(rst)
);

initial begin
    clk = 0;
end

always #(`P/2) clk = ~clk;

initial begin
    // Initialize
    rst = 0;
    start = 0;
    code_mode = 0;
    val = 127 << 8;

    #(`P*2);
    rst = 1;
    #(`P*2);
    start = 1'b1;
    for(i = 0; i < `PIXEL_BUS_CNT; i = i + 1) begin
        dct_in_y_channel[i] =  '{ val, val, val, val, val, val, val, val };
        dct_in_cb_channel[i] = '{ val, val, val, val, val, val, val, val };
        dct_in_cr_channel[i] = '{ val, val, val, val, val, val, val, val };
    end
    #(`P);
        for(i = 0; i < `PIXEL_BUS_CNT; i = i + 1) begin
        dct_in_y_channel[i] =  '{ 0,0,0,0,0,0,0,0 };
        dct_in_cb_channel[i] = '{ 0,0,0,0,0,0,0,0 };
        dct_in_cr_channel[i] = '{ 0,0,0,0,0,0,0,0 };
    end
    #(`P);
        for(i = 0; i < `PIXEL_BUS_CNT; i = i + 1) begin
        dct_in_y_channel[i] =  '{ val, val, val, val, val, val, val, val };
        dct_in_cb_channel[i] = '{ val, val, val, val, val, val, val, val };
        dct_in_cr_channel[i] = '{ val, val, val, val, val, val, val, val };
    end
    #(`P);
        for(i = 0; i < `PIXEL_BUS_CNT; i = i + 1) begin
        dct_in_y_channel[i] =  '{ 0,0,0,0,0,0,0,0};
        dct_in_cb_channel[i] = '{0,0,0,0,0,0,0,0 };
        dct_in_cr_channel[i] = '{0,0,0,0,0,0,0,0 };
    end
    #(`P);
        for(i = 0; i < `PIXEL_BUS_CNT; i = i + 1) begin
        dct_in_y_channel[i] =  '{ val, val, val, val, val, val, val, val };
        dct_in_cb_channel[i] = '{ val, val, val, val, val, val, val, val };
        dct_in_cr_channel[i] = '{ val, val, val, val, val, val, val, val };
    end
    #(`P);
        for(i = 0; i < `PIXEL_BUS_CNT; i = i + 1) begin
        dct_in_y_channel[i] =  '{ 0,0,0,0,0,0,0,0};
        dct_in_cb_channel[i] = '{ 0,0,0,0,0,0,0,0 };
        dct_in_cr_channel[i] = '{ 0,0,0,0,0,0,0,0 };
    end
    #(`P);
        for(i = 0; i < `PIXEL_BUS_CNT; i = i + 1) begin
        dct_in_y_channel[i] =  '{ val, val, val, val, val, val, val, val };
        dct_in_cb_channel[i] = '{ val, val, val, val, val, val, val, val };
        dct_in_cr_channel[i] = '{ val, val, val, val, val, val, val, val };
    end
    #(`P);
        for(i = 0; i < `PIXEL_BUS_CNT; i = i + 1) begin
        dct_in_y_channel[i] =  '{ 0,0,0,0,0,0,0,0};
        dct_in_cb_channel[i] = '{0,0,0,0,0,0,0,0 };
        dct_in_cr_channel[i] = '{0,0,0,0,0,0,0,0 };
    end
    #(`P);
    while(!out_ready) #(`P);
    while(out_ready) #(`P);
    $finish;
end

endmodule