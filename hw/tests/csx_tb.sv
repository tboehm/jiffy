`include "../src/configuration.vh"
`define P 10

module csx_tb ();

// Outputs
wire [`YCBCR_CHANNEL_RNG] y_channel [`PIXEL_BUS_RNG];
wire [`YCBCR_CHANNEL_RNG] cb_channel [`PIXEL_BUS_RNG];
wire [`YCBCR_CHANNEL_RNG] cr_channel [`PIXEL_BUS_RNG];
wire encode_out_ready;

wire [`RGB_CHANNEL_RNG] csx_omem_r_channel [`PIXEL_BUS_RNG];
wire [`RGB_CHANNEL_RNG] csx_omem_g_channel [`PIXEL_BUS_RNG];
wire [`RGB_CHANNEL_RNG] csx_omem_b_channel [`PIXEL_BUS_RNG];
wire decode_out_ready;

// Inputs
reg [`RGB_CHANNEL_RNG] imem_csx_r_channel [`PIXEL_BUS_RNG];
reg [`RGB_CHANNEL_RNG] imem_csx_g_channel [`PIXEL_BUS_RNG];
reg [`RGB_CHANNEL_RNG] imem_csx_b_channel [`PIXEL_BUS_RNG];
reg                    start;
reg                    clk;
reg                    rst;

csx_encode csx_encode0(
    .csx_dct_y_channel(y_channel),
    .csx_dct_cb_channel(cb_channel),
    .csx_dct_cr_channel(cr_channel),
    .imem_csx_r_channel(imem_csx_r_channel),
    .imem_csx_g_channel(imem_csx_g_channel),
    .imem_csx_b_channel(imem_csx_b_channel),
    .start(start),
    .out_ready(encode_out_ready),
    .clk(clk),
    .rst(rst)
);

csx_decode csx_decode_tb(
    .csx_omem_r_channel(csx_omem_r_channel),
    .csx_omem_g_channel(csx_omem_g_channel),
    .csx_omem_b_channel(csx_omem_b_channel),
    .dct_csx_y_channel(y_channel),
    .dct_csx_cb_channel(cb_channel),
    .dct_csx_cr_channel(cr_channel),
    .start(encode_out_ready),
    .out_ready(decode_out_ready),
    .clk(clk),
    .rst(rst)
);

initial begin
    clk = 0;
end

always #(`P/2) clk = ~clk;

integer i;

initial begin
    // Initialize
    rst = 0;
    for(i = 0; i < `PIXEL_BUS_CNT; i = i + 1) begin
        imem_csx_r_channel[i] = 0;
        imem_csx_g_channel[i] = 0;
        imem_csx_b_channel[i] = 0;
    end
    start = 0;
    #(`P*2);
    rst = 1;
    #(`P*2);
    imem_csx_r_channel = {0, 1, 2, 3, 4, 5, 6, 7};
    imem_csx_g_channel = {0, 1, 2, 3, 4, 5, 6, 7};
    imem_csx_b_channel = {0, 1, 2, 3, 4, 5, 6, 7};
    start = 1;
    #`P;
    imem_csx_r_channel = {0, 255, 0, 255, 0, 255, 0, 255};
    imem_csx_g_channel = {0, 255, 0, 255, 0, 255, 0, 255};
    imem_csx_b_channel = {0, 255, 0, 255, 0, 255, 0, 255};
    #`P;
    imem_csx_r_channel = {0, 0, 0, 0, 0, 0, 0, 0};
    imem_csx_g_channel = {0, 0, 0, 0, 0, 0, 0, 0};
    imem_csx_b_channel = {0, 0, 0, 0, 0, 0, 0, 0};
    #`P;
    imem_csx_r_channel = {255, 255, 255, 255, 255, 255, 255, 255};
    imem_csx_g_channel = {255, 255, 255, 255, 255, 255, 255, 255};
    imem_csx_b_channel = {255, 255, 255, 255, 255, 255, 255, 255};
    #`P;
    imem_csx_r_channel = {0, 1, 2, 3, 4, 5, 6, 7};
    imem_csx_g_channel = {8, 9, 10, 11, 12, 13, 14, 15};
    imem_csx_b_channel = {16, 17, 18, 19, 20, 21, 22, 23};
    #`P;
    imem_csx_r_channel = {0, 0, 0, 0, 0, 0, 0, 0};
    imem_csx_g_channel = {128, 128, 128, 128, 128, 128, 128, 128};
    imem_csx_b_channel = {255, 255, 255, 255, 255, 255, 255, 255};
    #`P;
    imem_csx_r_channel = {0, 0, 0, 0, 0, 0, 0, 0};
    imem_csx_g_channel = {0, 0, 0, 0, 0, 0, 0, 0};
    imem_csx_b_channel = {0, 0, 0, 0, 0, 0, 0, 0};
    #`P;
    imem_csx_r_channel = {0, 0, 0, 0, 0, 0, 0, 0};
    imem_csx_g_channel = {0, 0, 0, 0, 0, 0, 0, 0};
    imem_csx_b_channel = {0, 0, 0, 0, 0, 0, 0, 0};
    #(`P*4);
    $finish;
end

endmodule

