`timescale 1ns / 1ps
`define P 10
`include "configuration.vh"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/14/2021 09:27:22 AM
// Design Name: 
// Module Name: jiffy_AXI_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module jiffy_AXI_tb();
// Users to add ports here
reg [31:0] din_BRAM;
wire [31:0] addr_BRAM;
wire clk_BRAM;
wire [31:0] dout_BRAM;
wire en_BRAM;
wire rst_BRAM;
wire [3:0] we_BRAM;
wire interrupt_out;

// User ports ends
// Do not modify the ports beyond this line

// Global Clock Signal
reg S_AXI_ACLK;
// Global Reset Signal. This Signal is Active LOW
reg S_AXI_ARESETN;

// AXI REGISTERS
reg [31:0] total_num_blocks; // Number of Blocks 
reg ctrl_code_mode, ctrl_reset_jiffy, ctrl_start_jiffy; // Control Registers (encode/decode mode, start, reset)
wire global_done, start_bram_read, buffer_sel; // Status Register (Done, Accelerator Blocking, Buffer (A/B) reading from)
reg wr_bram_ack; // Interrupt Clear

jiffy_v1_AXI jiffy_v1_AXI_inst (
	.din_BRAM(din_BRAM),
	.addr_BRAM(addr_BRAM),
	.clk_BRAM(clk_BRAM),
	.dout_BRAM(dout_BRAM),
	.en_BRAM(en_BRAM),
	.rst_BRAM(rst_BRAM),
	.we_BRAM(we_BRAM),
	.interrupt_out(interrupt_out),
	.S_AXI_ACLK(S_AXI_ACLK),
	.S_AXI_ARESETN(S_AXI_ARESETN),
	.total_num_blocks(total_num_blocks),
	.ctrl_code_mode(ctrl_code_mode),
	.ctrl_reset_jiffy(ctrl_reset_jiffy),
	.ctrl_start_jiffy(ctrl_start_jiffy),
	.global_done(global_done),
	.start_bram_read(start_bram_read),
	.buffer_sel(buffer_sel),
	.wr_bram_ack(wr_bram_ack)
);

initial begin
	S_AXI_ACLK = 0;
end

always #(`P/2) S_AXI_ACLK = ~S_AXI_ACLK;

initial begin
	// Initialize
	S_AXI_ARESETN = 0;
	din_BRAM = 0;
	total_num_blocks = 0;
	ctrl_code_mode = 1;
	ctrl_reset_jiffy = 0;
	ctrl_start_jiffy = 0;
	wr_bram_ack = 0;
	#(`P*2);
	S_AXI_ARESETN = 1;
	#(`P*2);
	total_num_blocks = 1;
	din_BRAM = `MAGIC_NUMBER;
	ctrl_start_jiffy = 1;
	while(!global_done) #(`P);
	wr_bram_ack = 1;
	ctrl_start_jiffy = 0;
	#(`P);
	ctrl_reset_jiffy = 1;
	#(`P);
	ctrl_reset_jiffy = 0;
	#(`P);
	ctrl_start_jiffy = 1;
	ctrl_code_mode = 0;
	while(!global_done) #(`P);
	#(`P*4);
	$finish;
end

endmodule
