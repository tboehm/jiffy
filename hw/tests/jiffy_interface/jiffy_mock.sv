`include "configuration.vh"
module jiffy (

    // Encode
    input wire [`RGB_CHANNEL_RNG]   imem_encode_r [`PIXEL_BUS_RNG],
    input wire [`RGB_CHANNEL_RNG]   imem_encode_g [`PIXEL_BUS_RNG],
    input wire [`RGB_CHANNEL_RNG]   imem_encode_b [`PIXEL_BUS_RNG],

    output reg [`QTZ_CHANNEL_RNG]   omem_encode_y  [`PIXEL_BUS_RNG],
    output reg [`QTZ_CHANNEL_RNG]   omem_encode_cb [`PIXEL_BUS_RNG],
    output reg [`QTZ_CHANNEL_RNG]   omem_encode_cr [`PIXEL_BUS_RNG],

    // Decode
    input wire [`QTZ_CHANNEL_RNG]   imem_decode_y  [`PIXEL_BUS_RNG],
    input wire [`QTZ_CHANNEL_RNG]   imem_decode_cb [`PIXEL_BUS_RNG],
    input wire [`QTZ_CHANNEL_RNG]   imem_decode_cr [`PIXEL_BUS_RNG],

    output reg [`RGB_CHANNEL_RNG]   omem_decode_r [`PIXEL_BUS_RNG],
    output reg [`RGB_CHANNEL_RNG]   omem_decode_g [`PIXEL_BUS_RNG],
    output reg [`RGB_CHANNEL_RNG]   omem_decode_b [`PIXEL_BUS_RNG],
    
    input                           code_mode,
    input                           start,
    output reg                      out_ready,
    input                           clk,
    input                           rst
);
integer i, pixel;
reg [3:0] jiffy_sm;
reg [`PIXEL_ROW_RNG] row_select;

reg [`RGB_CHANNEL_RNG] imem_encode_r_hold [`PIXEL_ROW_RNG][`PIXEL_ROW_RNG];
reg [`RGB_CHANNEL_RNG] imem_encode_g_hold [`PIXEL_ROW_RNG][`PIXEL_ROW_RNG];
reg [`RGB_CHANNEL_RNG] imem_encode_b_hold [`PIXEL_ROW_RNG][`PIXEL_ROW_RNG];

reg [`QTZ_CHANNEL_RNG] imem_decode_y_hold [`PIXEL_ROW_RNG][`PIXEL_ROW_RNG];
reg [`QTZ_CHANNEL_RNG] imem_decode_cb_hold [`PIXEL_ROW_RNG][`PIXEL_ROW_RNG];
reg [`QTZ_CHANNEL_RNG] imem_decode_cr_hold [`PIXEL_ROW_RNG][`PIXEL_ROW_RNG];

localparam 
    IDLE = 0,
    START = 1,
    WAIT = 2,
    OUT_READY = 3,
    OUT_STREAM = 4,
    HOLD = 5;
// DUMMY JIFFY TRANSACTION FOR TESTING
reg [3:0] DUMMY_WAIT;
reg [`PIXEL_ROW_RNG] out_row_select;


always @(posedge clk) begin
    if (rst) begin
        for(pixel = 0; pixel < `PIXEL_ROW_SIZE; pixel=pixel+1) begin
            omem_encode_y[pixel] <= 0;
            omem_encode_cb[pixel] <= 0;
            omem_encode_cr[pixel] <= 0;
            omem_decode_r[pixel] <= 0;
            omem_decode_g[pixel]  <= 0;
            omem_decode_b[pixel]  <= 0;
            for(i = 0; i < `PIXEL_ROW_SIZE; i = i + 1) begin
                imem_encode_r_hold[pixel][i] <= 8'b0;
                imem_encode_g_hold[pixel][i] <= 8'b0;
                imem_encode_b_hold[pixel][i] <= 8'b0;
                imem_decode_y_hold[pixel][i] <= 16'b0;
                imem_decode_cb_hold[pixel][i] <= 16'b0;
                imem_decode_cr_hold[pixel][i] <= 16'b0;
            end
        end
        out_ready <= 0;
        row_select <= 0;
        out_row_select <= 0;
        jiffy_sm <= IDLE;
        DUMMY_WAIT <= 4'h0;
    end else begin
        case (jiffy_sm)
            IDLE: begin
                for(pixel = 0; pixel < `PIXEL_ROW_SIZE; pixel=pixel+1) begin
                    omem_encode_y[pixel] <= 0;
                    omem_encode_cb[pixel] <= 0;
                    omem_encode_cr[pixel] <= 0;
                    omem_decode_r[pixel] <= 0;
                    omem_decode_g[pixel]  <= 0;
                    omem_decode_b[pixel]  <= 0;
                    for(i = 0; i < `PIXEL_ROW_SIZE; i = i + 1) begin
                        imem_encode_r_hold[pixel][i] <= 8'b0;
                        imem_encode_g_hold[pixel][i] <= 8'b0;
                        imem_encode_b_hold[pixel][i] <= 8'b0;
                        imem_decode_y_hold[pixel][i] <= 16'b0;
                        imem_decode_cb_hold[pixel][i] <= 16'b0;
                        imem_decode_cr_hold[pixel][i] <= 16'b0;
                    end
                end
                out_ready <= 0;
                row_select <= 0;
                out_row_select <= 0;
                jiffy_sm <= start ? START : IDLE;
                DUMMY_WAIT <= 4'hF;
            end 
            START: begin
                row_select <= row_select + 1;
                if(code_mode) begin
                    for(pixel = 0; pixel < `PIXEL_ROW_SIZE; pixel=pixel+1) begin
                        imem_encode_r_hold[row_select][pixel] <= imem_encode_r[pixel];
                        imem_encode_g_hold[row_select][pixel] <= imem_encode_g[pixel];
                        imem_encode_b_hold[row_select][pixel] <= imem_encode_b[pixel];
                    end
                end
                else begin
                    for(pixel = 0; pixel < `PIXEL_ROW_SIZE; pixel=pixel+1) begin
                        imem_decode_y_hold[row_select][pixel] <= imem_decode_y[pixel];
                        imem_decode_cb_hold[row_select][pixel] <= imem_decode_cb[pixel];
                        imem_decode_cr_hold[row_select][pixel] <= imem_decode_cr[pixel];
                    end
                end
                jiffy_sm <= (row_select == 3'b111) ? WAIT : START;
            end
            WAIT: begin
                DUMMY_WAIT <= DUMMY_WAIT - 1;
                jiffy_sm <= DUMMY_WAIT ? WAIT : OUT_READY;
            end
            OUT_READY: begin
                out_ready <= 1'b1;
                out_row_select <= 0;
                jiffy_sm <= OUT_STREAM;
            end
            OUT_STREAM: begin
                out_row_select <= out_row_select + 1;
                if(code_mode) begin
                    for(pixel = 0; pixel < `PIXEL_ROW_SIZE; pixel=pixel+1) begin
                        omem_decode_r[pixel] <= imem_decode_y_hold[out_row_select][pixel] + 1;
                        omem_decode_g[pixel] <= imem_decode_cb_hold[out_row_select][pixel] + 1;
                        omem_decode_b[pixel] <= imem_decode_cr_hold[out_row_select][pixel] + 1;
                    end
                end
                else begin
                    for(pixel = 0; pixel < `PIXEL_ROW_SIZE; pixel=pixel+1) begin
                        omem_encode_y[pixel] <= imem_encode_r_hold[out_row_select][pixel] + 1;
                        omem_encode_cb[pixel] <= imem_encode_g_hold[out_row_select][pixel] + 1;
                        omem_encode_cr[pixel] <= imem_encode_b_hold[out_row_select][pixel] + 1;
                    end
                end
                jiffy_sm <= (out_row_select == 3'b111) ? HOLD : OUT_STREAM; 
            end
            HOLD: begin
                jiffy_sm <= start ? HOLD : IDLE;
            end
            default: begin
                for(pixel = 0; pixel < `PIXEL_ROW_SIZE; pixel=pixel+1) begin
                    omem_encode_y[pixel] <= 0;
                    omem_encode_cb[pixel] <= 0;
                    omem_encode_cr[pixel] <= 0;
                    omem_decode_r[pixel] <= 0;
                    omem_decode_g[pixel]  <= 0;
                    omem_decode_b[pixel]  <= 0;
                    for(i = 0; i < `PIXEL_ROW_SIZE; i = i + 1) begin
                        imem_encode_r_hold[pixel][i] <= 8'b0;
                        imem_encode_g_hold[pixel][i] <= 8'b0;
                        imem_encode_b_hold[pixel][i] <= 8'b0;
                        imem_decode_y_hold[pixel][i] <= 16'b0;
                        imem_decode_cb_hold[pixel][i] <= 16'b0;
                        imem_decode_cr_hold[pixel][i] <= 16'b0;
                    end
                end
                out_ready <= 0;
                jiffy_sm <= IDLE;
                DUMMY_WAIT <= 4'h0;
                end
        endcase
    end
end

endmodule