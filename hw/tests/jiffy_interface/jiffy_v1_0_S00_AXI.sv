`include "configuration.vh"
`timescale 1 ns / 1 ps

	module jiffy_v1_AXI
	(
		// Users to add ports here
        input wire [31:0] din_BRAM,
		output reg [31:0] addr_BRAM,
		output wire clk_BRAM,
		output wire [31:0] dout_BRAM,
		output reg en_BRAM,
		output wire rst_BRAM,
		output wire [3:0] we_BRAM,
		output wire interrupt_out,

		// User ports ends
		// Do not modify the ports beyond this line

		// Global Clock Signal
		input wire  S_AXI_ACLK,
		// Global Reset Signal. This Signal is Active LOW
		input wire  S_AXI_ARESETN,
		
		// AXI REGISTERS
		input wire [31:0] total_num_blocks, // Number of Blocks 
	    input wire ctrl_code_mode, ctrl_reset_jiffy, ctrl_start_jiffy, // Control Registers (encode/decode mode, start, reset)
	    output reg global_done, start_bram_read, buffer_sel, // Status Register (Done, Accelerator Blocking, Buffer (A/B) reading from)
	    input wire wr_bram_ack // Interrupt Clear
);

	// ################################################################
	// #                                                              #
	// #                    JIFFY ACCELERATOR                         #
	// #                                                              #
	// ################################################################

	// Global Jiffy Controller
	integer i, j;
	// AXI Registers
	// wire ctrl_code_mode; 
	// wire ctrl_reset_jiffy; 
	// wire ctrl_start_jiffy;
	// wire [31:0] total_num_blocks;

	// Control registers
	// reg start_bram_read;
	reg start_jiffy;
	reg start_bram_write;
	// reg global_done;
	// wire wr_bram_ack;

	// Status from Other Controllers
	reg qtz_mtx_done;
	reg rd_bram_done;
	reg jiffy_done;
	reg wr_bram_done;

	// Registers
	reg [31:0] block_cnt; 
	// reg buffer_sel;
	reg code_mode;
	reg qtz_fetched;

	reg [3:0] global_sm;
	reg [31:0] qtz_mtx [`QTZ_ADDRESS_RNG];
	wire reset_jiffy;

	localparam 
		IDLE = 0,
		START_GLOBAL = 1,
		START_BRAM_READ = 2,
		BRAM_READ = 3,
		START_JIFFY = 4,
		JIFFY = 5,
		START_BRAM_WRITE = 6,
		BRAM_WRITE = 7,
		NEXT_BLOCK = 8,
		CHECK_DONE = 9,
		GLOBAL_DONE = 10,
		FETCH_QTZ_MTX = 11,
		FETCH_QTZ_WAIT = 12;

	localparam 
	    A_BUF = 0,
		B_BUF = 1;

	always @(posedge S_AXI_ACLK) begin
		if( ctrl_reset_jiffy | ~S_AXI_ARESETN ) begin
			start_bram_read <= 1'b0;
			start_jiffy <= 1'b0;
			start_bram_write <= 1'b0;
			global_done <= 1'b0;
			block_cnt <= 0;
			buffer_sel <= 1'b0;
			code_mode <= 1'b0;
			global_sm <= IDLE;
			qtz_fetched <= 1'b0;
			for(i = 0; i < `QTZ_TRANSFER_CNT; i = i + 1) qtz_mtx[i] <= 0;
		end
		else begin
			case (global_sm)
				IDLE: begin
					start_bram_read <= 1'b0;
					start_jiffy <= 1'b0;
					start_bram_write <= 1'b0;
					global_done <= 1'b0;
					block_cnt <= 0;
					buffer_sel <= 1'b0;
					code_mode <= 0;
					qtz_fetched <= 1'b0;
					for(i = 0; i < `QTZ_TRANSFER_CNT; i = i + 1) qtz_mtx[i] <= 0;
					global_sm <= ctrl_start_jiffy ? START_GLOBAL : IDLE;
				end 
				START_GLOBAL: begin
					block_cnt <= total_num_blocks;
					buffer_sel <= A_BUF;
					code_mode <= ctrl_code_mode;
					global_sm <= FETCH_QTZ_MTX;
				end
				FETCH_QTZ_MTX: begin
					start_bram_read <= 1'b1;
					global_sm <= FETCH_QTZ_WAIT;
				end
				FETCH_QTZ_WAIT: begin
					global_sm <= qtz_mtx_done ? START_BRAM_READ : FETCH_QTZ_WAIT;
				end
				START_BRAM_READ: begin
					qtz_fetched <= 1'b1;
					start_bram_read <= 1'b1;
					global_sm <= BRAM_READ;
				end
				BRAM_READ: begin
					global_sm <= rd_bram_done ? START_JIFFY : BRAM_READ;
				end
				START_JIFFY: begin
					start_bram_read <= 1'b0;
					start_jiffy <= 1'b1;
					global_sm <= JIFFY;
				end
				JIFFY: begin
					global_sm <= jiffy_done ? START_BRAM_WRITE : JIFFY;
				end
				START_BRAM_WRITE: begin
					start_jiffy <= 1'b0;
					start_bram_write <= 1'b1;
					global_sm <= BRAM_WRITE;
				end
				BRAM_WRITE: begin
					global_sm <= wr_bram_done ? NEXT_BLOCK : BRAM_WRITE;
				end
				NEXT_BLOCK: begin
					start_bram_write <= 1'b0;
					buffer_sel <= ~buffer_sel;
					block_cnt <= block_cnt - 1;
					global_sm <= CHECK_DONE;
				end
				CHECK_DONE: begin
					global_sm <= (block_cnt > 0) ? START_BRAM_READ : GLOBAL_DONE;
				end
				GLOBAL_DONE: begin
					global_done <= 1'b1;
					global_sm <= ctrl_start_jiffy ? GLOBAL_DONE : IDLE;
				end
				default: begin
					start_bram_read <= 1'b0;
					start_jiffy <= 1'b0;
					start_bram_write <= 1'b0;
					global_done <= 1'b0;
					block_cnt <= 0;
					buffer_sel <= 1'b0;
					code_mode <= 0;
					qtz_fetched <= 1'b0;
					for(i = 0; i < `QTZ_TRANSFER_CNT; i = i + 1) qtz_mtx[i] <= 0;
					global_sm <= IDLE;					
				end
			endcase
		end
	end

	// BRAM Interface Controller

	// Registers for BRAM
	reg [`MEM_WIDTH_RNG] imem [`MEM_DEPTH_RNG];
	reg [`MEM_WIDTH_RNG] omem [`MEM_DEPTH_RNG];

	// Encode
	wire [`RGB_CHANNEL_RNG] imem_r [`PIXEL_ROW_RNG][`PIXEL_ROW_RNG];
    wire [`RGB_CHANNEL_RNG] imem_g [`PIXEL_ROW_RNG][`PIXEL_ROW_RNG];
    wire [`RGB_CHANNEL_RNG] imem_b [`PIXEL_ROW_RNG][`PIXEL_ROW_RNG];

	reg [`QTZ_CHANNEL_RNG] omem_y  [`PIXEL_ROW_RNG][`PIXEL_ROW_RNG];
    reg [`QTZ_CHANNEL_RNG] omem_cb [`PIXEL_ROW_RNG][`PIXEL_ROW_RNG];
    reg [`QTZ_CHANNEL_RNG] omem_cr [`PIXEL_ROW_RNG][`PIXEL_ROW_RNG];

	// Decode
	wire [`QTZ_CHANNEL_RNG] imem_y  [`PIXEL_ROW_RNG][`PIXEL_ROW_RNG];
    wire [`QTZ_CHANNEL_RNG] imem_cb [`PIXEL_ROW_RNG][`PIXEL_ROW_RNG];
    wire [`QTZ_CHANNEL_RNG] imem_cr [`PIXEL_ROW_RNG][`PIXEL_ROW_RNG];

	reg [`RGB_CHANNEL_RNG] omem_r [`PIXEL_ROW_RNG][`PIXEL_ROW_RNG];
    reg [`RGB_CHANNEL_RNG] omem_g [`PIXEL_ROW_RNG][`PIXEL_ROW_RNG];
    reg [`RGB_CHANNEL_RNG] omem_b [`PIXEL_ROW_RNG][`PIXEL_ROW_RNG];

	// Assign Input Memory to Corresponding Channel

	// Encode IMEM
	assign {imem_r[0][7], imem_r[0][6], imem_r[0][5], imem_r[0][4], imem_r[0][3], imem_r[0][2], imem_r[0][1], imem_r[0][0]} = {imem[1],  imem[0]};
	assign {imem_r[1][7], imem_r[1][6], imem_r[1][5], imem_r[1][4], imem_r[1][3], imem_r[1][2], imem_r[1][1], imem_r[1][0]} = {imem[3],  imem[2]};
	assign {imem_r[2][7], imem_r[2][6], imem_r[2][5], imem_r[2][4], imem_r[2][3], imem_r[2][2], imem_r[2][1], imem_r[2][0]} = {imem[5],  imem[4]};
	assign {imem_r[3][7], imem_r[3][6], imem_r[3][5], imem_r[3][4], imem_r[3][3], imem_r[3][2], imem_r[3][1], imem_r[3][0]} = {imem[7],  imem[6]};
	assign {imem_r[4][7], imem_r[4][6], imem_r[4][5], imem_r[4][4], imem_r[4][3], imem_r[4][2], imem_r[4][1], imem_r[4][0]} = {imem[9],  imem[8]};
	assign {imem_r[5][7], imem_r[5][6], imem_r[5][5], imem_r[5][4], imem_r[5][3], imem_r[5][2], imem_r[5][1], imem_r[5][0]} = {imem[11], imem[10]};
	assign {imem_r[6][7], imem_r[6][6], imem_r[6][5], imem_r[6][4], imem_r[6][3], imem_r[6][2], imem_r[6][1], imem_r[6][0]} = {imem[13], imem[12]};
	assign {imem_r[7][7], imem_r[7][6], imem_r[7][5], imem_r[7][4], imem_r[7][3], imem_r[7][2], imem_r[7][1], imem_r[7][0]} = {imem[15], imem[14]};

	assign {imem_g[0][7], imem_g[0][6], imem_g[0][5], imem_g[0][4], imem_g[0][3], imem_g[0][2], imem_g[0][1], imem_g[0][0]} = {imem[17], imem[16]};
	assign {imem_g[1][7], imem_g[1][6], imem_g[1][5], imem_g[1][4], imem_g[1][3], imem_g[1][2], imem_g[1][1], imem_g[1][0]} = {imem[19], imem[18]};
	assign {imem_g[2][7], imem_g[2][6], imem_g[2][5], imem_g[2][4], imem_g[2][3], imem_g[2][2], imem_g[2][1], imem_g[2][0]} = {imem[21], imem[20]};
	assign {imem_g[3][7], imem_g[3][6], imem_g[3][5], imem_g[3][4], imem_g[3][3], imem_g[3][2], imem_g[3][1], imem_g[3][0]} = {imem[23], imem[22]};
	assign {imem_g[4][7], imem_g[4][6], imem_g[4][5], imem_g[4][4], imem_g[4][3], imem_g[4][2], imem_g[4][1], imem_g[4][0]} = {imem[25], imem[24]};
	assign {imem_g[5][7], imem_g[5][6], imem_g[5][5], imem_g[5][4], imem_g[5][3], imem_g[5][2], imem_g[5][1], imem_g[5][0]} = {imem[27], imem[26]};
	assign {imem_g[6][7], imem_g[6][6], imem_g[6][5], imem_g[6][4], imem_g[6][3], imem_g[6][2], imem_g[6][1], imem_g[6][0]} = {imem[29], imem[28]};
	assign {imem_g[7][7], imem_g[7][6], imem_g[7][5], imem_g[7][4], imem_g[7][3], imem_g[7][2], imem_g[7][1], imem_g[7][0]} = {imem[31], imem[30]};

	assign {imem_b[0][7], imem_b[0][6], imem_b[0][5], imem_b[0][4], imem_b[0][3], imem_b[0][2], imem_b[0][1], imem_b[0][0]} = {imem[33], imem[32]};
	assign {imem_b[1][7], imem_b[1][6], imem_b[1][5], imem_b[1][4], imem_b[1][3], imem_b[1][2], imem_b[1][1], imem_b[1][0]} = {imem[35], imem[34]};
	assign {imem_b[2][7], imem_b[2][6], imem_b[2][5], imem_b[2][4], imem_b[2][3], imem_b[2][2], imem_b[2][1], imem_b[2][0]} = {imem[37], imem[36]};
	assign {imem_b[3][7], imem_b[3][6], imem_b[3][5], imem_b[3][4], imem_b[3][3], imem_b[3][2], imem_b[3][1], imem_b[3][0]} = {imem[39], imem[38]};
	assign {imem_b[4][7], imem_b[4][6], imem_b[4][5], imem_b[4][4], imem_b[4][3], imem_b[4][2], imem_b[4][1], imem_b[4][0]} = {imem[41], imem[40]};
	assign {imem_b[5][7], imem_b[5][6], imem_b[5][5], imem_b[5][4], imem_b[5][3], imem_b[5][2], imem_b[5][1], imem_b[5][0]} = {imem[43], imem[42]};
	assign {imem_b[6][7], imem_b[6][6], imem_b[6][5], imem_b[6][4], imem_b[6][3], imem_b[6][2], imem_b[6][1], imem_b[6][0]} = {imem[45], imem[44]};
	assign {imem_b[7][7], imem_b[7][6], imem_b[7][5], imem_b[7][4], imem_b[7][3], imem_b[7][2], imem_b[7][1], imem_b[7][0]} = {imem[47], imem[46]};

	// Decode IMEM
	assign {imem_y[0][7], imem_y[0][6], imem_y[0][5], imem_y[0][4], imem_y[0][3], imem_y[0][2], imem_y[0][1], imem_y[0][0]} = {imem[3],  imem[2],  imem[1],  imem[0]};
	assign {imem_y[1][7], imem_y[1][6], imem_y[1][5], imem_y[1][4], imem_y[1][3], imem_y[1][2], imem_y[1][1], imem_y[1][0]} = {imem[7],  imem[6],  imem[5],  imem[4]};
	assign {imem_y[2][7], imem_y[2][6], imem_y[2][5], imem_y[2][4], imem_y[2][3], imem_y[2][2], imem_y[2][1], imem_y[2][0]} = {imem[11], imem[10], imem[9],  imem[8]};
	assign {imem_y[3][7], imem_y[3][6], imem_y[3][5], imem_y[3][4], imem_y[3][3], imem_y[3][2], imem_y[3][1], imem_y[3][0]} = {imem[15], imem[14], imem[13], imem[12]};
	assign {imem_y[4][7], imem_y[4][6], imem_y[4][5], imem_y[4][4], imem_y[4][3], imem_y[4][2], imem_y[4][1], imem_y[4][0]} = {imem[19], imem[18], imem[17], imem[16]};
	assign {imem_y[5][7], imem_y[5][6], imem_y[5][5], imem_y[5][4], imem_y[5][3], imem_y[5][2], imem_y[5][1], imem_y[5][0]} = {imem[23], imem[22], imem[21], imem[20]};
	assign {imem_y[6][7], imem_y[6][6], imem_y[6][5], imem_y[6][4], imem_y[6][3], imem_y[6][2], imem_y[6][1], imem_y[6][0]} = {imem[27], imem[26], imem[25], imem[24]};
	assign {imem_y[7][7], imem_y[7][6], imem_y[7][5], imem_y[7][4], imem_y[7][3], imem_y[7][2], imem_y[7][1], imem_y[7][0]} = {imem[31], imem[30], imem[29], imem[28]};

	assign {imem_cb[0][7], imem_cb[0][6], imem_cb[0][5], imem_cb[0][4], imem_cb[0][3], imem_cb[0][2], imem_cb[0][1], imem_cb[0][0]} = {imem[35], imem[34], imem[33], imem[32]};
	assign {imem_cb[1][7], imem_cb[1][6], imem_cb[1][5], imem_cb[1][4], imem_cb[1][3], imem_cb[1][2], imem_cb[1][1], imem_cb[1][0]} = {imem[39], imem[38], imem[37], imem[36]};
	assign {imem_cb[2][7], imem_cb[2][6], imem_cb[2][5], imem_cb[2][4], imem_cb[2][3], imem_cb[2][2], imem_cb[2][1], imem_cb[2][0]} = {imem[43], imem[42], imem[41], imem[40]};
	assign {imem_cb[3][7], imem_cb[3][6], imem_cb[3][5], imem_cb[3][4], imem_cb[3][3], imem_cb[3][2], imem_cb[3][1], imem_cb[3][0]} = {imem[47], imem[46], imem[45], imem[44]};
	assign {imem_cb[4][7], imem_cb[4][6], imem_cb[4][5], imem_cb[4][4], imem_cb[4][3], imem_cb[4][2], imem_cb[4][1], imem_cb[4][0]} = {imem[51], imem[50], imem[49], imem[48]};
	assign {imem_cb[5][7], imem_cb[5][6], imem_cb[5][5], imem_cb[5][4], imem_cb[5][3], imem_cb[5][2], imem_cb[5][1], imem_cb[5][0]} = {imem[55], imem[54], imem[53], imem[52]};
	assign {imem_cb[6][7], imem_cb[6][6], imem_cb[6][5], imem_cb[6][4], imem_cb[6][3], imem_cb[6][2], imem_cb[6][1], imem_cb[6][0]} = {imem[59], imem[58], imem[57], imem[56]};
	assign {imem_cb[7][7], imem_cb[7][6], imem_cb[7][5], imem_cb[7][4], imem_cb[7][3], imem_cb[7][2], imem_cb[7][1], imem_cb[7][0]} = {imem[63], imem[62], imem[61], imem[60]};

	assign {imem_cr[0][7], imem_cr[0][6], imem_cr[0][5], imem_cr[0][4], imem_cr[0][3], imem_cr[0][2], imem_cr[0][1], imem_cr[0][0]} = {imem[67], imem[66], imem[65], imem[64]};
	assign {imem_cr[1][7], imem_cr[1][6], imem_cr[1][5], imem_cr[1][4], imem_cr[1][3], imem_cr[1][2], imem_cr[1][1], imem_cr[1][0]} = {imem[71], imem[70], imem[69], imem[68]};
	assign {imem_cr[2][7], imem_cr[2][6], imem_cr[2][5], imem_cr[2][4], imem_cr[2][3], imem_cr[2][2], imem_cr[2][1], imem_cr[2][0]} = {imem[75], imem[74], imem[73], imem[72]};
	assign {imem_cr[3][7], imem_cr[3][6], imem_cr[3][5], imem_cr[3][4], imem_cr[3][3], imem_cr[3][2], imem_cr[3][1], imem_cr[3][0]} = {imem[79], imem[78], imem[77], imem[76]};
	assign {imem_cr[4][7], imem_cr[4][6], imem_cr[4][5], imem_cr[4][4], imem_cr[4][3], imem_cr[4][2], imem_cr[4][1], imem_cr[4][0]} = {imem[83], imem[82], imem[81], imem[80]};
	assign {imem_cr[5][7], imem_cr[5][6], imem_cr[5][5], imem_cr[5][4], imem_cr[5][3], imem_cr[5][2], imem_cr[5][1], imem_cr[5][0]} = {imem[87], imem[86], imem[85], imem[84]};
	assign {imem_cr[6][7], imem_cr[6][6], imem_cr[6][5], imem_cr[6][4], imem_cr[6][3], imem_cr[6][2], imem_cr[6][1], imem_cr[6][0]} = {imem[91], imem[90], imem[89], imem[88]};
	assign {imem_cr[7][7], imem_cr[7][6], imem_cr[7][5], imem_cr[7][4], imem_cr[7][3], imem_cr[7][2], imem_cr[7][1], imem_cr[7][0]} = {imem[95], imem[94], imem[93], imem[92]};

	// OMEM
	always @(*) begin
		if(!code_mode) begin
			{omem[3],  omem[2],  omem[1],  omem[0]}  <=  {omem_y[0][7], omem_y[0][6], omem_y[0][5], omem_y[0][4], omem_y[0][3], omem_y[0][2], omem_y[0][1], omem_y[0][0]};
			{omem[7],  omem[6],  omem[5],  omem[4]}  <=  {omem_y[1][7], omem_y[1][6], omem_y[1][5], omem_y[1][4], omem_y[1][3], omem_y[1][2], omem_y[1][1], omem_y[1][0]};
			{omem[11], omem[10], omem[9],  omem[8]}  <=  {omem_y[2][7], omem_y[2][6], omem_y[2][5], omem_y[2][4], omem_y[2][3], omem_y[2][2], omem_y[2][1], omem_y[2][0]};
			{omem[15], omem[14], omem[13], omem[12]} <=  {omem_y[3][7], omem_y[3][6], omem_y[3][5], omem_y[3][4], omem_y[3][3], omem_y[3][2], omem_y[3][1], omem_y[3][0]};
			{omem[19], omem[18], omem[17], omem[16]} <=  {omem_y[4][7], omem_y[4][6], omem_y[4][5], omem_y[4][4], omem_y[4][3], omem_y[4][2], omem_y[4][1], omem_y[4][0]};
			{omem[23], omem[22], omem[21], omem[20]} <=  {omem_y[5][7], omem_y[5][6], omem_y[5][5], omem_y[5][4], omem_y[5][3], omem_y[5][2], omem_y[5][1], omem_y[5][0]};
			{omem[27], omem[26], omem[25], omem[24]} <=  {omem_y[6][7], omem_y[6][6], omem_y[6][5], omem_y[6][4], omem_y[6][3], omem_y[6][2], omem_y[6][1], omem_y[6][0]};
			{omem[31], omem[30], omem[29], omem[28]} <=  {omem_y[7][7], omem_y[7][6], omem_y[7][5], omem_y[7][4], omem_y[7][3], omem_y[7][2], omem_y[7][1], omem_y[7][0]};
			{omem[35], omem[34], omem[33], omem[32]} <= {omem_cb[0][7], omem_cb[0][6], omem_cb[0][5], omem_cb[0][4], omem_cb[0][3], omem_cb[0][2], omem_cb[0][1], omem_cb[0][0]};
			{omem[39], omem[38], omem[37], omem[36]} <= {omem_cb[1][7], omem_cb[1][6], omem_cb[1][5], omem_cb[1][4], omem_cb[1][3], omem_cb[1][2], omem_cb[1][1], omem_cb[1][0]};
			{omem[43], omem[42], omem[41], omem[40]} <= {omem_cb[2][7], omem_cb[2][6], omem_cb[2][5], omem_cb[2][4], omem_cb[2][3], omem_cb[2][2], omem_cb[2][1], omem_cb[2][0]};
			{omem[47], omem[46], omem[45], omem[44]} <= {omem_cb[3][7], omem_cb[3][6], omem_cb[3][5], omem_cb[3][4], omem_cb[3][3], omem_cb[3][2], omem_cb[3][1], omem_cb[3][0]};
			{omem[51], omem[50], omem[49], omem[48]} <= {omem_cb[4][7], omem_cb[4][6], omem_cb[4][5], omem_cb[4][4], omem_cb[4][3], omem_cb[4][2], omem_cb[4][1], omem_cb[4][0]};
			{omem[55], omem[54], omem[53], omem[52]} <= {omem_cb[5][7], omem_cb[5][6], omem_cb[5][5], omem_cb[5][4], omem_cb[5][3], omem_cb[5][2], omem_cb[5][1], omem_cb[5][0]};
			{omem[59], omem[58], omem[57], omem[56]} <= {omem_cb[6][7], omem_cb[6][6], omem_cb[6][5], omem_cb[6][4], omem_cb[6][3], omem_cb[6][2], omem_cb[6][1], omem_cb[6][0]};
			{omem[63], omem[62], omem[61], omem[60]} <= {omem_cb[7][7], omem_cb[7][6], omem_cb[7][5], omem_cb[7][4], omem_cb[7][3], omem_cb[7][2], omem_cb[7][1], omem_cb[7][0]};
			{omem[67], omem[66], omem[65], omem[64]} <= {omem_cr[0][7], omem_cr[0][6], omem_cr[0][5], omem_cr[0][4], omem_cr[0][3], omem_cr[0][2], omem_cr[0][1], omem_cr[0][0]};
			{omem[71], omem[70], omem[69], omem[68]} <= {omem_cr[1][7], omem_cr[1][6], omem_cr[1][5], omem_cr[1][4], omem_cr[1][3], omem_cr[1][2], omem_cr[1][1], omem_cr[1][0]};
			{omem[75], omem[74], omem[73], omem[72]} <= {omem_cr[2][7], omem_cr[2][6], omem_cr[2][5], omem_cr[2][4], omem_cr[2][3], omem_cr[2][2], omem_cr[2][1], omem_cr[2][0]};
			{omem[79], omem[78], omem[77], omem[76]} <= {omem_cr[3][7], omem_cr[3][6], omem_cr[3][5], omem_cr[3][4], omem_cr[3][3], omem_cr[3][2], omem_cr[3][1], omem_cr[3][0]};
			{omem[83], omem[82], omem[81], omem[80]} <= {omem_cr[4][7], omem_cr[4][6], omem_cr[4][5], omem_cr[4][4], omem_cr[4][3], omem_cr[4][2], omem_cr[4][1], omem_cr[4][0]};
			{omem[87], omem[86], omem[85], omem[84]} <= {omem_cr[5][7], omem_cr[5][6], omem_cr[5][5], omem_cr[5][4], omem_cr[5][3], omem_cr[5][2], omem_cr[5][1], omem_cr[5][0]};
			{omem[91], omem[90], omem[89], omem[88]} <= {omem_cr[6][7], omem_cr[6][6], omem_cr[6][5], omem_cr[6][4], omem_cr[6][3], omem_cr[6][2], omem_cr[6][1], omem_cr[6][0]};
			{omem[95], omem[94], omem[93], omem[92]} <= {omem_cr[7][7], omem_cr[7][6], omem_cr[7][5], omem_cr[7][4], omem_cr[7][3], omem_cr[7][2], omem_cr[7][1], omem_cr[7][0]};
		end
		else begin
			{omem[1],  omem[0]}  <= {omem_r[0][7], omem_r[0][6], omem_r[0][5], omem_r[0][4], omem_r[0][3], omem_r[0][2], omem_r[0][1], omem_r[0][0]};
			{omem[3],  omem[2]}  <= {omem_r[1][7], omem_r[1][6], omem_r[1][5], omem_r[1][4], omem_r[1][3], omem_r[1][2], omem_r[1][1], omem_r[1][0]};
			{omem[5],  omem[4]}  <= {omem_r[2][7], omem_r[2][6], omem_r[2][5], omem_r[2][4], omem_r[2][3], omem_r[2][2], omem_r[2][1], omem_r[2][0]};
			{omem[7],  omem[6]}  <= {omem_r[3][7], omem_r[3][6], omem_r[3][5], omem_r[3][4], omem_r[3][3], omem_r[3][2], omem_r[3][1], omem_r[3][0]};
			{omem[9],  omem[8]}  <= {omem_r[4][7], omem_r[4][6], omem_r[4][5], omem_r[4][4], omem_r[4][3], omem_r[4][2], omem_r[4][1], omem_r[4][0]};
			{omem[11], omem[10]} <= {omem_r[5][7], omem_r[5][6], omem_r[5][5], omem_r[5][4], omem_r[5][3], omem_r[5][2], omem_r[5][1], omem_r[5][0]};
			{omem[13], omem[12]} <= {omem_r[6][7], omem_r[6][6], omem_r[6][5], omem_r[6][4], omem_r[6][3], omem_r[6][2], omem_r[6][1], omem_r[6][0]};
			{omem[15], omem[14]} <= {omem_r[7][7], omem_r[7][6], omem_r[7][5], omem_r[7][4], omem_r[7][3], omem_r[7][2], omem_r[7][1], omem_r[7][0]};
			{omem[17], omem[16]} <= {omem_g[0][7], omem_g[0][6], omem_g[0][5], omem_g[0][4], omem_g[0][3], omem_g[0][2], omem_g[0][1], omem_g[0][0]};
			{omem[19], omem[18]} <= {omem_g[1][7], omem_g[1][6], omem_g[1][5], omem_g[1][4], omem_g[1][3], omem_g[1][2], omem_g[1][1], omem_g[1][0]};
			{omem[21], omem[20]} <= {omem_g[2][7], omem_g[2][6], omem_g[2][5], omem_g[2][4], omem_g[2][3], omem_g[2][2], omem_g[2][1], omem_g[2][0]};
			{omem[23], omem[22]} <= {omem_g[3][7], omem_g[3][6], omem_g[3][5], omem_g[3][4], omem_g[3][3], omem_g[3][2], omem_g[3][1], omem_g[3][0]};
			{omem[25], omem[24]} <= {omem_g[4][7], omem_g[4][6], omem_g[4][5], omem_g[4][4], omem_g[4][3], omem_g[4][2], omem_g[4][1], omem_g[4][0]};
			{omem[27], omem[26]} <= {omem_g[5][7], omem_g[5][6], omem_g[5][5], omem_g[5][4], omem_g[5][3], omem_g[5][2], omem_g[5][1], omem_g[5][0]};
			{omem[29], omem[28]} <= {omem_g[6][7], omem_g[6][6], omem_g[6][5], omem_g[6][4], omem_g[6][3], omem_g[6][2], omem_g[6][1], omem_g[6][0]};
			{omem[31], omem[30]} <= {omem_g[7][7], omem_g[7][6], omem_g[7][5], omem_g[7][4], omem_g[7][3], omem_g[7][2], omem_g[7][1], omem_g[7][0]};
			{omem[33], omem[32]} <= {omem_b[0][7], omem_b[0][6], omem_b[0][5], omem_b[0][4], omem_b[0][3], omem_b[0][2], omem_b[0][1], omem_b[0][0]};
			{omem[35], omem[34]} <= {omem_b[1][7], omem_b[1][6], omem_b[1][5], omem_b[1][4], omem_b[1][3], omem_b[1][2], omem_b[1][1], omem_b[1][0]};
			{omem[37], omem[36]} <= {omem_b[2][7], omem_b[2][6], omem_b[2][5], omem_b[2][4], omem_b[2][3], omem_b[2][2], omem_b[2][1], omem_b[2][0]};
			{omem[39], omem[38]} <= {omem_b[3][7], omem_b[3][6], omem_b[3][5], omem_b[3][4], omem_b[3][3], omem_b[3][2], omem_b[3][1], omem_b[3][0]};
			{omem[41], omem[40]} <= {omem_b[4][7], omem_b[4][6], omem_b[4][5], omem_b[4][4], omem_b[4][3], omem_b[4][2], omem_b[4][1], omem_b[4][0]};
			{omem[43], omem[42]} <= {omem_b[5][7], omem_b[5][6], omem_b[5][5], omem_b[5][4], omem_b[5][3], omem_b[5][2], omem_b[5][1], omem_b[5][0]};
			{omem[45], omem[44]} <= {omem_b[6][7], omem_b[6][6], omem_b[6][5], omem_b[6][4], omem_b[6][3], omem_b[6][2], omem_b[6][1], omem_b[6][0]};
			{omem[47], omem[46]} <= {omem_b[7][7], omem_b[7][6], omem_b[7][5], omem_b[7][4], omem_b[7][3], omem_b[7][2], omem_b[7][1], omem_b[7][0]};
			for(i = 48; i < 96; i=i+1) omem[i] <= 0;		
		end
	end

	// BRAM Interface 
	reg start_read;
	reg start_write;
	reg [31:0] current_bram_addr;
	reg [31:0] WR_data;
	reg [6:0] transfer_cnt;
	wire [31:0] axi_bram_read_data;
	wire bram_complete;

	BRAM_IF bram_if_inst (
		.axi_start_read(start_read),
		.axi_start_write(start_write),
		.axi_clk(S_AXI_ACLK),
		.axi_rst(S_AXI_ARESETN),
		.axi_bram_addr(current_bram_addr),
		.axi_bram_read_data(axi_bram_read_data),
		.axi_bram_write_data(WR_data),
		.bram_complete(bram_complete),
		.addr_BRAM(addr_BRAM),
		.clk_BRAM(clk_BRAM),
		.dout_BRAM(dout_BRAM),
		.din_BRAM(din_BRAM),
		.en_BRAM(en_BRAM),
		.rst_BRAM(rst_BRAM),
		.we_BRAM(we_BRAM) 
	);

	// Memory Fetch/Write Controller
	reg [4:0] mem_sm;

	localparam 
		//IDLE = 0,
		POLL0 = 1,
		POLL1 = 2,
		POLL2 = 3,
		START_FETCH = 4,
		FETCH0 = 5,
		FETCH1 = 6,
		FETCH2 = 7,
		DONE_FETCH_CHECK = 8,
		DONE_FETCH = 9,
		WAIT_OUT_DATA = 10,
		START_WRITE = 11,
		WRITE0 = 12,
		WRITE1 = 13,
		WRITE2 = 14,
		DONE_WRITE_CHECK = 15,
		DONE_WRITE = 16,
		FETCH_QTZ = 17,
		QTZ0 = 18,
		QTZ1 = 19,
		QTZ2 = 20,
		DONE_QTZ_CHECK = 21,
		CLEAR_MAGIC = 22,
		MAGIC0 = 23,
		MAGIC1 = 24;

	always @(posedge S_AXI_ACLK) begin
		if ( ctrl_reset_jiffy | ~S_AXI_ARESETN) begin
			for(i=0; i < `MEM_DEPTH; i=i+1) imem[i] <= 0;
			transfer_cnt <= 0;
			start_read <= 0;
			start_write <= 0;
			current_bram_addr <= 0;
			rd_bram_done <= 0;
			wr_bram_done <= 0;
			mem_sm <= IDLE;
			qtz_mtx_done <= 0;
		end
		else begin
			case (mem_sm)
				IDLE: begin
					for(i=0; i < `MEM_DEPTH; i=i+1) imem[i] <= 0;
					transfer_cnt <= 0;
					start_read <= 0;
					start_write <= 0;
					current_bram_addr <= 0;
					WR_data <= 0;
					rd_bram_done <= 0;
					wr_bram_done <= 0;
					qtz_mtx_done <= 0;
					mem_sm <= start_bram_read ? (qtz_fetched ? POLL0 : FETCH_QTZ) : IDLE;
				end 
				FETCH_QTZ: begin
					current_bram_addr <= `QTZ_ADDRESS;
					transfer_cnt <= `QTZ_TRANSFER_CNT;
					mem_sm <= QTZ0;
				end
				QTZ0: begin
					start_read <= 1'b1;
					mem_sm <= bram_complete ? QTZ1 : QTZ0;
				end
				QTZ1: begin
					start_read <= 1'b0;
					mem_sm <= bram_complete ? QTZ1 : QTZ2;
				end
				QTZ2: begin
					transfer_cnt <= transfer_cnt - 1;
					qtz_mtx[`QTZ_TRANSFER_CNT - transfer_cnt] <= axi_bram_read_data;					
					current_bram_addr <= current_bram_addr + `ADDR_INCR;
					mem_sm <= DONE_QTZ_CHECK;
				end
				DONE_QTZ_CHECK: begin
					mem_sm <= (transfer_cnt == 0) ? IDLE : QTZ0;
					qtz_mtx_done <= (transfer_cnt == 0);
				end
				POLL0: begin
					current_bram_addr <= buffer_sel ? `B_MAGIC_ADDR : `A_MAGIC_ADDR;
					start_read <= 1'b1;
					mem_sm <= bram_complete ? POLL1 : POLL0;
				end
				POLL1: begin
					start_read <= 1'b0;
					mem_sm <= bram_complete ? POLL1 : POLL2;
				end
				POLL2: begin
					mem_sm <= (axi_bram_read_data == `MAGIC_NUMBER) ? START_FETCH : POLL0;
				end
				START_FETCH: begin
					current_bram_addr <= buffer_sel ? `B_READ_ADDR  : `A_READ_ADDR;
					transfer_cnt <= `TOTAL_TRANSFER;
					mem_sm <= FETCH0;
				end
				FETCH0: begin
					start_read <= 1'b1;
					mem_sm <= bram_complete ? FETCH1 : FETCH0;
				end
				FETCH1: begin
					start_read <= 1'b0;
					mem_sm <= bram_complete ? FETCH1 : FETCH2;
				end
				FETCH2: begin
					transfer_cnt <= transfer_cnt - 1;
					imem[`TOTAL_TRANSFER - transfer_cnt] <= axi_bram_read_data;
					current_bram_addr <= current_bram_addr + `ADDR_INCR;
					mem_sm <= DONE_FETCH_CHECK;
				end
				DONE_FETCH_CHECK: begin
					mem_sm <= (transfer_cnt == 0) ? CLEAR_MAGIC : FETCH0;  
				end
				CLEAR_MAGIC: begin
					current_bram_addr <= buffer_sel ? `B_MAGIC_ADDR : `A_MAGIC_ADDR;
					WR_data <= 0;
					mem_sm <= MAGIC0;
				end
				MAGIC0: begin
					start_write <= 1'b1;
					mem_sm <= bram_complete ? MAGIC1 : MAGIC0;
				end
				MAGIC1: begin
					start_write <= 1'b0;
					mem_sm <= bram_complete ? MAGIC1 : DONE_FETCH;
				end
				DONE_FETCH: begin
					rd_bram_done <= 1'b1;
					mem_sm <= start_jiffy ? WAIT_OUT_DATA : DONE_FETCH;
				end
				WAIT_OUT_DATA: begin
					rd_bram_done <= 0;
					mem_sm <= start_bram_write ? START_WRITE : WAIT_OUT_DATA;
				end
				START_WRITE: begin
					current_bram_addr <= buffer_sel ? `B_WRITE_ADDR : `A_WRITE_ADDR;
					transfer_cnt <= `TOTAL_TRANSFER - 1;
					WR_data <= omem[0];
					mem_sm <= WRITE0;
				end
				WRITE0: begin
					start_write <= 1'b1;
					mem_sm <= bram_complete ? WRITE1 : WRITE0;
				end
				WRITE1: begin
					start_write <= 1'b0;
					mem_sm <= bram_complete ? WRITE1 : WRITE2;
				end
				WRITE2: begin
					transfer_cnt <= transfer_cnt - 1;
					current_bram_addr <= current_bram_addr + `ADDR_INCR;
					WR_data <= omem[`TOTAL_TRANSFER - transfer_cnt];
					mem_sm <= DONE_WRITE_CHECK;
				end
				DONE_WRITE_CHECK: begin
					mem_sm <= (transfer_cnt == 0) ? DONE_WRITE : WRITE0;
				end
				DONE_WRITE: begin
					wr_bram_done <= 1'b1;
					mem_sm <= wr_bram_ack ? IDLE : DONE_WRITE;
				end
				default: begin
					for(i=0; i < `MEM_DEPTH; i=i+1) imem[i] <= 0;
					transfer_cnt <= 0;
					start_read <= 0;
					start_write <= 0;
					current_bram_addr <= 0;
					WR_data <= 0;
					rd_bram_done <= 0;
					wr_bram_done <= 0;
					mem_sm <= IDLE;
				end
			endcase
		end
	end

	wire out_ready;
	reg in_ready;
	reg [2:0] row_sel; // 8 rows
	reg [2:0] jiffy_sm;

    // Encode
    reg  [`RGB_CHANNEL_RNG] imem_encode_r  [`PIXEL_BUS_RNG];
    reg  [`RGB_CHANNEL_RNG] imem_encode_g  [`PIXEL_BUS_RNG];
    reg  [`RGB_CHANNEL_RNG] imem_encode_b  [`PIXEL_BUS_RNG];
    wire [`QTZ_CHANNEL_RNG] omem_encode_y  [`PIXEL_BUS_RNG];
    wire [`QTZ_CHANNEL_RNG] omem_encode_cb [`PIXEL_BUS_RNG];
    wire [`QTZ_CHANNEL_RNG] omem_encode_cr [`PIXEL_BUS_RNG];

    // Decode
    reg  [`QTZ_CHANNEL_RNG] imem_decode_y  [`PIXEL_BUS_RNG];
    reg  [`QTZ_CHANNEL_RNG] imem_decode_cb [`PIXEL_BUS_RNG];
    reg  [`QTZ_CHANNEL_RNG] imem_decode_cr [`PIXEL_BUS_RNG];
    wire [`RGB_CHANNEL_RNG] omem_decode_r  [`PIXEL_BUS_RNG];
    wire [`RGB_CHANNEL_RNG] omem_decode_g  [`PIXEL_BUS_RNG];
    wire [`RGB_CHANNEL_RNG] omem_decode_b  [`PIXEL_BUS_RNG];

	localparam 
		// IDLE = 0,
		START = 1,
		INPUT = 2,
		WAIT = 3,
		OUTPUT = 4,
		DONE = 5,
		HOLD = 6;

	// JIFFY Accelerator
	always @(posedge S_AXI_ACLK) begin
		if ( ctrl_reset_jiffy | ~S_AXI_ARESETN ) begin
			for(i = 0; i < `PIXEL_ROW_SIZE; i=i+1) begin
				imem_encode_r[i] <= 0;
				imem_encode_g[i] <= 0;
				imem_encode_b[i] <= 0;
				imem_decode_y[i] <= 0;
				imem_decode_cb[i] <= 0;
				imem_decode_cr[i] <= 0;
				for(j = 0; j < `PIXEL_ROW_SIZE; j=j+1) begin
					omem_r[i][j]  <= 8'b0;
					omem_g[i][j]  <= 8'b0;
					omem_b[i][j]  <= 8'b0;
					omem_y[i][j]  <= 16'b0;
					omem_cb[i][j] <= 16'b0;
					omem_cr[i][j] <= 16'b0;
				end
			end
			row_sel <= 0;
			in_ready <= 0;
			jiffy_done <= 0;
			jiffy_sm <= IDLE;
		end
		else begin
			case (jiffy_sm)
				IDLE: begin
					for(i = 0; i < `PIXEL_ROW_SIZE; i=i+1) begin
						imem_encode_r[i] <= 0;
						imem_encode_g[i] <= 0;
						imem_encode_b[i] <= 0;
						imem_decode_y[i] <= 0;
						imem_decode_cb[i] <= 0;
						imem_decode_cr[i] <= 0;
						for(j = 0; j < `PIXEL_ROW_SIZE; j=j+1) begin
							omem_r[i][j]  <= 8'b0;
							omem_g[i][j]  <= 8'b0;
							omem_b[i][j]  <= 8'b0;
							omem_y[i][j]  <= 16'b0;
							omem_cb[i][j] <= 16'b0;
							omem_cr[i][j] <= 16'b0;
						end
					end
					row_sel <= 0;
					in_ready <= 0;
					jiffy_done <= 0;
					jiffy_sm <= start_jiffy ? START : IDLE; 
				end 
				START: begin
					row_sel <= 0;
					in_ready <= 1'b1;
					if(code_mode) begin
						for(i = 0; i < `PIXEL_ROW_SIZE; i=i+1) begin
							imem_decode_y[i] <=  imem_y[0][i];
							imem_decode_cb[i] <= imem_cb[0][i];
							imem_decode_cr[i] <= imem_cr[0][i];
						end
					end
					else begin
						for(i = 0; i < `PIXEL_ROW_SIZE; i=i+1) begin
							imem_encode_r[i] <= imem_r[0][i];
							imem_encode_g[i] <= imem_g[0][i];
							imem_encode_b[i] <= imem_b[0][i];
						end
					end
					jiffy_sm <= INPUT;
				end
				INPUT: begin
					row_sel <= row_sel + 1;
					if(code_mode) begin
						for(i = 0; i < `PIXEL_ROW_SIZE; i=i+1) begin
							imem_decode_y[i] <=  imem_y [row_sel+1][i];
							imem_decode_cb[i] <= imem_cb[row_sel+1][i];
							imem_decode_cr[i] <= imem_cr[row_sel+1][i];
						end
					end
					else begin
						for(i = 0; i < `PIXEL_ROW_SIZE; i=i+1) begin
							imem_encode_r[i] <= imem_r[row_sel+1][i];
							imem_encode_g[i] <= imem_g[row_sel+1][i];
							imem_encode_b[i] <= imem_b[row_sel+1][i];
						end
					end
					jiffy_sm <= (row_sel == 3'b110) ? WAIT : INPUT;
				end
				WAIT: begin
					row_sel <= 0;
					if(code_mode) begin
						for(i = 0; i < `PIXEL_ROW_SIZE; i=i+1) begin
							imem_decode_y[i] <= 0;
							imem_decode_cb[i] <=0;
							imem_decode_cr[i] <=0;
						end
					end
					else begin
						for(i = 0; i < `PIXEL_ROW_SIZE; i=i+1) begin
							imem_encode_r[i] <= 0;
							imem_encode_g[i] <= 0;
							imem_encode_b[i] <= 0;
						end
					end
					jiffy_sm <= out_ready ? OUTPUT : WAIT;
				end
				OUTPUT: begin	
					row_sel <= row_sel + 1;
					if(code_mode) begin
						for(i = 0; i < `PIXEL_ROW_SIZE; i=i+1) begin
							omem_r[row_sel][i] <= omem_decode_r[i];
							omem_g[row_sel][i] <= omem_decode_g[i];
							omem_b[row_sel][i] <= omem_decode_b[i];
						end
					end
					else begin
						for(i = 0; i < `PIXEL_ROW_SIZE; i=i+1) begin
							omem_y [row_sel][i] <= omem_encode_y[i];
							omem_cb[row_sel][i] <= omem_encode_cb[i];
							omem_cr[row_sel][i] <= omem_encode_cr[i];
						end
					end
					jiffy_sm <= (row_sel == 3'b111) ? DONE : OUTPUT;
				end
				DONE: begin
					jiffy_done <= 1'b1;
					jiffy_sm <= start_bram_write ? HOLD : DONE;
				end
				HOLD: begin
					jiffy_done <= 1'b0;
					jiffy_sm <= start_bram_write ? HOLD : IDLE;
				end
				default: begin
					for(i = 0; i < `PIXEL_ROW_SIZE; i=i+1) begin
						imem_encode_r[i] <= 0;
						imem_encode_g[i] <= 0;
						imem_encode_b[i] <= 0;
						imem_decode_y[i] <= 0;
						imem_decode_cb[i] <= 0;
						imem_decode_cr[i] <= 0;
						for(j = 0; j < `PIXEL_ROW_SIZE; j=j+1) begin
							omem_r[i][j]  <= 8'b0;
							omem_g[i][j]  <= 8'b0;
							omem_b[i][j]  <= 8'b0;
							omem_y[i][j]  <= 16'b0;
							omem_cb[i][j] <= 16'b0;
							omem_cr[i][j] <= 16'b0;
						end
					end
					row_sel <= 0;
					in_ready <= 0;
					jiffy_done <= 0;
					jiffy_sm <= IDLE; 
				end
			endcase
		end
	end

	jiffy jiffy_inst(
		.imem_encode_r(imem_encode_r),
		.imem_encode_g(imem_encode_g),
		.imem_encode_b(imem_encode_b),
		.omem_encode_y(omem_encode_y),
		.omem_encode_cb(omem_encode_cb),
		.omem_encode_cr(omem_encode_cr),
		.imem_decode_y(imem_decode_y),
		.imem_decode_cb(imem_decode_cb),
		.imem_decode_cr(imem_decode_cr),
		.omem_decode_r(omem_decode_r),
		.omem_decode_g(omem_decode_g),
		.omem_decode_b(omem_decode_b),
		.qtz_mtx(qtz_mtx),
		.code_mode(code_mode),
		.start(in_ready),
		.out_ready(out_ready),
		.clk(S_AXI_ACLK),
		.rst(reset_jiffy)
	);


	// Add user logic here
    assign clk_BRAM = S_AXI_ACLK;
	assign reset_jiffy = ~(ctrl_reset_jiffy | ~S_AXI_ARESETN);

	// AXI Register Assignment
//	assign total_num_blocks = slv_reg0;
//	assign ctrl_start_jiffy = slv_reg1[0];
//	assign ctrl_reset_jiffy = slv_reg1[1] | ~S_AXI_ARESETN;
//	assign ctrl_code_mode = slv_reg1[2];

//	// Interrupt Assignment
//	assign wr_bram_ack = slv_reg3[0];
	assign interrupt_out = wr_bram_done;
	// User logic ends

	endmodule
