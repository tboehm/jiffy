// Standard C/C++ includes
#include <stdio.h>
#include <stdlib.h>

// Verilator includes
#include "verilated.h"
#include "verilated_vcd_c.h"

// Jiffy: Verilated modules
#include "Vcsx_encode.h"

// Jiffy: C includes
#include "../../../sw/src/csx.h"

// #define TRACE
#undef TRACE

static VerilatedVcdC *trace;

// Declare the global testbench, of a type externally defined
static Vcsx_encode *tb;

// Simulate at the speed of the MiST Minimig port's SDRAM clock
static double timestamp = 0;

void
tick()
{
    tb->clk = 1;
    tb->eval();
    trace->dump(timestamp++);
    tb->clk = 0;
    tb->eval();
    trace->dump(timestamp++);
}

static int in_array[8][8][3] = { 0 };
static int out_array[8][8][3] = { 0 };
static int ref_array[8][8][3] = { 0 };

void
randomize_input()
{
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            for (int c = 0; c < 3; c++) {
                in_array[i][j][c] = rand() % 256;
            }
        }
    }
}

void
run_test()
{
    for (int i = 0; i < 8; i++) {
        tb->start = 1;
        for (int j = 0; j < 8; j++) {
            tb->imem_csx_r_channel[j] = in_array[i][j][0];
            tb->imem_csx_g_channel[j] = in_array[i][j][1];
            tb->imem_csx_b_channel[j] = in_array[i][j][2];
        }

        // It takes two ticks before the results are actually correct. I think this is just a Verilator thing?
        tick();
        tick();
        assert(tb->out_ready);

        for (int j = 0; j < 8; j++) {
            out_array[i][j][0] = tb->csx_dct_y_channel[j];
            out_array[i][j][1] = tb->csx_dct_cb_channel[j];
            out_array[i][j][2] = tb->csx_dct_cr_channel[j];
        }

        tb->start = 0;

        tick();
        assert(!tb->out_ready);
    }
}

int
check_differences()
{
    int differences = 0;

    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            for (int c = 0; c < 3; c++) {
                int out = out_array[i][j][c];
                int ref = ref_array[i][j][c];
                int diff = out - ref;
                if (diff > 1 || diff < -1) {
                    printf("[%d][%d][%d]: out %d != ref %d\n", i, j, c, out, ref);
                    differences += 1;
                }
            }
        }
    }

    return differences;
}

int
main(int argc, char **argv)
{
    // Initialize Verilators variables
    Verilated::commandArgs(argc, argv);
    Verilated::traceEverOn(true);
    trace = new VerilatedVcdC;

    // Create an instance of our module under test
    fprintf(stderr, "\033[;1mTesting CSX encode\033[0m\n");
    tb = new Vcsx_encode;

#ifdef TRACE
    tb->trace(trace, 99);
    trace->open("wave.vcd");
#endif

    const int TestCount = (int)1e6;
    int       differences = 0;

    srand(2112);

    for (int test_num = 0; test_num < TestCount; test_num++) {
        // Reset the testbench.
        tb->rst = 0;
        tick();
        tick();
        tb->rst = 1;

        randomize_input();

        csx_rgb_to_ycbcr(in_array, ref_array);

        run_test();

        differences = check_differences();

        if (differences) {
            fprintf(
                stderr,
                "\033[31;1mFailed test %d/%d: %d differences\033[0m\n",
                test_num + 1,
                TestCount,
                differences
            );
            break;
        }
    }

    if (!differences) {
        fprintf(stderr, "\033[32;1mPassed %d tests\033[0m\n", TestCount);
    }

#ifdef TRACE
    trace->close();
#endif

    delete tb;
    delete trace;
    return 0;
}
