// Standard C/C++ includes
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// Verilator includes
#include "verilated.h"
#include "verilated_vcd_c.h"

// Jiffy: Verilated modules
#include "Vjiffy.h"

// Jiffy: C includes

#define DEBUG

#ifdef DEBUG
#define NUM_TESTS 1
#define TRACE
#else
#define NUM_TESTS ((int)1e4)
#endif // DEBUG

#define MODE_ENCODE 0
#define MODE_DECODE 1

static VerilatedVcdC *trace;

// Declare the global testbench, of a type externally defined
static Vjiffy *tb;

// For trace.
static double timestamp = 0;

void
tick()
{
    tb->clk = 1;
    tb->eval();
    trace->dump(timestamp++);
    tb->clk = 0;
    tb->eval();
    trace->dump(timestamp++);
}

static int in_array[8][8][3] = { 0 };
static int out_array[8][8][3] = { 0 };
static int out_array_t[8][8][3] = { 0 };
static int ref_array[8][8][3] = { 0 };
static float f_ref_array[8][8][3] = { 0.f };

void
randomize_input()
{
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            for (int c = 0; c < 3; c++) {
                in_array[i][j][c] = 64;
                // in_array[i][j][c] = i % 2 == 0 ? 128 : 0;
                // in_array[i][j][c] = rand() % 256;
            }
        }
    }
}

void
run_test(int mode)
{
    tb->start = 1;
    tb->code_mode = mode; // 1 is decode, 0 is encode

    // Fill the inputs.
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {

            // tb->imem_encode_r[j] = in_array[i][j][0];
            // tb->imem_encode_g[j] = in_array[i][j][1];
            // tb->imem_encode_b[j] = in_array[i][j][2];

            tb->imem_decode_y[j] = in_array[i][j][0];
            tb->imem_decode_cb[j] = in_array[i][j][1];
            tb->imem_decode_cr[j] = in_array[i][j][2];

        }
        tick();
        // assert(!tb->out_ready);
    }

    while (!tb->out_ready) {
        tick();
    }

    // Get the outputs.
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {

            // out_array[i][j][0] = tb->omem_encode_y[j];
            // out_array[i][j][1] = tb->omem_encode_cb[j];
            // out_array[i][j][2] = tb->omem_encode_cr[j];

            out_array[i][j][0] = tb->omem_decode_r[j];
            out_array[i][j][1] = tb->omem_decode_g[j];
            out_array[i][j][2] = tb->omem_decode_b[j];

        }
        assert(tb->out_ready);
        tick();
    }

    tb->start = 0;
    tick();
    tick();
    tick();

    // assert(!tb->out_ready);
}

int
check_differences()
{
    static const int tolerance = 9;

    int differences = 0;

    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            // Just do one channel, for now.
            for (int c = 0; c < 1; c++) {
                int out = out_array_t[i][j][c];
                int ref = ref_array[i][j][c];
                int diff = out - ref;
                if (diff > tolerance || diff < -tolerance) {
                    printf("[%d][%d][%d]: out %d not within %d of ref %d\n", i, j, c, out, tolerance, ref);
                    differences += 1;
                }
            }
        }
    }

    return differences;
}

int
float_to_fixed(float coef)
{
    static const int multiplier = 256;
    static const int frac_mask = ~((1 << 8) - 1);
    static const int fixed_mask = ((1 << 21) - 1);

    int coef_int = (int)(coef * multiplier);
    int int_part = (int)(coef * multiplier) & frac_mask;
    int frac_part = coef_int - int_part;
    int fixed = (int_part | frac_part) & fixed_mask;

    return fixed;
}

int
fixed_to_int(int coef)
{
    static const int mask = ((1 << 21) - 1);

    if (coef & (1 << 20)) {
        // Negative
        coef = (~coef) & mask;
        coef = -coef;
        coef += 4; // FIXME: make +1 when coefficient matrices are corrected.
    }
    // coef /= 4; // FIXME: remove when coefficient matrices are corrected.

    return coef;
}

#ifdef DEBUG
void
print_mcu(const char *name, int mcu[8][8][3])
{
    printf("\n%s:\n", name);

    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            for (int c = 0; c < 1; c++) {
                int coef = mcu[i][j][c];

                // if (coef > 0x1ffe00 || coef < 92) {
                if (coef == 0) {
                    printf(" \033[90m%4d\033[0m", 0);
                } else {
                    printf(" \033[;1m%4d\033[0m", coef);
                }

            }
        }
        printf("\n");
    }
}
#else
#define print_mcu(name, mcu)
#endif // DEBUG

int
main(int argc, char **argv)
{
    int rc = 0;

    // Initialize Verilators variables
    Verilated::commandArgs(argc, argv);
    Verilated::traceEverOn(true);
    trace = new VerilatedVcdC;

    // Create an instance of our module under test
    tb = new Vjiffy;

#ifdef TRACE
    tb->trace(trace, 99);
    trace->open("wave.vcd");
#endif

    const int TestCount = NUM_TESTS;
    int       differences = 0;

    srand(2112);

    ///////////////////////////////
    // DISCRETE COSINE TRANSFORM //
    ///////////////////////////////

    fprintf(stderr, "\033[;1mTesting DCT systolic array\033[0m\n");

    tb->rst = 0;
    tick();
    tick();
    tb->rst = 1;

    for (int test_num = 0; test_num < TestCount; test_num++) {

        randomize_input();

        print_mcu("Input", in_array);

        // dct_transform(in_array, f_ref_array);

        // // Convert to integer (to print)
        // for (int i = 0; i < 8; i++) {
        //     for (int j = 0; j < 8; j++) {
        //         for (int c = 0; c < 3; c++) {
        //             float coef = f_ref_array[i][j][c];
        //             ref_array[i][j][c] = (int)coef;
        //         }
        //     }
        // }

        // print_mcu("DCT (encode) reference", ref_array);

        run_test(MODE_ENCODE);

        // Convert negative numbers back.
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                for (int c = 0; c < 3; c++) {
                    int coef = out_array[i][j][c];
                    coef = fixed_to_int(coef);
                    out_array[i][j][c] = coef;
                    out_array_t[j][i][c] = coef;
                }
            }
        }

        print_mcu("Output", out_array_t);

        // differences = check_differences();

        // if (differences) {
        //     fprintf(
        //         stderr,
        //         "\033[31;1mFailed DCT %d/%d: %d differences\033[0m\n",
        //         test_num + 1,
        //         TestCount,
        //         differences
        //     );
        //     rc = 1;
        //     break;
        // }

    }

    // return 0;

    // ///////////////////////////////////////
    // // INVERSE DISCRETE COSINE TRANSFORM //
    // ///////////////////////////////////////

    // fprintf(stderr, "\033[;1mTesting IDCT systolic array\033[0m\n");

    // tb->rst = 0;
    // tick();
    // tick();
    // tb->rst = 1;

    // for (int test_num = 0; !differences && test_num < TestCount; test_num++) {

    //     randomize_input();

    //     print_mcu("Input", in_array);

    //     for (int i = 0; i < 8; i++) {
    //         for (int j = 0; j < 8; j++) {
    //             for (int c = 0; c < 3; c++) {
    //                 in_array[i][j][c] = ref_array[i][j][c];
    //             }
    //         }
    //     }

    //     dct_inverse_transform(in_array, ref_array);

    //     print_mcu("Inverse DCT (decode) reference", ref_array);

    //     run_test(MODE_DECODE);

    //     // Convert negative numbers back.
    //     for (int i = 0; i < 8; i++) {
    //         for (int j = 0; j < 8; j++) {
    //             for (int c = 0; c < 3; c++) {
    //                 int coef = out_array[i][j][c];
    //                 coef = fixed_to_int(coef);
    //                 out_array[i][j][c] = coef;
    //                 out_array_t[j][i][c] = coef;
    //             }
    //         }
    //     }

    //     print_mcu("Inverse DCT (decode) output", out_array_t);

    //     differences = check_differences();

    //     if (differences) {
    //         fprintf(
    //             stderr,
    //             "\033[31;1mFailed inverse DCT %d/%d: %d differences\033[0m\n",
    //             test_num + 1,
    //             TestCount,
    //             differences
    //         );
    //         rc = 1;
    //         break;
    //     }
    // }

    // if (!differences) {
    //     fprintf(stderr, "\033[32;1mPassed %d DCTs and IDCTs\033[0m\n", TestCount);
    // } else {
    //     fprintf(stderr, "\033[31;1mFailed\033[0m\n");
    //     rc = 1;
    // }

#ifdef TRACE
    trace->close();
#endif

    delete tb;
    delete trace;
    return rc;
}
