# Makefile for Verilator test benches
#
# Copyright (c) 2021 Trey Boehm & Jordan Pamatmat
#
# This file is part of the JFIF encoder/decoder for EE 382N-4 at UT Austin
# with Dr. Mark McDermott.

SHELL := bash
.ONESHELL:
.SHELLFLAGS := -eu -o pipefail -c  # "strict" bash mode
.DELETE_ON_ERROR:
# MAKEFLAGS += --warn-undefined-variables

# Will echo the value of variable $%
print-%  : ; @echo $* = $($*)

CSRC := ../../../sw/src
COBJ := $(CSRC:src=build)
TESTSRC := ../../../sw/tests/src
VSRC := ../../src

CSX := $(VSRC)/csx.sv
DCT_SA_SRC := $(addprefix $(VSRC)/,dct_sa.sv sys_array.sv coef_mat.sv)
JIFFY_SRC := $(DCT_SA_SRC) $(addprefix $(VSRC)/,qtz.sv dct.sv jiffy.sv)

IFLAGS := -I$(VSRC) -I$(CSRC) -I$(TESTSRC)
WFLAGS := -Wno-WIDTH -Wno-deprecated
VFLAGS := $(IFLAGS) $(WFLAGS)

.DEFAULT_GOAL = csx-encode

define verilate_tb
	printf '%b' "  VERILATE $^ -> $@\n"
	verilator $(VFLAGS) --trace --top-module $@ -cc --exe $^
	printf '%b' "  MAKE $@\n"
	make -C obj_dir/ -fV$@.mk
endef

.PHONY: jiffy
jiffy: tb_jiffy.cpp $(JIFFY_SRC) ; $(verilate_tb)

.PHONY: dct-sa dct_sa
dct-sa: dct_sa
dct_sa: tb_dct_sa.cpp $(CSRC)/dct.c $(DCT_SA_SRC) ; $(verilate_tb)

.PHONY: csx-decode csx_decode
csx-decode: csx_decode
csx_decode: tb_csx_decode.cpp $(CSRC)/csx.c $(CSX) ; $(verilate_tb)

.PHONY: csx-encode csx_encode
csx-encode: csx_encode
csx_encode: tb_csx_encode.cpp $(CSRC)/csx.c $(CSX) ; $(verilate_tb)

.PHONY: clean
clean:
	rm -rf obj_dir
