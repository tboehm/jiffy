// dct.c: Read and parse discrete cosine tables.
//
// Copyright (c) 2021 Trey Boehm & Jordan Pamatmat
//
// This file is part of the JFIF encoder/decoder for EE 382N-4 at UT Austin
// with Dr. Mark McDermott.

#include "dct.h"

#include <assert.h> // assert()
#include <math.h>   // cos()
#include <stdio.h>  // printf()
#include <stdlib.h> // malloc()
#include <unistd.h> // read()

#include "jpeg.h"
#include "logging.h"
#include "parse.h"

void
dct_shift(const int block[MCU_DIM], int shifted[MCU_DIM], int shamt)
{
    for (int i = 0; i < MCU_W; i++) {
        for (int j = 0; j < MCU_W; j++) {
            for (int c = 0; c < 3; c++) {
                shifted[i][j][c] = block[i][j][c] + shamt;
            }
        }
    }
}

void
dct_clamp(int block[MCU_DIM], int clamped[MCU_DIM], int min, int max)
{
    for (int i = 0; i < MCU_W; i++) {
        for (int j = 0; j < MCU_W; j++) {
            for (int c = 0; c < 3; c++) {
                int orig = block[i][j][c];
                clamped[i][j][c] = orig > max ? max
                    : orig < min ? min
                    : orig;
            }
        }
    }
}

static inline float
alpha(int u, int v)
{
    if (u != 0 && v != 0) {
        return 1;
    } else if ((u == 0 && v != 0) || (u != 0 && v == 0)) {
        return 1.f / (float)sqrt(2.f);
    } else {
        return 0.5f;
    }
}

void
dct_transform(const int block[MCU_DIM], float transformed[MCU_DIM])
{
    // Perform the actual DCT.
    for (int u = 0; u < MCU_W; u++) {
        for (int v = 0; v < MCU_W; v++) {
            // Compute G_{u,v} for each channel.
            float g_uv[3] = { 0.f };
            for (int x = 0; x < MCU_W; x++) {
                for (int y = 0; y < MCU_W; y++) {
                    float cos1 = (float)cos(((2.f * x + 1.f) * u * M_PI) / 16.f);
                    float cos2 = (float)cos(((2.f * y + 1.f) * v * M_PI) / 16.f);
                    for (int c = 0; c < 3; c++) {
                        g_uv[c] += block[x][y][c] * cos1 * cos2;
                    }
                }
            }

            for (int c = 0; c < 3; c++) {
                g_uv[c] *= alpha(u, v);
                g_uv[c] /= 4;
                transformed[u][v][c] = g_uv[c];
            }
        }
    }
}

void
dct_inverse_transform(const int block[MCU_DIM], int transformed[MCU_DIM])
{
    for (int x = 0; x < MCU_W; x++) {
        for (int y = 0; y < MCU_W; y++) {
            // Compute f_{x,y} for each channel.
            float f_xy[3] = { 0.f };
            for (int u = 0; u < MCU_W; u++) {
                for (int v = 0; v < MCU_W; v++) {
                    float cos1 = (float)cos(((2.f * x + 1.f) * u * M_PI) / 16.f);
                    float cos2 = (float)cos(((2.f * y + 1.f) * v * M_PI) / 16.f);
                    float a = alpha(u, v);
                    for (int c = 0; c < 3; c++) {
                        f_xy[c] += a * block[u][v][c] * cos1 * cos2;
                    }
                }
            }

            for (int c = 0; c < 3; c++) {
                f_xy[c] /= 4.f;
                transformed[x][y][c] = (int)lround(f_xy[c]);
            }
        }
    }
}
