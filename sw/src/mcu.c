#include "mcu.h"

#include <stdio.h> // fprintf()

#include "jpeg.h" // MCU_DIM

void
mcu_print(int mcu[MCU_DIM], int n_components)
{
    for (int comp = 0; comp < n_components; comp++) {
        fprintf(stderr, "\nComponent %d:\n", comp);
        for (int i = 0; i < MCU_W; i++) {
            fprintf(stderr, "  { ");
            for (int j = 0; j < MCU_W; j++) {
                fprintf(stderr, "%4d ", mcu[i][j][comp]);
            }
            fprintf(stderr, "}\n");
        }
        fprintf(stderr, "\n");
    }
}
