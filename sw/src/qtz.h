// qtz.h: Quantization.
//
// Copyright (c) 2021 Trey Boehm & Jordan Pamatmat
//
// This file is part of the JFIF encoder/decoder for EE 382N-4 at UT Austin
// with Dr. Mark McDermott.

#ifndef __QTZ_H__
#define __QTZ_H__

#include <stdint.h>

#include "jpeg.h"

/// Quantization matrix for 50% quality (from Wikipedia).
static const uint8_t QtzMat50[MCU_W][MCU_W] = {
    { 16, 11, 10, 16, 24,  40,  51,  61  },
    { 12, 12, 14, 19, 26,  58,  60,  55  },
    { 14, 13, 16, 24, 40,  57,  69,  56  },
    { 14, 17, 22, 29, 51,  87,  80,  62  },
    { 18, 22, 37, 56, 68,  109, 103, 77  },
    { 24, 35, 55, 64, 81,  104, 113, 92  },
    { 49, 64, 78, 87, 103, 121, 120, 101 },
    { 72, 92, 95, 98, 112, 100, 103, 99  }
};

/// Quantization matrix for 100% quality.
static const uint8_t QtzMat100[MCU_W][MCU_W] = {
    { 1, 1, 1, 1, 1, 1, 1, 1 },
    { 1, 1, 1, 1, 1, 1, 1, 1 },
    { 1, 1, 1, 1, 1, 1, 1, 1 },
    { 1, 1, 1, 1, 1, 1, 1, 1 },
    { 1, 1, 1, 1, 1, 1, 1, 1 },
    { 1, 1, 1, 1, 1, 1, 1, 1 },
    { 1, 1, 1, 1, 1, 1, 1, 1 },
    { 1, 1, 1, 1, 1, 1, 1, 1 }
};


/**
 * Quantize a block resulting from a DCT.
 */
void qtz_quantize(
    const float   block[MCU_DIM],             ///< Block to quantize
    const uint8_t y_qtz_mat[MCU_W][MCU_W],    ///< Quantization matrix for luminance
    const uint8_t cbcr_qtz_mat[MCU_W][MCU_W], ///< Quantization matrix for chroma
    int           quantized[MCU_DIM]          ///< Result of the quantization
);


/**
 * Reverse the a decoded block's quantization.
 */
void qtz_dequantize(
    const int     block[MCU_DIM],             ///< Block to de-quantize
    const uint8_t y_qtz_mat[MCU_W][MCU_W],    ///< Quantization matrix for luminance
    const uint8_t cbcr_qtz_mat[MCU_W][MCU_W], ///< Quantization matrix for chroma
    int           dequantized[MCU_DIM]        ///< Result of the de-quantization
);

#endif // __QTZ_H__
