// dqt.h: Read and parse quantization tables.
//
// Copyright (c) 2021 Trey Boehm & Jordan Pamatmat
//
// This file is part of the JFIF encoder/decoder for EE 382N-4 at UT Austin
// with Dr. Mark McDermott.

#ifndef __DQT_H__
#define __DQT_H__

#include <stdint.h>
#include <stdio.h>

#include "jpeg.h"

/// Marker for Define Quantization Table.
#define DQT_MARKER 0xffdb

/// Size of quantization table.
#define DQT_ELEMENT_COUNT (MCU_W * MCU_W)

/// Maximum number of quantization tables.
#define DQT_TABLES_MAX 4

/**
 * Quantization table definitions (marker 0xffdb).
 */
struct define_quantization_table {
    /// Always 0xffdb.
    uint16_t marker;
    /// Quantization table length.
    uint16_t length;
    /// Precision (FIXME: 0 => 8 bits, ? => 16 bits?)
    unsigned int precision : 4;
    /// Identifier (often 0)
    unsigned int identifier : 4;
    /// Table elements.
    uint8_t elements[MCU_W][MCU_W];
};

/**
 * Print the DQT table[s].
 */
void
dqt_print_table(
    struct define_quantization_table *dqt ///< Table to read into
);

// Forward-declaration for the JFIF file struct.
struct jfif_file;

/**
 * Read the DQT table[s].
 *
 * @return 0 on success, otherwise 1.
 */
int
dqt_read_table(
    uint8_t                         **file_data, ///< Buffer containing file data
    struct define_quantization_table *dqt,       ///< Table to read into
    struct jfif_file                 *file       /// JFIF file, in case we read multiple tables
);
#endif // __DQT_H__
