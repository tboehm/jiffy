// decode.c: Decode a JFIF file.
//
// Copyright (c) 2021 Trey Boehm & Jordan Pamatmat
//
// This file is part of the JFIF encoder/decoder for EE 382N-4 at UT Austin
// with Dr. Mark McDermott.

#include "decode.h"

#include <stdbool.h> // true/false
#include <stdio.h>   // printf()
#include <string.h>  // memset()
#include <unistd.h>  // read(), lseek(), etc.

#include "csx.h"
#include "dct.h"
#include "huffman.h"
#include "jfif.h"
#include "jpeg.h"
#include "logging.h"
#include "mcu.h"
#include "parse.h"
#include "profile.h"
#include "qtz.h"
#include "scan.h"

#ifdef ACCELERATOR
#include "interface.h"
#endif // ACCELERATOR

struct huffman_state {
    uint8_t **file_data;
    int       file_length;
    uint32_t  buffer;
    int       bit_position;
    uint16_t  huffman_data;
    uint8_t   previous_prefix_length;
    uint8_t   last_byte;
    bool      end_of_image;
    bool      reached_eof;
};

static struct huffman_state HuffmanState = { 0 };

#if LOGGING_LEVEL >= LEVEL_SPEW

static inline void
print_huffman_state(void)
{
    log_spew(
        "buffer = 0x%.8x, "
        "bit_position = %d, "
        "last_byte = 0x%.2x, "
        "end_of_image = %d, "
        "reached_eof = %d",
        HuffmanState.buffer,
        HuffmanState.bit_position,
        HuffmanState.last_byte,
        HuffmanState.end_of_image,
        HuffmanState.reached_eof
    );
}
#else // LOGGING_LEVEL < LEVEL_SPEW
#define print_huffman_state()
#endif

static inline uint16_t
next_huffman_data(void)
{
    print_huffman_state();

    int n_bits = HuffmanState.previous_prefix_length;

    while (HuffmanState.bit_position <= 24) {
        uint8_t byte = **HuffmanState.file_data;
        *HuffmanState.file_data += 1;

        int marker = (HuffmanState.last_byte << 8) | byte;

        if (marker == PADDING) {
            // The last byte was 0xff and this byte was 0x00. Leave the 0xff in place, but just skip
            // this 0x00.
            log_debug("Skipping padding");
        } else if (marker == JFIF_EOI_MARKER) {
            log_info("Hit EOI");
            HuffmanState.end_of_image = true;
            HuffmanState.buffer = (HuffmanState.buffer << 8) | byte;
            HuffmanState.bit_position += 8;
        } else if ((marker & 0xffd0) == 0xffd0) {
            log_error("Hit an unknown special marker: 0x%.4x", marker);
        } else if (marker == 0xfffe) {
            log_warning("Hit a comment");
        } else if (HuffmanState.last_byte == 0xff) {
            log_warning("LastByte = 0xff, marker = 0x%.4x", marker);
        } else {
            // Insert this byte like normal
            HuffmanState.buffer = (HuffmanState.buffer << 8) | byte;
            HuffmanState.bit_position += 8;
        }
        HuffmanState.last_byte = byte;
    }

    // e.g.: BitPosition = 24, n_bits = 4
    // -> Need to shift down 20 and select just those 4 bits.
    int      shift_width = HuffmanState.bit_position - n_bits;
    unsigned mask = (1 << n_bits) - 1;
    unsigned result = (HuffmanState.buffer >> shift_width) & mask;

    // Move the current bit position down towards 0
    HuffmanState.bit_position -= n_bits;

    // Update the internal state
    HuffmanState.huffman_data <<= HuffmanState.previous_prefix_length;
    HuffmanState.huffman_data |= (uint16_t)result;

    return HuffmanState.huffman_data;
}

static inline uint8_t
get_code(struct huffman_lut *lut, uint16_t huffman_data)
{
    uint8_t code;

    huffman_get_code(lut, huffman_data, &code, &HuffmanState.previous_prefix_length);

    return code;
}

static const int ZigZagIndices[] = {
    0,  1,  8,  16,  9,  2,   3,  10,
    17, 24, 32, 25,  18, 11,  4,  5,
    12, 19, 26, 33,  40, 48,  41, 34,
    27, 20, 13, 6,   7,  14,  21, 28,
    35, 42, 49, 56,  57, 50,  43, 36,
    29, 22, 15, 23,  30, 37,  44, 51,
    58, 59, 52, 45,  38, 31,  39, 46,
    53, 60, 61, 54,  47, 55,  62, 63,
};

static int
huffman_decode_mcu(int mcu[MCU_DIM], struct jfif_file *file, int prev_dc[3])
{
    // Use these to interact with the scan data. Everything else is hidden in HuffmanState.
    uint16_t huffman_data = 0;
    uint8_t  code = 0;

    int coef_index = 0;

    for (int comp_ac_dc = 0; comp_ac_dc < file->shdr.n_components * 2; comp_ac_dc++) {
        unsigned                     table_index = file->dht_for_component[comp_ac_dc];
        struct define_huffman_table *dht = &file->dht[table_index];
        struct huffman_lut          *lut = &dht->lut;
        bool                         is_dc = (comp_ac_dc & 1) == 0;
        int                          component = comp_ac_dc / 2;

        if (is_dc) {
            // Reset the index.
            coef_index = 0;
        }

        log_debug("Component %d, dc = %d (Table %d)", component, is_dc, table_index);

        for (int prefixes_read = 0; coef_index < 64 && prefixes_read < 64; prefixes_read++) {
            huffman_data = next_huffman_data();

            code = get_code(lut, huffman_data);

            if (is_dc) {
                // For DC tables, the code tells us how many bits to read for the DC offset.
                if (code == 0) {
                    // No more bits. The DC offset is 0.
                    log_debug("DC code 0x%x -> DC offset 0", code);
                    prev_dc[component] += 0;
                    mcu[0][0][component] = prev_dc[component];
                    coef_index = 1;
                    break;
                }

                // If the code is nonzero, we need to read a few more bits to get the DC offset.
                huffman_data = next_huffman_data();

                int  shift_width = 16 - code;
                int  dc_offset = (int)((unsigned)huffman_data >> shift_width);
                int  sign_bit_pos = (1 << (code - 1));
                bool positive = (dc_offset & sign_bit_pos) != 0;
                int  min_code = -((1 << code) - 1);
                dc_offset = positive ? dc_offset : min_code + dc_offset;

                prev_dc[component] += dc_offset;
                mcu[0][0][component] = prev_dc[component];

                log_debug("DC code 0x%x -> offset %d.", code, dc_offset);

                coef_index += 1;

                // Since we read bits for the offset value, the next `next_huffman_data()` call
                // will need to replace that many.
                HuffmanState.previous_prefix_length = code;
                break;
            }

            if (code == EOB) {
                // End of block.
                log_debug("Reached end-of-block for component %d", component);
                break;
            }

            if (code == ZRL) {
                // 16 zeros
                log_debug("Hit a ZRL");
                // continue;
                for (int i = 0; i < 16; i++, coef_index++) {
                    if (coef_index >= 64) {
                        log_error("Coefficient index %d is too large", coef_index);
                        return 1;
                    }
                    int zigzag_index = ZigZagIndices[coef_index];
                    int row = zigzag_index / 8;
                    int col = zigzag_index % 8;
                    log_debug("mcu[%d][%d][%d] <- ZRL (coef %d)", row, col, component, coef_index);
                    mcu[row][col][component] = 0;
                }
                continue;
            }

            // An AC code tells us how many zeros precede its value...
            int zeros = (code & 0xf0) >> 4;
            // ... and how many bits to read to get the value.
            uint8_t value_bits = (code & 0x0f);

            // Get the actual value.
            huffman_data = next_huffman_data();

            HuffmanState.previous_prefix_length = value_bits;

            unsigned value_mask = (1 << value_bits) - 1;
            int      shift_width = 16 - value_bits;

            int  value = (huffman_data >> shift_width) & value_mask;
            int  sign_bit_pos = (1 << (value_bits - 1));
            bool positive = (value & sign_bit_pos) != 0;
            int  min_code = -((1 << value_bits) - 1);
            value = positive ? value : min_code + value;

            for (int i = 0; i < zeros + 1; i++, coef_index++) {
                if (coef_index >= 64) {
                    log_error("Coefficient index %d is too large", coef_index);
                    coef_index = 63;
                    return 1;
                }
                int zigzag_index = ZigZagIndices[coef_index];
                int row = zigzag_index / 8;
                int col = zigzag_index % 8;
                if (i < zeros) {
                    mcu[row][col][component] = 0;
                } else {
                    mcu[row][col][component] = value;
                }
            }
        }
    }

    return 0;
}

int
decode_file(struct jfif_file *file, uint8_t *image_data)
{
    log_info("Decode scan data");

    int image_width = file->sof0.samples_per_line;
    int image_height = file->sof0.lines;

    if (image_width % 8 != 0 || image_height % 8 != 0) {
        log_error("Decoding images with non-multiple-of-8 dimensions is not supported.");
        return 1;
    }

    int x_mcus = image_width / 8;
    int y_mcus = image_height / 8;
    int mcu_count = x_mcus * y_mcus;

    log_info("Decoding %d MCUs", mcu_count);

    HuffmanState.file_data = &file->data;
    HuffmanState.previous_prefix_length = 16; // Read 16 bits on the first iteration.

    // Starting indices for writing a given MCU into the BMP data.
    int x_start = 0;
    int y_start = image_height - MCU_W;

    int mcu[MCU_DIM];
    int prev_dc_offset[3] = { 0 }; // One for each component

    long huffman_microseconds = 0;
    long math_microseconds = 0;

    for (int mcu_remaining = mcu_count; mcu_remaining > 0; mcu_remaining--) {
        log_info("MCUs remaining: %d", mcu_remaining);

        memset(mcu, 0, sizeof(int) * MCU_W * MCU_W * 3);

        // Read Huffman prefixes and reconstruct the DCT coefficients.
        if (huffman_decode_mcu(mcu, file, prev_dc_offset)) {
            return 1;
        }

        huffman_microseconds += profile_timestamp(NULL);

        // Use either hardware or software to decode the MCU.
#ifdef ACCELERATOR
        // To start out, just use single buffering.
        interface_send_mcu(mcu);
        interface_receive_mcu(mcu, JIFFY_MODE_DECODE);
#else
        decode_mcu(mcu, file);
#endif // ACCELERATOR

        math_microseconds += profile_timestamp(NULL);

        // Copy the MCU into the image data array.
        for (int i = 0, y_offset = image_width * (y_start + 7);
             i < MCU_W;
             i++, y_offset -= image_width) {
            for (int j = 0, x_offset = x_start; j < MCU_W; j++, x_offset++) {
                for (int c = 0; c < 3; c++) {
                    uint8_t pix = (uint8_t)mcu[i][j][c];
                    int     idx = 4 * (x_offset + y_offset) + (2 - c);
                    image_data[idx] = pix;
                }
            }
        }

        // Adjust the position we're writing into the MCU.
        if ((mcu_remaining - 1) % x_mcus == 0) {
            x_start = 0;
            y_start -= 8;
        } else {
            x_start += 8;
        }
    }

    log_timestamp("Math", math_microseconds);
    log_timestamp("Huffman", huffman_microseconds);

    if (HuffmanState.end_of_image) {
        log_info("Scan complete.");
        return 0;
    }

    log_error("Scan complete, but end-of-image marker not found.");
    return 1;
}

void
decode_mcu(int mcu[MCU_DIM], struct jfif_file *file)
{
    int pre_dct[MCU_DIM];

    int y_qtz_mat_idx = file->sof0.components[Y_IDX].qt_select;
    // FIXME: Enforce the assumption that Cb and Cr always have the same quantization matrix.
    int cbcr_qtz_mat_idx = file->sof0.components[CB_IDX].qt_select;

    qtz_dequantize(
        mcu,
        file->dqt[y_qtz_mat_idx].elements,
        file->dqt[cbcr_qtz_mat_idx].elements,
        pre_dct
    );

    dct_inverse_transform(pre_dct, mcu);
    dct_shift(mcu, mcu, 128);
    dct_clamp(mcu, mcu, 0, 255);

    csx_ycbcr_to_rgb(mcu, mcu);
    dct_clamp(mcu, mcu, 0, 255);
}
