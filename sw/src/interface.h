// interface.h: Interface between software and hardware.
//
// Copyright (c) 2021 Trey Boehm & Jordan Pamatmat
//
// This file is part of the JFIF encoder/decoder for EE 382N-4 at UT Austin
// with Dr. Mark McDermott.

#ifndef __INTERFACE_H__
#define __INTERFACE_H__

#include <signal.h>   // sighandler_t
#include <stdint.h>   // uint32_t and friends
#include <sys/mman.h> // mmap flags

#include "jpeg.h" // MCU_W, MCU_DIM

/// Permissions for mmap'ing devices.
#define MMAP_PERMISSIONS (PROT_READ | PROT_WRITE)

/// Flags for mmap'ing devices.
#define MMAP_FLAGS MAP_SHARED

/// CDMA base address.
#define CDMA 0xB0000000

/// Size of CDMA mmap'd region
#define CDMA_SIZE 0x1000

// DMA commands.
#define CDMACR            0x00
#define CDMASR            0x04
#define CURDESC_PNTR      0x08
#define CURDESC_PNTR_MSB  0x0C
#define TAILDESC_PNTR     0x10
#define TAILDESC_PNTR_MSB 0x14
#define SA                0x18
#define SA_MSB            0x1C
#define DA                0x20
#define DA_MSB            0x24
#define BTT               0x28

/// BRAM base address for CDMA.
#define BRAM_CDMA 0xB0006000

/// OCM base address (for writing).
#define OCM 0xFFFC0000

/// Size of OCM mmap'd region
#define OCM_SIZE 0x10000

/// Offset into OCM for reading.
#define OCM_READ_OFFSET 0x2000

/// Size of the capture timer mmap'd region.
#define TIMER_SIZE 0x1000

/// Mask for timer addresses.
#define TIMER_MASK (TIMER_SIZE - 1)

/// Timer registers.
#define TIMER_BASE 0xA0029000

/// Timer capture registers.
#define TIMER_CAPTURE_R               0
#define TIMER_CAPTURE_GATE_IDX        0
#define TIMER_CAPTURE_COMPLETE_IDX    4
#define TIMER_CAPTURE_TIMERENABLE_IDX 7

/// Timer interrupt register.
#define TIMER_INT_R               1
#define TIMER_INT_OUT_IDX         0
#define TIMER_INT_TIMERENABLE_IDX 1

/// Timer value register.
#define TIMER_VALUE_R 3

/// Timer state register (for debug).
#define TIMER_STATE_R 4

/// Size of the Jiffy mmap'd region.
#define JIFFY_SIZE 0x1000

/// Jiffy device base address.
#define JIFFY_BASE 0xA0028000

/// Number of blocks for Jiffy to process.
#define JIFFY_BLOCKS_R 0

/// Control and status for Jiffy.
#define JIFFY_CONTROL_STATUS_R 1

// Bits in the CSR.
#define JIFFY_CSR_START_S 0
#define JIFFY_CSR_START_M (1 << JIFFY_CSR_START_S)
#define JIFFY_CSR_RESET_S 1
#define JIFFY_CSR_RESET_M (1 << JIFFY_CSR_RESET_S)
#define JIFFY_CSR_MODE_S  2
#define JIFFY_CSR_MODE_M  (1 << JIFFY_CSR_MODE_S)

// Mode encodings.
#define JIFFY_MODE_ENCODE 0
#define JIFFY_MODE_DECODE 1

/// Acknowledge the interrupt for Jiffy block processing completion.
#define JIFFY_INTERRUPT_R 3

// Bits in the interrupt register.
#define JIFFY_INT_ACK_S 0
#define JIFFY_INT_ACK_M (1 << JIFFY_INT_ACK_S)

/// First hardware buffer.
#define BUFFER_A 0

/// Second hardware buffer.
#define BUFFER_B 1

/// Get the offset to read from a buffer.
#define BUFFER_READ_OFFSET(buf_num) ((buf_num) << 10)

/// Get the offset to write to a buffer.
#define BUFFER_WRITE_OFFSET(buf_num) (((buf_num) << 10) | 0x200)

/// Offset into the BRAM for writing quantization matrices.
#define QTZ_OFFSET 0x800

/// Magic byte so hardware knows when the DMA is complete.
#define BUFFER_MAGIC 0xf0

/// Always transfer 3 8x8 MCUs (each entry is 2 bytes) plus the magic byte (word).
#define TRANSFER_SIZE (3 * 8 * 8 * 2 + 4)

/// One-bit masks for bits 0-31.
#define ONE_BIT_MASK(_bit) (0x00000001 << (_bit))

/// Signal for Jiffy interrupts.
#define JIFFY_SIGNAL SIGUSR1

/// Signal for CDMA interrupts.
#define CDMA_SIGNAL SIGUSR2

// Path to Jiffy kernel module/device.
static const char JiffyDevPath[] = "/dev/jiffy_int";

// Path to CDMA kernel module/device.
static const char CdmaDevPath[] = "/dev/cdma_int";

/// Information needed to register a device signal handler.
struct kernel_handler {
    const char  *dev_path; ///< Path to device
    int          signo;    ///< Signal number
    sighandler_t handler;  ///< Signal handler
};

/**
 * Initialize the hardware/software interface.
 *
 * Map addresses for devices and set up signals for kernel modules.
 *
 * @return 0 on success, otherwise 1.
 */
int
interface_open();

/**
 * Tear down the hardware/software interface.
 *
 * Unmap addresses.
 *
 * @return 0 on success, otherwise 1.
 */
int
interface_close();

/**
 * Start the accelerator.
 *
 * Send quantization tables to hardware, set the mode, then tell the device to start polling BRAM
 * for the magic byte.
 */
void
interface_initialize_accelerator(
    int           mcu_count,               ///< Number of MCUs to process
    int           mode,                    ///< Decode or encode
    const uint8_t qtz_tab_0[MCU_W][MCU_W], ///< Quantization table for Y or R
    const uint8_t qtz_tab_1[MCU_W][MCU_W], ///< Quantization table for Cb or G
    const uint8_t qtz_tab_2[MCU_W][MCU_W]  ///< Quantization table for Cr or B
);

/**
 * Reset the accelerator.
 */
void
interface_reset_accelerator();

/**
 * Send an MCU to the accelerator.
 *
 * Static state in interface.c keeps track of which buffer to send to.
 */
void
interface_send_mcu(
    int mcu[MCU_DIM]
);

/**
 * Receive an MCU from the accelerator.
 *
 * Static state in interface.c keeps track of which buffer to receive from.
 */
void
interface_receive_mcu(
    int mcu[MCU_DIM], ///< MCU to receive into
    int mode          ///< Receive for decode or encode
);

/**
 * Print statistics about interrupt latency.
 */
void
interface_print_interrupt_stats();


#endif // __INTERFACE_H__
