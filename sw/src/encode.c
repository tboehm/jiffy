// encode.c: Encode raw image data in the JPEG format.
//
// Copyright (c) 2021 Trey Boehm & Jordan Pamatmat
//
// This file is part of the JFIF encoder/decoder for EE 382N-4 at UT Austin
// with Dr. Mark McDermott.

#include "encode.h"

#include <stdbool.h> // bool, true/false
#include <stdint.h>  // uint8_t, etc.
#include <stdlib.h>  // calloc(), realloc()

#include "csx.h"
#include "dct.h"
#include "huffman.h"
#include "jfif.h"
#include "jpeg.h"
#include "logging.h"
#include "mcu.h"
#include "profile.h"
#include "qtz.h"

#ifdef ACCELERATOR
#include "interface.h"
#endif // ACCELERATOR

static long huffman_usec;
static long math_usec;

static inline int
bits_for_value(int value)
{
    int negative = value < 0;

    value = negative ? 0 - value : value;

    int num_bits = 0;

    while (value) {
        value >>= 1;
        num_bits += 1;
    }

    return num_bits;
}

uint16_t
encode_get_value_bits(int value, int n_bits)
{
    int  mask = (1 << n_bits) - 1;
    int  min_code = -((1 << (n_bits)) - 1);
    bool negative = value < 0;

    value = negative ? 0 - value : value;

    int value_bits = negative ? min_code + value : value;

    if (negative) {
        int sign_bit_pos = 1 << (n_bits);
        value_bits = ~value_bits;
        value_bits += 1;
        value_bits &= ~sign_bit_pos;
    }

    value_bits &= mask;

    return (uint16_t)value_bits;
}

uint16_t
encode_get_prefix(int n_bits, int zeros, int *prefix_length, const struct huffman_code *prefixes)
{
    int code = (zeros << 4) | n_bits;

    *prefix_length = prefixes[code].prefix_length;

    return prefixes[code].prefix;
}

uint32_t
encode_prefix_and_value(int value, int zeros, const struct huffman_code *prefixes, int *bit_count)
{
    int n_value_bits = bits_for_value(value);
    int n_prefix_bits;

    uint32_t prefix = encode_get_prefix(n_value_bits, zeros, &n_prefix_bits, prefixes);
    uint16_t value_bits = encode_get_value_bits(value, n_value_bits);

    uint32_t prefix_and_value = (prefix << n_value_bits) | value_bits;

    log_spew(
        "value %d, zeros %d -> %d bits, prefix 0x%x, value bits 0x%x, prefix_and_value 0x%.8x",
        value,
        zeros,
        n_value_bits,
        prefix,
        value_bits,
        prefix_and_value
    );

    *bit_count = n_prefix_bits + n_value_bits;

    return prefix_and_value;
}

static const int ZigZagIndices[] = {
    0,  1,  8,  16, 9,  2,  3,  10,
    17, 24, 32, 25, 18, 11, 4,  5,
    12, 19, 26, 33, 40, 48, 41, 34,
    27, 20, 13, 6,  7,  14, 21, 28,
    35, 42, 49, 56, 57, 50, 43, 36,
    29, 22, 15, 23, 30, 37, 44, 51,
    58, 59, 52, 45, 38, 31, 39, 46,
    53, 60, 61, 54, 47, 55, 62, 63,
};

// JPEG binary data / buffer state
static uint8_t buffer = 0;
static int     byte_position = 0;
static int     bit_position = 0;
static int     buffer_length = 0;

static void
flush_buffer(uint8_t *binary_data)
{
    if (bit_position) {
        buffer <<= (8 - bit_position);
        binary_data[byte_position++] = buffer;
        bit_position = 0;
        buffer = 0;
    }
}

static void
add_eoi(uint8_t *binary_data)
{
    binary_data[byte_position++] = 0xff;
    binary_data[byte_position++] = 0xd9;
}

static void
write_binary_data(uint32_t data, int n_bits, uint8_t *binary_data)
{
    // static const int top_shift = 64 - 8;
    // static const uint64_t top_mask = (uint64_t)0xff << (64 - 8);

    // Push data up to the top of the word.
    data <<= (32 - n_bits);

    if (byte_position + 4 > buffer_length) {
        log_info("realloc'ing binary data buffer");
        buffer_length *= 2;
        binary_data = (uint8_t *)realloc(binary_data, (size_t)buffer_length);
        if (!binary_data) {
            log_perror("realloc");
            exit(EXIT_FAILURE);
        }
    }

    while (n_bits) {
        if (bit_position > 0 || n_bits < 8) {
            // If there's any data in the byte buffer or we have less than a byte left, add data to
            // the buffer and potentially write it to the byte array.
            int bits_remaining = 8 - bit_position;
            int shift_amount = (bits_remaining >= n_bits)
                ? n_bits
                : bits_remaining;
            buffer <<= shift_amount;
            bit_position += shift_amount;
            n_bits -= shift_amount;

            // Write part of `data` into the buffer.
            unsigned mask = (1 << shift_amount) - 1;
            mask <<= (32 - shift_amount); // data is at the top of the word.
            buffer |= (data & mask) >> (32 - shift_amount);
            data <<= shift_amount;

            if (bit_position == 8) {
                // Write the top shift_amount bits of data into the buffer.
                binary_data[byte_position++] = buffer;

                if (buffer == 0xff) {
                    binary_data[byte_position++] = 0x00;
                }

                // Clear the buffer.
                buffer = 0;
                bit_position = 0;
            }
        } else {
            // Easier case: just write a byte to the array.
            // uint8_t byte = (uint8_t)((data & top_mask) >> top_shift);
            uint8_t byte = (data & 0xff000000) >> 24;
            n_bits -= 8;
            data <<= 8;
            binary_data[byte_position++] = byte;
            if (byte == 0xff) {
                binary_data[byte_position++] = 0x00;
            }
        }
    }
}

void
encode_mcu(int mcu[MCU_DIM], int prev_dc[3], struct jfif_file *file)
{
#ifndef ACCELERATOR
    /////////////////////////////////
    // Math portion (if not in HW) //
    /////////////////////////////////

    float transformed_mcu[MCU_DIM];

    // 1. Colorspace transformation.
    csx_rgb_to_ycbcr(mcu, mcu);

    // 2. Discrete cosine transform.
    dct_shift(mcu, mcu, -128);
    dct_clamp(mcu, mcu, -128, 127);
    dct_transform(mcu, transformed_mcu);

    // 3. Quantization. Hard-code tables for now, because of laziness.
    qtz_quantize(transformed_mcu, file->dqt[0].elements, file->dqt[1].elements, mcu);

    math_usec += profile_timestamp(NULL);
#endif // ACCELERATOR

    ////////////////////////////
    // Huffman coding portion //
    ////////////////////////////

    for (int c = 0; c < 3; c++) {
        int chroma = c > 0;

        // DC value
        int value = mcu[0][0][c];
        int prev = value - prev_dc[c];

        prev_dc[c] = value;
        value = prev;

        int table_select = chroma;
        int class_id = HuffmanStandardTableClassIds[table_select];

        const struct huffman_code *table = HuffmanStandardTables[table_select];

        int      bit_count;
        uint32_t p_v = encode_prefix_and_value(value, 0, table, &bit_count);
        write_binary_data(p_v, bit_count, file->data);

        int n_zeros = 0;

        table_select = (1 << 1) | chroma;
        class_id = HuffmanStandardTableClassIds[table_select];
        table = HuffmanStandardTables[table_select];

        int ij;
        for (ij = 1; ij < MCU_W * MCU_W; ij++) {
            int zz = ZigZagIndices[ij];
            int zi = zz / 8;
            int zj = zz % 8;
            value = mcu[zi][zj][c];

            if (value == 0) {
                n_zeros++;
                continue;
            }

            while (n_zeros >= 16) {
                // Take care of ZRL(s). We can use the same encode_prefix_and_value() function
                // since ZRL follows the pattern: the value zero followed by 15 zeros.
                p_v = encode_prefix_and_value(0, 15, table, &bit_count);
                write_binary_data(p_v, bit_count, file->data);
                n_zeros -= 16;
            }

            // Now, take care of this value.
            p_v = encode_prefix_and_value(value, n_zeros, table, &bit_count);
            write_binary_data(p_v, bit_count, file->data);
            n_zeros = 0;
        }

        // Add EOB to the binary data stream.
        if (ij < 64 || n_zeros) {
            log_debug("ij = %d, n_zeros = %d -> writing EOB", ij, n_zeros);
            p_v = encode_prefix_and_value(EOB, 0, table, &bit_count);
            write_binary_data(p_v, bit_count, file->data);
        }
    }

    huffman_usec += profile_timestamp(NULL);

#if LOGGING_LEVEL >= LEVEL_DEBUG
    log_debug("Encoded MCU:");
    int n_components = file->sof0.component_count;
    mcu_print(mcu, n_components);
#endif // LOGGING_LEVEL >= LEVEL_DEBUG
}

int
encode_file(struct jfif_file *file, uint8_t *image_data)
{
    huffman_usec = 0L;
    math_usec = 0L;

    int image_width = file->sof0.samples_per_line;
    int image_height = file->sof0.lines;

    if (image_width % 8 != 0 || image_height % 8 != 0) {
        log_error("Encoding images with non-multiple-of-8 dimensions is not supported.");
        return 1;
    }

    // Estimate that the buffer will require as much data as the bitmap.
    buffer_length = image_width * image_height * 3;
    file->data = (uint8_t *)calloc((size_t)buffer_length, sizeof(uint8_t));
    if (!file->data) {
        log_perror("calloc");
        return 1;
    }

    int x_mcus = image_width / 8;
    int y_mcus = image_height / 8;
    int mcu_count = x_mcus * y_mcus;

    // Starting indices for writing a given MCU into the BMP data.
    int x_start = 0;
    int y_start = image_height - MCU_W;

    int mcu[MCU_DIM];
    int prev_dc_offset[3] = { 0 }; // One for each component

    for (int mcu_remaining = mcu_count; mcu_remaining > 0; mcu_remaining--) {
        log_info("MCUs remaining: %d, x_start = %d, y_start = %d", mcu_remaining, x_start, y_start);

        for (int i = 0, y_offset = image_width * (y_start + 7);
             i < MCU_W;
             i++, y_offset -= image_width) {
            for (int j = 0, x_offset = x_start; j < MCU_W; j++, x_offset++) {
                for (int c = 0; c < 3; c++) {
                    int     idx = 4 * (x_offset + y_offset) + (2 - c);
                    uint8_t pix = image_data[idx];
                    mcu[i][j][c] = (int)pix;
                }
            }
        }

#ifdef ACCELERATOR
        // To start out, just use single buffering.
        interface_send_mcu(mcu);
        interface_receive_mcu(mcu, JIFFY_MODE_ENCODE);
#endif // ACCELERATOR
        encode_mcu(mcu, prev_dc_offset, file);

        // Adjust the position we're writing into the MCU.
        if ((mcu_remaining - 1) % x_mcus == 0) {
            x_start = 0;
            y_start -= 8;
        } else {
            x_start += 8;
        }
    }

    // Get the EOI at a byte-aligned boundary.
    flush_buffer(file->data);
    add_eoi(file->data);

    file->data_length = (size_t)byte_position;

    log_timestamp("Math", math_usec);
    log_timestamp("Huffman", huffman_usec);

    return 0;
}
