// dwn.h: Downsampling.
//
// Copyright (c) 2021 Trey Boehm & Jordan Pamatmat
//
// This file is part of the JFIF encoder/decoder for EE 382N-4 at UT Austin
// with Dr. Mark McDermott.

#ifndef __DWN_H__
#define __DWN_H__

#include "jpeg.h"

#define DWN_REGION_W 4
#define DWN_REGION_H 2

enum DownsampleRatio {
    DWN_411,
    DWN_420,
    DWN_422,
    DWN_444,
    DWN_440,
    DWN_COUNT,
};

struct downsample_ratio {
    int samples_per_row;
    int changes_between_rows;
};

static const struct downsample_ratio DownsampleRatios[] = {
    [DWN_411] = { 1, 1 },
    [DWN_420] = { 2, 0 },
    [DWN_422] = { 2, 2 },
    [DWN_444] = { 4, 4 },
    [DWN_440] = { 4, 0 },
};

/**
 * Downsample a block.
 */
void
dwn_downsample(
    const int            block[MCU_DIM],       ///< Block to downsample
    int                  downsampled[MCU_DIM], ///< Result of the downsampling
    enum DownsampleRatio ratio                 ///< Downsampling ratio to apply
);
#endif // __DWN_H__
