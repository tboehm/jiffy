// qtz.c: Quantization.
//
// Copyright (c) 2021 Trey Boehm & Jordan Pamatmat
//
// This file is part of the JFIF encoder/decoder for EE 382N-4 at UT Austin
// with Dr. Mark McDermott.

#include "qtz.h"

#include <math.h> // lroundf()

#include "jpeg.h"

void
qtz_quantize(const float block[MCU_DIM], const uint8_t y_qtz_mat[MCU_W][MCU_W],
    const uint8_t cbcr_qtz_mat[MCU_W][MCU_W], int quantized[MCU_DIM])
{
    for (int i = 0; i < MCU_W; i++) {
        for (int j = 0; j < MCU_W; j++) {
            quantized[i][j][Y_IDX] = (int)lroundf(block[i][j][Y_IDX] / y_qtz_mat[i][j]);
            quantized[i][j][CB_IDX] = (int)lroundf(block[i][j][CB_IDX] / cbcr_qtz_mat[i][j]);
            quantized[i][j][CR_IDX] = (int)lroundf(block[i][j][CR_IDX] / cbcr_qtz_mat[i][j]);
        }
    }
}

void
qtz_dequantize(const int block[MCU_DIM], const uint8_t y_qtz_mat[MCU_W][MCU_W],
    const uint8_t cbcr_qtz_mat[MCU_W][MCU_W], int dequantized[MCU_DIM])
{
    for (int i = 0; i < MCU_W; i++) {
        for (int j = 0; j < MCU_W; j++) {
            dequantized[i][j][Y_IDX] = block[i][j][Y_IDX] * y_qtz_mat[i][j];
            dequantized[i][j][CB_IDX] = block[i][j][CB_IDX] * cbcr_qtz_mat[i][j];
            dequantized[i][j][CR_IDX] = block[i][j][CR_IDX] * cbcr_qtz_mat[i][j];
        }
    }
}
