// csx.h: Colorspace transformation.
//
// Copyright (c) 2021 Trey Boehm & Jordan Pamatmat
//
// This file is part of the JFIF encoder/decoder for EE 382N-4 at UT Austin
// with Dr. Mark McDermott.

#ifndef __CSX_H__
#define __CSX_H__

#include "jpeg.h"

/**
 * Transform an 8x8 block from RGB to YCbCr colorspace.
 */
void
csx_rgb_to_ycbcr(
    const int block[MCU_DIM],      ///< Block to transform
    int       transformed[MCU_DIM] ///< Result of the transformation
);

/**
 * Transform an 8x8 block from YCbCr to RGB colorspace.
 */
void
csx_ycbcr_to_rgb(
    const int block[MCU_DIM],      ///< Block to transform
    int       transformed[MCU_DIM] ///< Result of the transformation
);

#endif // __CSX_H__
