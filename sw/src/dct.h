// dct.h: Read and parse discrete cosine tables.
//
// Copyright (c) 2021 Trey Boehm & Jordan Pamatmat
//
// This file is part of the JFIF encoder/decoder for EE 382N-4 at UT Austin
// with Dr. Mark McDermott.

#ifndef __DCT_H__
#define __DCT_H__

#include <stdint.h>

#include "jpeg.h"

/**
 * Shift a block by some amount.
 *
 * This is used to center a block at 0 before DCT, or recenter at 128 after the inverse.
 */
void
dct_shift(
    const int block[MCU_DIM],   ///< Block to shift
    int       shifted[MCU_DIM], ///< Shifted block
    int       shamt             ///< Amount by which to shift
);

/**
 * Clamp values in a block within some bounds.
 */
void
dct_clamp(
    int block[MCU_DIM],   ///< Block to clamp
    int clamped[MCU_DIM], ///< Clamped values
    int min,              ///< Minimum value
    int max               ///< Maximum value
);

/**
 * Do a discrete cosine transform on all 3 channels of an 8x8 block.
 */
void
dct_transform(
    const int block[MCU_DIM],      ///< Block to transform
    float     transformed[MCU_DIM] ///< Result of the transformation
);

/**
 * Do an inverse discrete cosine transform on all 3 channels of an 8x8 block.
 */
void
dct_inverse_transform(
    const int block[MCU_DIM],      ///< Block to transform
    int       transformed[MCU_DIM] ///< Result of the transformation
);

#endif // __DCT_H__
