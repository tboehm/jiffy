// jpeg.h: JPEG encoder/decoder helper functions, macros, and constants.
//
// Copyright (c) 2021 Trey Boehm & Jordan Pamatmat
//
// This file is part of the JFIF encoder/decoder for EE 382N-4 at UT Austin
// with Dr. Mark McDermott.

#ifndef __JPEG_H__
#define __JPEG_H__

#include <stdint.h>

/// Width of the minimum coded unit.
#define MCU_W 8

/// Dimensions of the multi-dimensional minimum coded unit array.
#define MCU_DIM MCU_W][MCU_W][3

/// Red channel index.
#define R_IDX 0
/// Green channel index.
#define G_IDX 1
/// Blue channel index.
#define B_IDX 2

/// Y (luma) channel index.
#define Y_IDX 0
/// Cb (blue difference) channel index.
#define CB_IDX 1
/// Cr (red difference) channel index.
#define CR_IDX 2

#endif // __JPEG_H__
