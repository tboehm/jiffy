// bmp.c: Read and write bitmap files.
//
// Copyright (c) 2021 Trey Boehm & Jordan Pamatmat
//
// This file is part of the JFIF encoder/decoder for EE 382N-4 at UT Austin
// with Dr. Mark McDermott.

#include "bmp.h"

#include <fcntl.h>  // open() and flags
#include <stdint.h> // uint32_t, etc.
#include <stdlib.h> // malloc()
#include <string.h> // memset()
#include <unistd.h> // system calls

#include "logging.h"
#include "parse.h"

void
bmp_print_bmp_header(struct bmp_header *bmp)
{
    printf(
        "BMP header:\n"
        "  Signature:   0x%.4x\n"
        "  File size:   %u\n"
        "  _reserved_1: 0x%.4x\n"
        "  _reserved_2: 0x%.4x\n"
        "  Data offset: %u\n",
        bmp->signature,
        bmp->file_size,
        bmp->_reserved_1,
        bmp->_reserved_2,
        bmp->data_offset
    );
}

void
bmp_print_dib_header(struct dib_header *dib)
{
    printf(
        "DIB header:\n"
        "  Size: %u\n"
        "  Width: %u\n"
        "  Height: %u\n"
        "  Planes: %u\n"
        "  Color depth: %u\n"
        "  Compression: %u\n"
        "  Image size: %u\n"
        "  Horizontal resolution: %u\n"
        "  Vertical resolution: %u\n"
        "  Palette colors: %u\n"
        "  Important colors: %u\n",
        dib->size,
        dib->width,
        dib->height,
        dib->planes,
        dib->color_depth,
        dib->compression,
        dib->image_size,
        dib->horizontal_resolution,
        dib->vertical_resolution,
        dib->palette_colors,
        dib->important_colors
    );
}

void
bmp_new_file(
    uint32_t            width,
    uint32_t            height,
    struct bmp_header **bmp_ptr,
    struct dib_header **dib_ptr,
    uint8_t           **image_data_ptr)
{
    log_info("Allocating headers/image data for %ux%u image", width, height);

    struct bmp_header *bmp = (struct bmp_header *)malloc(sizeof(struct bmp_header));
    struct dib_header *dib = (struct dib_header *)malloc(sizeof(struct dib_header));

    memset(bmp, 0, sizeof(struct bmp_header));
    memset(dib, 0, sizeof(struct dib_header));


    // Length: one byte per pixel per channel (no padding -- see notes in bmp.h) plus headers.
    uint32_t image_length = width * height * 4;
    uint8_t *image_data = (uint8_t *)malloc(image_length * sizeof(uint8_t));
    uint32_t header_sizes = sizeof(*bmp) + sizeof(*dib);

    bmp->signature = BMP_SIGNATURE;
    bmp->file_size = image_length + sizeof(*bmp) + sizeof(*dib);
    bmp->_reserved_1 = 0x494a; // JI
    bmp->_reserved_2 = 0x5946; // FY
    bmp->data_offset = header_sizes;

    dib->size = sizeof(*dib);
    dib->width = width;
    dib->height = height;
    dib->planes = 1;       // Must be 1
    dib->color_depth = 32; // 8 bits per channel, 3 channels per pixel + 1 to guarantee padding
    dib->compression = 0;  // No compression
    dib->image_size = image_length;

    *bmp_ptr = bmp;
    *dib_ptr = dib;
    *image_data_ptr = image_data;
}

int
bmp_write_file(const char *fname,
    struct bmp_header     *bmp,
    struct dib_header     *dib,
    uint8_t               *image_data)
{
    log_info("Writing bitmap to '%s'", fname);

    ssize_t  bytes_written;
    uint32_t image_size = dib->image_size;

    int fd = open(fname, O_WRONLY | O_CREAT | O_TRUNC, 0644);

    if (fd < 0) {
        log_perror("open(BMP file)");
        return 1;
    }

    bytes_written = write(fd, bmp, sizeof(*bmp));
    if (bytes_written != sizeof(*bmp)) {
        log_perror("write(BMP header)");
        return 1;
    }

    bytes_written = write(fd, dib, sizeof(*dib));
    if (bytes_written != sizeof(*dib)) {
        log_perror("write(DIB header)");
        return 1;
    }

    bytes_written = write(fd, image_data, image_size);
    if (bytes_written != image_size) {
        log_perror("write(image data)");
        return 1;
    }

    if (close(fd) < 0) {
        log_perror("close(BMP file)");
        return 1;
    }

    return 0;
}

int
bmp_read_file(int fd, struct bmp_file *file, int verbose)
{
    log_info("Reading BMP file");

    file->fd = fd;

    ssize_t bytes_read;

    bytes_read = read(fd, &file->bmp, sizeof(struct bmp_header));
    if (bytes_read != (ssize_t)sizeof(struct bmp_header)) {
        log_perror("read(bmp_header)");
        return 1;
    }

    if (file->bmp.signature != BMP_SIGNATURE) {
        log_error(
            "Bad signature: read 0x%.4x != expected 0x%.4x",
            file->bmp.signature,
            BMP_SIGNATURE
        );
        return 1;
    }

    if (file->bmp.data_offset != 54) {
        log_error("Unsupported: extra BMP headers. Data offset %d != 54", file->bmp.data_offset);
        return 1;
    }

    if (verbose) {
        bmp_print_bmp_header(&file->bmp);
    }

    bytes_read = read(fd, &file->dib, sizeof(struct dib_header));
    if (bytes_read != (ssize_t)sizeof(struct dib_header)) {
        log_perror("read(dib_header)");
        return 1;
    }

    if (verbose) {
        bmp_print_dib_header(&file->dib);
    }

    if (file->dib.color_depth != 32) {
        log_error("DIB: Color depth must equal 32");
        return 1;
    }

    if (file->dib.compression != 0) {
        log_error("DIB: Compression must equal 0");
        return 1;
    }

    if (file->dib.important_colors != 0) {
        log_warning("DIB: Important colors = %u", file->dib.important_colors);
    }

    if (file->dib.palette_colors != 0) {
        log_warning("DIB: Palette colors = %u", file->dib.palette_colors);
    }

    size_t image_size = file->dib.image_size;

    file->data = (uint8_t *)malloc(image_size * sizeof(uint8_t));
    if (!file->data) {
        log_perror("malloc(BMP file data)");
        return 1;
    }

    bytes_read = read(fd, file->data, image_size);
    if (bytes_read != (ssize_t)image_size) {
        log_perror("read(BMP image data)");
    }

    log_debug("Read %ld bytes of BMP scan data", bytes_read);

    return 0;
}
