// encode.c: Encode raw image data in the JPEG format.
//
// Copyright (c) 2021 Trey Boehm & Jordan Pamatmat
//
// This file is part of the JFIF encoder/decoder for EE 382N-4 at UT Austin
// with Dr. Mark McDermott.

#ifndef __ENCODE_H__
#define __ENCODE_H__

#include <stdint.h> // uint8_t, etc.

#include "huffman.h"
#include "jfif.h"

/**
 * Encode a JFIF file.
 *
 * The file's headers should already be set up (see jfif_new_file()).
 *
 * @return 0 on success, otherwise 1.
 */
int
encode_file(
    struct jfif_file *file,      ///< File to encode
    uint8_t          *image_data ///< Data from the BMP
);

/**
 * Encode a single MCU from a file.
 */
void
encode_mcu(
    int               mcu[MCU_DIM], ///< MCU to encode
    int               prev_dc[3],   ///< Previous DC offsets
    struct jfif_file *file          ///< In-memory JFIF (for raw data and quantization tables)
);

/**
 * Get the prefix for a given value, number of leading zeros, and Huffman table.
 *
 * @return An 8-bit prefix.
 */
uint16_t
encode_get_prefix(
    int                        n_bits,        ///< Number of bits to represent the value
    int                        zeros,         ///< Number of leading zeros
    int                       *prefix_length, ///< NUmber of bits to represent the prefix
    const struct huffman_code *prefixes       ///< Table of Huffman prefixes
);

/**
 * Encode a value and leading zero count as a prefix and bit representation.
 *
 * The number of bits that matter in the return value are returned by pointer via `bit_count`.
 *
 * @return The code and value (at most 32 bits).
 */
uint32_t
encode_prefix_and_value(
    int                        value,    ///< Value to encode
    int                        zeros,    ///< Number of leading zeros
    const struct huffman_code *prefixes, ///< Table of Huffman prefixes
    int                       *bit_count ///< Number of bits to represent the code and value
);

/**
 * Convert a signed value to the bit representation JPEG uses.
 *
 * @return At most 16 bits representing a given value.
 */
uint16_t
encode_get_value_bits(
    int value, ///< Value to encode
    int n_bits ///< Number of bits to represent the value
);

#endif // __ENCODE_H__
