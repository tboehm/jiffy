// profile.h: Profile the application and accelerator.
//
// Copyright (c) 2021 Trey Boehm & Jordan Pamatmat
//
// This file is part of the JFIF encoder/decoder for EE 382N-4 at UT Austin
// with Dr. Mark McDermott.

#ifndef __PROFILE_H__
#define __PROFILE_H__

#include "logging.h"

#if LOGGING_LEVEL >= LEVEL_TIME
/**
 * Set the initial timestamp value.
 */
void
profile_start_time(
    void
);
#else
#define profile_start_time()
#endif

#if LOGGING_LEVEL >= LEVEL_TIME
/**
 * Print and return the time elapsed since the last call to profile_timestamp().
 *
 * If the `event` parameter is NULL, do not print anything.
 *
 * @return The time elapsed in microseconds.
 */
long
profile_timestamp(
    const char *event ///< Name of event that was timed.
);
#else
#define profile_timestamp(event) 0
#endif

#endif // __PROFILE_H__
