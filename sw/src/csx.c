// csx.c: Colorspace transformation.
//
// Copyright (c) 2021 Trey Boehm & Jordan Pamatmat
//
// This file is part of the JFIF encoder/decoder for EE 382N-4 at UT Austin
// with Dr. Mark McDermott.

#include "csx.h"

#include <math.h> // lroundf()

#include "jpeg.h"

// Calculations from: https://en.wikipedia.org/wiki/YCbCr#JPEG_conversion

#define CLAMP_256(x) (x < 0 ? x : x > 255 ? 255 : x)

void
csx_rgb_to_ycbcr(const int block[MCU_DIM], int transformed[MCU_DIM])
{
    for (int i = 0; i < MCU_W; i++) {
        for (int j = 0; j < MCU_W; j++) {
            int r = block[i][j][R_IDX];
            int g = block[i][j][G_IDX];
            int b = block[i][j][B_IDX];
            // Y, Cb, and Cr are just linear combinations of R, G, and B.
            float y = 0.299f * r + 0.587f * g + 0.114f * b;
            float cb = 128 - 0.168736f * r - 0.331264f * g + 0.5f * b;
            float cr = 128 + 0.5f * r - 0.418688f * g - 0.081312f * b;
            transformed[i][j][Y_IDX] = CLAMP_256((int)lroundf(y));
            transformed[i][j][CB_IDX] = CLAMP_256((int)lroundf(cb));
            transformed[i][j][CR_IDX] = CLAMP_256((int)lroundf(cr));
        }
    }
}

void
csx_ycbcr_to_rgb(const int block[MCU_DIM], int transformed[MCU_DIM])
{
    for (int i = 0; i < MCU_W; i++) {
        for (int j = 0; j < MCU_W; j++) {
            int y = block[i][j][Y_IDX];
            int cb = block[i][j][CB_IDX];
            int cr = block[i][j][CR_IDX];
            // R, G, and B are just linear combinations of Y, Cb, and Cr.
            float r = y + 1.402f * (cr - 128);
            float g = y - 0.344136f * (cb - 128) - 0.714136f * (cr - 128);
            float b = y + 1.772f * (cb - 128);
            transformed[i][j][R_IDX] = CLAMP_256((int)lroundf(r));
            transformed[i][j][G_IDX] = CLAMP_256((int)lroundf(g));
            transformed[i][j][B_IDX] = CLAMP_256((int)lroundf(b));
        }
    }
}
