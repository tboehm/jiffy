// mcu.h: Minimum coded unit utilities.
//
// Copyright (c) 2021 Trey Boehm & Jordan Pamatmat
//
// This file is part of the JFIF encoder/decoder for EE 382N-4 at UT Austin
// with Dr. Mark McDermott.

#ifndef __MCU_H__
#define __MCU_H__

#include "jpeg.h"

/**
 * Print an MCU.
 */
void
mcu_print(
    int mcu[MCU_DIM], ///< MCU to print
    int n_components  ///< Number of components to print
);

#endif // __MCU_H__
