// dqt.c: Read and parse quantization tables.
//
// Copyright (c) 2021 Trey Boehm & Jordan Pamatmat
//
// This file is part of the JFIF encoder/decoder for EE 382N-4 at UT Austin
// with Dr. Mark McDermott.

#include "dqt.h"

#include <inttypes.h> // PRIx64, PRIu64
#include <stdint.h>   // integer types
#include <stdio.h>    // printf()
#include <string.h>   // memcpy()

#include "jfif.h"
#include "logging.h"
#include "parse.h"

void
dqt_print_table(struct define_quantization_table *dqt)
{
    printf(
        "Define quantization table:\n"
        "  Marker:          0x%.4x\n"
        "  Length:          %u\n"
        "  Precision:       %u\n"
        "  Identifier:      %u\n"
        "  Elements:\n",
        dqt->marker,
        dqt->length,
        dqt->precision,
        dqt->identifier
    );

    for (int row = 0; row < 8; row++) {
        printf("   ");
        for (int col = 0; col < 8; col++) {
            printf(" %.2x", dqt->elements[row][col]);
        }
        putchar('\n');
    }
}

int
dqt_read_table(uint8_t **file_data, struct define_quantization_table *dqt, struct jfif_file *file)
{
    log_info("Parsing DQT");

    dqt->marker = NEXT_2_BYTES(file_data);

    if (dqt->marker != DQT_MARKER) {
        log_error("Marker: read 0x%.4x != expected 0x%.4x", dqt->marker, DQT_MARKER);
        return 1;
    }

    dqt->length = NEXT_2_BYTES(file_data);

    bool concat = false;
    if (dqt->length == 132) {
        log_debug("Concatenated quantization tables");
        concat = true;
    } else if (dqt->length == 67) {
        log_debug("Separate quantization tables");
        concat = false;
    } else {
        log_error("Invalid DQT length: %d", dqt->length);
        return 1;
    }

    uint8_t precision_and_id;

    precision_and_id = NEXT_BYTE(file_data);

    dqt->precision = (precision_and_id >> 4) & 0xf;
    dqt->identifier = (precision_and_id) & 0xf;

    if (dqt->precision != 0) {
        log_error("Unsupported precision: %d", dqt->precision);
        return 1;
    }

    memcpy(dqt->elements, *file_data, DQT_ELEMENT_COUNT);
    *file_data += DQT_ELEMENT_COUNT;

    if (!concat) {
        return 0;
    }

    // There's another table to read after this one.
    dqt = &file->dqt[1];

    precision_and_id = NEXT_BYTE(file_data);

    dqt->precision = (precision_and_id >> 4) & 0xf;
    dqt->identifier = (precision_and_id) & 0xf;

    if (dqt->precision != 0) {
        log_error("Unsupported precision: %d", dqt->precision);
        return 1;
    }

    memcpy(&dqt->elements, *file_data, DQT_ELEMENT_COUNT);
    *file_data += DQT_ELEMENT_COUNT;

    return 0;
}
