// bmp.h: Read and write bitmap files.
//
// Copyright (c) 2021 Trey Boehm & Jordan Pamatmat
//
// This file is part of the JFIF encoder/decoder for EE 382N-4 at UT Austin
// with Dr. Mark McDermott.

#ifndef __BMP_H__
#define __BMP_H__

#include <assert.h> // static_assert()
#include <stdint.h>

// Notes:
//
// 1) The color table is only required if the color depth (bits per pixel) is less than or equal to
//    8. We will only work with images that use 24 bits per pixel (8 each for RGB).
//
// 2) The image data section requires that each row is padded to the next multiple of 4
//    bytes. Currently, the JPEG part of jiffy doesn't handle images with a dimension that is not a
//    multiple of 8, so we should only encounter images that do not require row padding.

/// Bitmap signature (Big Endian "BM")
#define BMP_SIGNATURE 0x4d42

/**
 * Bitmap file header.
 *
 * Since we'll be writing this to a file, it /must/ be packed so that it is 14 bytes.
 */
struct __attribute__ ((__packed__)) bmp_header {
    /// ASCII "BM".
    uint16_t signature;
    /// File size in bytes.
    uint32_t file_size;
    /// 2 reserved fields that depend on the application that created the image. Can be 0.
    uint16_t _reserved_1;
    uint16_t _reserved_2;
    /// Offset from the beginning of the file to the bitmap data.
    uint32_t data_offset;
};

/**
 * Bitmap information header (DIB header).
 */
struct dib_header {
    /// Size of the info header (40).
    uint32_t size;
    /// Horizontal width of the bitmap (in pixels).
    uint32_t width;
    /// Vertical hegiht of the bitmap (in pixels).
    uint32_t height;
    /// Number of planes (1).
    uint16_t planes;
    /// Bits per pixel (typically 1, 4, 8, 16, 24, or 32).
    uint16_t color_depth;
    /// Compression method in use (typically 0, for "none").
    uint32_t compression;
    /// Size of the raw bitmap data.
    uint32_t image_size;
    /// Horizontal resolution of the image (pixels per meter).
    uint32_t horizontal_resolution;
    /// Vertical resolution of the image (pixels per meter).
    uint32_t vertical_resolution;
    /// Number of colors in the color palette (0 or 2^n).
    uint32_t palette_colors;
    /// Number of important colors used (0 when every color is important. Typically ignored).
    uint32_t important_colors;
};

/**
 * A bitmap file's header and data.
 */
struct bmp_file {
    /// BMP file header.
    struct bmp_header bmp;
    /// Information header.
    struct dib_header dib;
    /// Raw image data.
    uint8_t *data;
    /// File descriptor, if applicable.
    int fd;
};

static_assert(sizeof(struct bmp_header) == 14, "Incorrect BMP header size");
static_assert(sizeof(struct dib_header) == 40, "Incorrect DIB header size");

/**
 * Print a BMP header.
 */
void
bmp_print_bmp_header(
    struct bmp_header *bmp ///< Header to print.
);

/**
 * Print a DIB header.
 */
void
bmp_print_dib_header(
    struct dib_header *dib ///< Header to print.
);

/**
 * Create a new bitmap file.
 *
 * Allocate and set up headers for an image of a given size. This will also allocate space for the
 * image data so the decode() routine can write values as it generates them.
 */
void
bmp_new_file(
    uint32_t            width,         ///< Image width
    uint32_t            height,        ///< Image height
    struct bmp_header **bmp_ptr,       ///< Return-by-pointer BMP header
    struct dib_header **dib_ptr,       ///< Return-by-pointer DIB header
    uint8_t           **image_data_ptr ///< Return-by-pointer image data
);

/**
 * Write a bitmap file.
 *
 * @return 0 on success, otherwise 1.
 */
int
bmp_write_file(
    const char        *fname,     ///< File name
    struct bmp_header *bmp,       ///< BMP header
    struct dib_header *dib,       ///< DIB header
    uint8_t           *image_data ///< Raw image data
);

/**
 * Read a bitmap file.
 *
 * @return 0 on success, otherwise 1.
 */
int
bmp_read_file(
    int              fd,     ///< File descriptor.
    struct bmp_file *file,   ///< BMP file struct to read into.
    int              verbose ///< Print headers after reading them.
);

#endif // __BMP_H__
