// scan.c: Scan data from the image.
//
// Copyright (c) 2021 Trey Boehm & Jordan Pamatmat
//
// This file is part of the JFIF encoder/decoder for EE 382N-4 at UT Austin
// with Dr. Mark McDermott.

#include "scan.h"

#include <stdbool.h> // true/false
#include <stdio.h>   // printf()

#include "jfif.h"
#include "logging.h"
#include "parse.h"

void
scan_print_header(struct scan_header *shdr)
{
    printf(
        "Scan header\n"
        "  Length:     %u\n"
        "  Components: %u\n",
        shdr->length,
        shdr->n_components
    );

    for (int component = 0; component < shdr->n_components; component++) {
        struct scan_component *comp = &shdr->components[component];
        printf(
            "  Component %d:\n"
            "    Scan component selector: %u\n"
            "    DC entropy coding table: %u\n"
            "    AC entropy coding table: %u\n",
            component,
            comp->selector,
            comp->dc_table,
            comp->ac_table
        );
    }

    printf(
        "  Start of spectral selection:  %u\n"
        "  End of spectral selection:    %u\n"
        "  Successive approx bit (high): %u\n"
        "  Successive approx bit (low):  %u\n",
        shdr->spectral_start,
        shdr->spectral_end,
        shdr->approx_hi,
        shdr->approx_lo
    );
}

int
scan_read_header(uint8_t **file_data, struct scan_header *shdr)
{
    log_info("Parsing scan");

    shdr->marker = NEXT_2_BYTES(file_data);

    if (shdr->marker != SOS_MARKER) {
        log_error("Marker: read 0x%.4x != expected 0x%.4x", shdr->marker, SOS_MARKER);
        return 1;
    }

    shdr->length = NEXT_2_BYTES(file_data);

    shdr->n_components = NEXT_BYTE(file_data);

    if (shdr->n_components > SCAN_COMPONENTS_MAX) {
        log_error(
            "Not supported: %u is more than the allowable %d scan components",
            shdr->n_components,
            SCAN_COMPONENTS_MAX
        );
        return 1;
    }

    for (int component = 0; component < shdr->n_components; component++) {
        struct scan_component *comp = &shdr->components[component];

        comp->selector = NEXT_BYTE(file_data);

        uint8_t selectors;

        selectors = NEXT_BYTE(file_data);

        comp->dc_table = selectors & 0xf;
        comp->ac_table = (selectors >> 4) & 0xf;
    }

    // We don't actually use the next three bytes in this implementation, but we'll read them in anyway.
    shdr->spectral_start = NEXT_BYTE(file_data);

    shdr->spectral_end = NEXT_BYTE(file_data);

    uint8_t approx;

    approx = NEXT_BYTE(file_data);

    shdr->approx_hi = approx & 0xf;
    shdr->approx_lo = (approx >> 4) & 0xf;

    // 6 bytes for length (2), n_components (1), spectral start (1), spectral end (1), successive
    // approx bit positions (2). Each component has 2 bytes: scan component selector (1) and the two
    // entropy coding table selectors (each 4 bits).
    unsigned bytes_read = 6 + shdr->n_components * 2;
    if (shdr->length != bytes_read) {
        log_error("Scan header length %u != bytes read %u", shdr->length, bytes_read);
        return 1;
    }

    return 0;
}

int
scan_create_dht_mapping(struct jfif_file *file)
{
    struct scan_header *shdr = &file->shdr;

    // Create the component-to-DHT mapping
    for (int comp = 0; comp < shdr->n_components; comp++) {
        for (int ac = 0; ac < 2; ac++) {
            int comp_idx = comp * 2 + ac;
            int table_number = ac ? shdr->components[comp].ac_table
                        : shdr->components[comp].dc_table;
            int      class_id = (ac << 4) | table_number;
            unsigned table_idx;
            for (table_idx = 0;
                 table_idx < file->dht_count && (file->dht[table_idx].class_id != class_id);
                 table_idx++) {
                ;
            }

            if (table_idx == file->dht_count) {
                log_error("Unable to find DHT with class/ID 0x%x", class_id);
                return 1;
            }

            file->dht_for_component[comp_idx] = table_idx;
            log_debug("Comp %d, DC = %d (idx %d) -> DHT %d", comp, !ac, comp_idx, table_idx);
        }
    }

    return 0;
}

void
scan_print_dri(struct define_restart_interval *dri)
{
    printf(
        "Define restart interval\n"
        "  Length:           %u\n"
        "  Restart interval: %u\n",
        dri->length,
        dri->restart_interval
    );
}

int
scan_read_dri(uint8_t **file_data, struct define_restart_interval *dri)
{
    log_info("Parsing define-restart-interval");

    dri->marker = NEXT_2_BYTES(file_data);

    if (dri->marker != DRI_MARKER) {
        log_error("Marker: read 0x%.4x != expected 0x%.4x", dri->marker, DRI_MARKER);
        return 1;
    }

    dri->length = NEXT_2_BYTES(file_data);

    if (dri->length != 4) {
        log_error("Length: read %u != expected %u", dri->length, 4);
    }

    dri->restart_interval = NEXT_2_BYTES(file_data);

    return 0;
}
