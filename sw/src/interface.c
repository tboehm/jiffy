// interface.c: Interface between software and hardware.
//
// Copyright (c) 2021 Trey Boehm & Jordan Pamatmat
//
// This file is part of the JFIF encoder/decoder for EE 382N-4 at UT Austin
// with Dr. Mark McDermott.

#include "interface.h"

#include <assert.h>   // assert()
#include <errno.h>    // int errno, EINTR
#include <fcntl.h>    // fcntl() and flags
#include <math.h>     // sqrt()
#include <string.h>   // memset(), memcpy()
#include <sys/mman.h> // mmap()
#include <sys/time.h> // gettimeofday(), struct timeval
#include <unistd.h>   // General POSIX API

#include "logging.h"
#include "mcu.h"

// mmap'd devices
static volatile uint32_t *CdmaBase;
static volatile uint32_t *JiffyBase;
static volatile uint32_t *OcmBase;
static volatile uint32_t *TimerBase;

// Interrupt counting
static volatile int          JiffySignalCount = 0;
static volatile sig_atomic_t JiffySignalProcessed = 0;
static volatile int          CdmaSignalCount = 0;
static volatile sig_atomic_t CdmaSignalProcessed = 0;

// Interrupt timing
static struct timeval StartTimestamp;
static struct timeval SignalTimestamp;
static volatile int   TimerValue;

// Double-buffer status.
static int WriteBuffer;
static int ReadBuffer;

#define TIMER_SAMPLES_MAX 32768

static int TimerSamples[TIMER_SAMPLES_MAX];
static int TimerSampleCount = 0;

/**
 * Start the capture timer.
 */
static inline void
timer_start(void)
{
    uint32_t timer_reg = TimerBase[TIMER_INT_R];

    timer_reg |= (1 << TIMER_INT_TIMERENABLE_IDX);
    TimerBase[TIMER_INT_R] = timer_reg;
}

/**
 * Reset the capture timer.
 */
static inline void
timer_reset(void)
{
    uint32_t timer_reg = TimerBase[TIMER_INT_R];

    timer_reg &= ~((1 << TIMER_INT_OUT_IDX) | (1 << TIMER_INT_TIMERENABLE_IDX));
    TimerBase[TIMER_INT_R] = timer_reg;
}

/**
 * Map addresses for hardware devices.
 *
 * @return 0 on success, otherwise 1.
 */
static int
map_addresses()
{
    log_info("Mapping addresses");

    // Open /dev/mem which represents the whole physical memory
    int dh = open("/dev/mem", O_RDWR | O_SYNC);

    if (dh < 0) {
        log_perror("open(/dev/mem)");
        return 1;
    }

    CdmaBase = mmap(NULL, CDMA_SIZE, MMAP_PERMISSIONS, MMAP_FLAGS, dh, CDMA);
    if (!CdmaBase) {
        log_perror("mmap(CDMA)");
        return 1;
    }

    OcmBase = mmap(NULL, OCM_SIZE, MMAP_PERMISSIONS, MMAP_FLAGS, dh, OCM);
    if (!OcmBase) {
        log_perror("mmap(OCM)");
        return 1;
    }

    JiffyBase = mmap(NULL, JIFFY_SIZE, MMAP_PERMISSIONS, MMAP_FLAGS, dh, JIFFY_BASE);
    if (!JiffyBase) {
        log_perror("mmap(Jiffy)");
        return 1;
    }

    TimerBase = mmap(NULL, TIMER_SIZE, MMAP_PERMISSIONS, MMAP_FLAGS, dh, TIMER_BASE);
    if (!TimerBase) {
        log_perror("mmap(Timer)");
        return 1;
    }

    if (close(dh)) {
        log_perror("close(/dev/mem)");
        return 1;
    }

    return 0;
}

static void
jiffy_signal_handler(int signo)
{
    // Confirm correct signal #
    assert(signo == JIFFY_SIGNAL);

    // Read the cycle counter (slv_reg3).
    // FIXME: Uncomment once capture timer is working again.
    TimerValue = TimerBase[TIMER_VALUE_R];

    // Timestamp for latency measurements.
    gettimeofday(&SignalTimestamp, NULL);

    JiffySignalCount++;
    JiffySignalProcessed = 1;

    // Acknowledge interrupt.
    JiffyBase[JIFFY_INTERRUPT_R] = JIFFY_INT_ACK_M;

    // Disable interrupts and the capture timer.
    // FIXME: Uncomment once capture timer is working again.
    timer_reset();

    log_debug("Cycles elapsed: %u", TimerValue);
    if (TimerValue == 0) {
        log_warning("JiffySignalCount %d: timer value 0", JiffySignalCount);
    }
    TimerSamples[TimerSampleCount++] = TimerValue;
}

static void
cdma_signal_handler(int signo)
{
    // Confirm correct signal #
    assert(signo == CDMA_SIGNAL);

    // Timestamp for latency measurements.
    gettimeofday(&SignalTimestamp, NULL);

    CdmaSignalCount++;
    CdmaSignalProcessed = 1;

    log_debug("CDMA done");
}

// Signal handlers
const struct kernel_handler DevHandlers[] = {
    { .dev_path = JiffyDevPath,
      .signo = JIFFY_SIGNAL,
      .handler = jiffy_signal_handler },
    { .dev_path = CdmaDevPath,
      .signo = CDMA_SIGNAL,
      .handler = cdma_signal_handler },
};

static int
setup_signals(void)
{
    log_info("Setting up signals");

    // Set this process to receive signals from the kernel modules.
    pid_t pid = getpid();

    if (pid < 0) {
        log_perror("getpid");
        return 1;
    }

    // We need interrupts for both CDMA and Jiffy.
    for (size_t i = 0; i < sizeof(DevHandlers) / sizeof(DevHandlers[0]); i++) {
        const struct kernel_handler *kh = &DevHandlers[i];

        if (kh->signo == CDMA_SIGNAL) {
            log_info("Skipping CDMA signal");
            continue;
        }

        log_info("Setting up signal handler for device %s", kh->dev_path);

        struct sigaction dev_sigaction;

        memset(&dev_sigaction, 0, sizeof(dev_sigaction));
        dev_sigaction.sa_handler = kh->handler;

        // Block signals while our signal handler is executing.
        (void)sigfillset(&dev_sigaction.sa_mask);

        // Register the handler.
        if (sigaction(kh->signo, &dev_sigaction, NULL) < 0) {
            log_perror("sigaction");
            return 1;
        }

        // Open the Keccak/CDMA device file.
        int dev_fd = open(kh->dev_path, O_RDWR);

        if (dev_fd < 0) {
            log_perror("open(dev_path)");
            return 1;
        }

        if (fcntl(dev_fd, F_SETOWN, pid)) {
            log_perror("fcntl(F_SETOWN)");
            return 1;
        }

        // Enable reception of signals for the kernel module.
        int fd_flags = fcntl(dev_fd, F_GETFL);

        if (fd_flags < 0) {
            log_perror("fcntl(GETFL)");
            return 1;
        }

        if (fcntl(dev_fd, F_SETFL, fd_flags | O_ASYNC) < 0) {
            log_perror("fcntl(SETFL)");
            return 1;
        }

        if (fcntl(dev_fd, F_SETSIG, kh->signo)) {
            log_perror("fcntl(SETSIG)");
            return 1;
        }
    }

    return 0;
}

static uint32_t
dma_set(int offset, uint32_t value)
{
    CdmaBase[offset >> 2] = value;
    return 0;
}

static uint32_t
dma_get(int offset)
{
    return CdmaBase[offset >> 2];
}

static int
cdma_sync(void)
{
    uint32_t status = dma_get(CDMASR);

    if ((status & 0x40) != 0) {
        uint32_t desc = dma_get(CURDESC_PNTR);
        log_error("Error address: 0x%x", desc);
        return 1;
    }

    while (!(status & 1 << 1)) {
        status = dma_get(CDMASR);
    }

    return 0;
}

int
interface_open(void)
{
    if (map_addresses()) {
        return 1;
    }

    if (setup_signals()) {
        return 1;
    }

    return 0;
}

int
interface_close(void)
{
    if (munmap((void *)JiffyBase, JIFFY_SIZE)) {
        log_perror("munmap(Jiffy)");
        return 1;
    }

    if (munmap((void *)CdmaBase, CDMA_SIZE)) {
        log_perror("munmap(CDMA)");
        return 1;
    }

    if (munmap((void *)OcmBase, OCM_SIZE)) {
        log_perror("munmap(OCM)");
        return 1;
    }

    if (munmap((void *)TimerBase, TIMER_SIZE)) {
        log_perror("munmap(Timer)");
        return 1;
    }

    return 0;
}

static int16_t
get_qtz_encode_coef(int coef)
{
    float   scaled = 1.f / coef;
    int     int_part = (int)scaled;
    int     frac_part = (int)((scaled - int_part) * 256);
    int16_t fixed = ((int_part & 0xff) << 8) | (frac_part & 0xff);

    return fixed;
}

void
interface_initialize_accelerator(
    int           mcu_count,
    int           mode,
    const uint8_t qtz_tab_0[MCU_W][MCU_W],
    const uint8_t qtz_tab_1[MCU_W][MCU_W],
    const uint8_t qtz_tab_2[MCU_W][MCU_W]
)
{
    volatile uint32_t jiffy_csr;

    // 1. Reset the accelerator to clear out any old state.
    interface_reset_accelerator();

    // 2. Come back out of reset. TODO: How long do we need to wait?
    jiffy_csr = JiffyBase[JIFFY_CONTROL_STATUS_R];
    jiffy_csr &= ~JIFFY_CSR_RESET_M;
    JiffyBase[JIFFY_CONTROL_STATUS_R] = jiffy_csr;
    log_debug("Brought accelerator out of reset (CSR = 0x%x)", JiffyBase[JIFFY_CONTROL_STATUS_R]);

    // .. Set the operating mode (decode/encode).
    assert((mode == JIFFY_MODE_DECODE) || (mode == JIFFY_MODE_ENCODE));
    jiffy_csr = JiffyBase[JIFFY_CONTROL_STATUS_R];
    jiffy_csr &= ~JIFFY_CSR_MODE_M;
    jiffy_csr |= (mode << JIFFY_CSR_MODE_S) & JIFFY_CSR_MODE_M;
    JiffyBase[JIFFY_CONTROL_STATUS_R] = jiffy_csr;
    log_debug("Set mode (CSR = 0x%x)", JiffyBase[JIFFY_CONTROL_STATUS_R]);

    // 4. Send quantization tables.
    int16_t *qtz_base = (int16_t *)((uintptr_t)OcmBase + QTZ_OFFSET);

    // FIXME: Send ((1 / qtz_coef) << 8) for encode.
    for (int i = 0; i < MCU_W; i++) {
        for (int j = 0; j < MCU_W; j++) {
            int     idx = i * 8 + j;
            int16_t coef0, coef1, coef2;
            coef0 = qtz_tab_0[i][j];
            coef1 = qtz_tab_1[i][j];
            coef2 = qtz_tab_2[i][j];

            log_debug("coefs = %d, %d, %d", coef0, coef1, coef2);

            if (mode == JIFFY_MODE_ENCODE) {
                // Avoid doing extra division in the FPGA.
                coef0 = get_qtz_encode_coef(coef0);
                coef1 = get_qtz_encode_coef(coef1);
                coef2 = get_qtz_encode_coef(coef2);
                log_debug("scaled coefs = %d, %d, %d", coef0, coef1, coef2);
            }

            qtz_base[idx] = coef0;
            qtz_base[idx + 64] = coef1;
            qtz_base[idx + 128] = coef2;
        }
    }

    cdma_sync();
    dma_set(DA, BRAM_CDMA + QTZ_OFFSET); // Set destination address.
    dma_set(SA, OCM + QTZ_OFFSET);       // Set source address.
    dma_set(CDMACR, 0x0);                // Ensure interrupts are disabled.
    dma_set(BTT, TRANSFER_SIZE);         // Do the transfer.

    // 5. Set the number of MCUs.
    JiffyBase[JIFFY_BLOCKS_R] = mcu_count;
    log_debug("Set number of blocks (Block register = %u)", JiffyBase[JIFFY_BLOCKS_R]);

    // 6. Start polling for the magic byte in the BRAM.
    jiffy_csr = JiffyBase[JIFFY_CONTROL_STATUS_R];
    jiffy_csr |= JIFFY_CSR_START_M;
    JiffyBase[JIFFY_CONTROL_STATUS_R] = jiffy_csr;
    log_debug("Set start bit (CSR = 0x%x)", JiffyBase[JIFFY_CONTROL_STATUS_R]);
}

void
interface_reset_accelerator()
{
    volatile uint32_t jiffy_csr;

    // 1. Set the reset bit.
    JiffyBase[JIFFY_CONTROL_STATUS_R] = JIFFY_CSR_RESET_M;

    // 2. Set the interrupt-acknowledge bit.
    JiffyBase[JIFFY_INTERRUPT_R] |= JIFFY_INT_ACK_M;

    // 3. Clear the reset bit.
    JiffyBase[JIFFY_CONTROL_STATUS_R] &= ~JIFFY_CSR_RESET_M;

    // 4. Clear the interrupt-acknowledge bit.
    JiffyBase[JIFFY_INTERRUPT_R] &= ~JIFFY_INT_ACK_M;

    // Start processing from buffer A next time.
    WriteBuffer = BUFFER_A;
    ReadBuffer = BUFFER_A;

    log_debug("Reset accelerator and interface state");
}

void
interface_send_mcu(int mcu[MCU_DIM])
{
    // 1. Copy the MCU into the OCM.
    // FIXME: Switch 'int' to 'int16_t' for all MCUs. This will let us just use memcpy.
    // Alternatively, get rid of memcpy() entirely and just have decode_mcu()/encode_mcu() write
    // directly into the OCM. Probably a better solution.
    // FIXME: Change how MCU is layed out in memory.
    uint32_t write_offset = BUFFER_WRITE_OFFSET(WriteBuffer);
    int16_t *write_addr = (int16_t *)((uintptr_t)OcmBase + write_offset);

    log_info("Write offset = %u", write_offset);

    for (int c = 0; c < 3; c++) {
        for (int i = 0; i < MCU_W; i++) {
            for (int j = 0; j < MCU_W; j++) {
                *write_addr = mcu[i][j][c];
                write_addr++;
            }
        }
    }
    // Add the magic byte.
    *write_addr = 0xf0;
    write_addr++;
    *write_addr = 0x0000;

    // 2. Initiate a DMA.
    // We don't need interrupts for this DMA since the CPU will continue to do work while the
    // accelerator is using the data.
    cdma_sync();
    dma_set(DA, BRAM_CDMA + write_offset); // Set destination address.
    dma_set(SA, OCM + write_offset);       // Set source address.
    dma_set(CDMACR, 0x0);                  // Ensure interrupts are disabled.
    dma_set(BTT, TRANSFER_SIZE); // Do the transfer.

    // Write to the other buffer next time
    WriteBuffer = !WriteBuffer;
}

void
interface_receive_mcu(int mcu[MCU_DIM], int mode)
{
    sigset_t signal_mask, signal_mask_old, signal_mask_most;

    sigfillset(&signal_mask);
    sigfillset(&signal_mask_most);
    sigdelset(&signal_mask_most, JIFFY_SIGNAL);
    sigprocmask(SIG_SETMASK, &signal_mask, &signal_mask_old);

    // Wait for the interrupt signal from Jiffy.
    if (JiffySignalProcessed == 0) {
        log_info("Waiting on Jiffy signal");
        int rc = sigsuspend(&signal_mask_most);
        // Confirm we are coming out of suspend mode correcly
        assert(rc == -1 && errno == EINTR && JiffySignalProcessed);
    }

    // Restore previous interrupt mask.
    sigprocmask(SIG_SETMASK, &signal_mask_old, NULL);

    // DMA the processed MCU into the OCM.
    log_info("Reading from buffer %d", ReadBuffer);
    uint32_t read_offset = BUFFER_READ_OFFSET(ReadBuffer);
    void    *read_addr = (void *)((uintptr_t)OcmBase + OCM_READ_OFFSET + read_offset);

    log_info("Read offset = %u", read_offset);

    cdma_sync();
    dma_set(SA, BRAM_CDMA + read_offset);             // Set destination address.
    dma_set(DA, OCM + OCM_READ_OFFSET + read_offset); // Set source address.
    dma_set(CDMACR, 0x0);                             // Ensure interrupts are disabled.
    dma_set(BTT, TRANSFER_SIZE);                      // Do the transfer.
    cdma_sync();

    for (int c = 0; c < 3; c++) {
        for (int i = 0; i < MCU_W; i++) {
            for (int j = 0; j < MCU_W / 4; j++) {
                if (mode == JIFFY_MODE_ENCODE) {
                    mcu[i][4 * j + 1][c] = *(int16_t *)read_addr;
                    read_addr += 2;
                    mcu[i][4 * j + 0][c] = *(int16_t *)read_addr;
                    read_addr += 2;
                    mcu[i][4 * j + 3][c] = *(int16_t *)read_addr;
                    read_addr += 2;
                    mcu[i][4 * j + 2][c] = *(int16_t *)read_addr;
                    read_addr += 2;
                } else {
                    mcu[i][4 * j + 3][c] = *(uint8_t *)read_addr;
                    read_addr += 1;
                    mcu[i][4 * j + 2][c] = *(uint8_t *)read_addr;
                    read_addr += 1;
                    mcu[i][4 * j + 1][c] = *(uint8_t *)read_addr;
                    read_addr += 1;
                    mcu[i][4 * j + 0][c] = *(uint8_t *)read_addr;
                    read_addr += 1;
                }
            }
        }
    }

#if LOGGING_LEVEL >= LEVEL_DEBUG
    mcu_print(mcu, 3);
#endif // LOGGING_LEVEL >= LEVEL_DEBUG

    // Read from the other buffer next time.
    ReadBuffer = !ReadBuffer;
}

void
interface_print_interrupt_stats(void)
{
    long    min = INT64_MAX;
    long    max = 0;
    long    sum = 0;
    long    sum_squares = 0;

    // The first sample is no good for the cycle count.
    int n_samples = TimerSampleCount;

    for (int i = 0; i < n_samples; i++) {
        int val = TimerSamples[i];

        if (val < min) {
            min = val;
        }

        if (val > max) {
            max = val;
        }

        sum += val;
        sum_squares += val * val;
    }

    double mean = (double)sum / n_samples;
    double stddev = sqrt((sum_squares / (double)n_samples) - (mean * mean));

    fprintf(stderr, "Minimum latency:     %ld\n", min);
    fprintf(stderr, "Maximum latency:     %ld\n", max);
    fprintf(stderr, "Average latency:     %.6f\n", mean);
    fprintf(stderr, "Standard deviation:  %.6f\n", stddev);
    fprintf(stderr, "Number of samples:   %d\n", TimerSampleCount);
    fprintf(stderr, "Interrupts detected: %d\n", JiffySignalCount);
}
