// huffman.c: Huffman encoding/decoding.
//
// Copyright (c) 2021 Trey Boehm & Jordan Pamatmat
//
// This file is part of the JFIF encoder/decoder for EE 382N-4 at UT Austin
// with Dr. Mark McDermott.

#include "huffman.h"

#include <stdbool.h> // true/false
#include <stdio.h>   // printf()
#include <stdlib.h>  // malloc()
#include <string.h>  // memset()

#include "jfif.h"
#include "jpeg.h"
#include "logging.h"
#include "parse.h"
#include "scan.h"

void
huffman_print_table(struct define_huffman_table *dht)
{
    printf(
        "Define Huffman table:\n"
        "  Marker:          0x%.4x\n"
        "  Length:          %u\n",
        dht->marker,
        dht->length
    );

    printf("  Table class/id 0x%.2x (%s):\n", dht->class_id, DhtClasses[dht->class_id]);
    for (int i = 0; i < 16; i++) {
        uint8_t length = dht->lengths[i];
        if (length == 0) {
            continue;
        }
        printf("    Length %d (%u values):", i + 1, length);
        for (int j = 0; j < length; j++) {
            printf(" %.2x", dht->values[i][j]);
        }
        printf("\n");
    }
}

static inline bool
known_class_id(uint8_t class_id)
{
    return class_id == DHT_DC_Y
           || class_id == DHT_AC_Y
           || class_id == DHT_DC_C
           || class_id == DHT_AC_C;
}

static int
read_huffman_table_entries(uint8_t **file_data, struct define_huffman_table *dht, int *bytes_read)
{
    *bytes_read = 0;

    dht->class_id = NEXT_BYTE(file_data);

    if (!known_class_id(dht->class_id)) {
        log_error("Unknown class/id: %u", dht->class_id);
    } else {
        log_debug("Huffman table class/id = 0x%.2x (%s)", dht->class_id, DhtClasses[dht->class_id]);
    }

    *bytes_read = 1; // Size of class/ID fields.

    for (int i = 0; i < 16; i++) {
        dht->lengths[i] = NEXT_BYTE(file_data);
        *bytes_read += 1;
    }

    for (int i = 0; i < 16; i++) {
        for (int j = 0; j < dht->lengths[i]; j++, *bytes_read += 1) {
            dht->values[i][j] = NEXT_BYTE(file_data);
        }
    }

    return 0;
}

int
huffman_read_table(struct jfif_file *file, int *tables_read)
{
    log_info("Parsing DHT");

    *tables_read = 0;

    struct define_huffman_table *dht = &file->dht[file->dht_count];

    dht->marker = NEXT_2_BYTES(&file->data);

    if (dht->marker != DHT_MARKER) {
        log_error("Marker: read 0x%.4x != expected 0x%.4x", dht->marker, DHT_MARKER);
        return 1;
    }

    dht->length = NEXT_2_BYTES(&file->data);

    // There might be multiple concatenated tables to read.
    int bytes_remaining = dht->length - 2;
    unsigned dht_index;
    int bytes_read, rc;
    for (dht_index = file->dht_count; dht_index < 4 && bytes_remaining; dht_index++) {
        dht = &file->dht[dht_index];
        bytes_read = 0;
        rc = read_huffman_table_entries(&file->data, dht, &bytes_read);
        log_debug("Huffman table %u: read %d bytes", dht_index, bytes_read);
        if (rc) {
            log_error("read_huffman_table_entries return %d", rc);
            return 1;
        }
        bytes_remaining -= bytes_read;
        *tables_read += 1;
    }

    if (bytes_remaining != 0) {
        log_error("After reading DHT[s], bytes remaining = %d", bytes_remaining);
        for (int i = 0; i < bytes_remaining; i++) {
            uint8_t b = NEXT_BYTE(&file->data);
            printf("0x%.2x ", b);
        }
        printf("\n");
        return 1;
    }

    return 0;
}

void
huffman_generate_dht(const struct huffman_code *codes, struct define_huffman_table *dht)
{
    memset(dht, 0, sizeof(*dht));
    for (size_t i = 0; i <= 0xfa; i++) {
        const struct huffman_code *hc = &codes[i];
        uint8_t length = hc->prefix_length;
        if (length == 0) {
            continue;
        }
        uint8_t value = hc->value;
        uint8_t num_codes = dht->lengths[length - 1];
        // printf("Codes[%lu]: prefix length %d, value 0x%.2x\n", i, length, value);
        dht->values[length - 1][num_codes] = value;
        dht->lengths[length - 1]++;
    }
}

// TODO: Define const int tables for mapping [i][j] -> zigzag coefs

//   0    1    5    6   14   15   27   28
//   2    4    7   13   16   26   29   42
//   3    8   12   17   25   30   41   43
//   9   11   18   24   31   40   44   53
//  10   19   23   32   39   45   52   54
//  20   22   33   38   46   51   55   60
//  21   34   37   47   50   56   59   61
//  35   36   48   49   57   58   62   63

void
huffman_encode(const int mcu[MCU_DIM])
{
    const int channel = 0;
    int       i, j, i_step, j_step;

    // Lots of overhead just for a zigzag traversal.
    for (int diag = 0; diag < 2 * MCU_W - 1; diag++) {
        if (diag % 2 == 1) {
            i_step = 1;
            j_step = -1;
            if (diag < MCU_W) {
                // Start on the top side and go down/left.
                i = 0;
                j = diag;
            } else {
                // Start on the right side and go down/left.
                i = diag - MCU_W + 1;
                j = MCU_W - 1;
            }
        } else {
            i_step = -1;
            j_step = 1;
            if (diag < MCU_W) {
                // Start on the left side and go up/right.
                i = diag;
                j = 0;
            } else {
                // Start on the bottom side and go up/right.
                i = MCU_W - 1;
                j = diag - MCU_W + 1;
            }
        }

        for ( ; i >= 0 && i < MCU_W && j >= 0 && j < MCU_W; i += i_step, j += j_step) {
            printf("%02d ", mcu[i][j][channel]);
        }
        printf("\n");
    }
}

static void
#if LOGGING_LEVEL < LEVEL_SPEW
__attribute__((unused))
#endif // LOGGING_LEVEL < LEVEL_SPEW
print_binary_length(int n, int length)
{
    char digit = (char)(n % 2) + '0';
    if (length > 1) {
        print_binary_length(n >> 1, length - 1);
    }
    putchar(digit);
}

static int
get_next_prefix(int length_remaining, int root_prefix)
{
    if (length_remaining == 0) {
        return root_prefix;
    }

    // Continue descending to the left: add a zero to the right side
    return get_next_prefix(length_remaining - 1, root_prefix << 1);
}

static void
find_next_root(int prefix, int length, int *new_prefix, int *new_length)
{
    if ((prefix & 0x1) == 0) {
        // Use the sibling node to the right.
        prefix |= 0x1;
        *new_prefix = prefix;
        *new_length = length;
    } else {
        // We're already in a right sibling. Strip away 1's from the right side (i.e. move up and
        // left in the tree) until we hit a 0. Then, move right.
        while ((prefix & 0x1) == 1) {
            prefix >>= 1;
            length -= 1;
        }
        prefix |= 0x1;
        *new_prefix = prefix;
        *new_length = length;
    }
}

void
huffman_print_prefixes(struct define_huffman_table *dht)
{
    int root_prefix = 0;
    int root_level = 0;

    for (int i = 0; i < 16; i++) {
        int count = dht->lengths[i];
        if (count == 0) {
            // No prefixes with this length.
            continue;
        }
        int prefix_length = i + 1;
        printf("%d-bit prefix (count %d):\n", prefix_length, count);
        // printf("    // %d-bit prefixes\n", prefix_length);
        int prefix = 0;
        for (int j = 0; j < count; j++) {
            uint8_t code = dht->values[i][j];
            prefix = get_next_prefix(prefix_length - root_level, root_prefix);
            find_next_root(prefix, prefix_length, &root_prefix, &root_level);
            printf("  0x%.4x (length %d) -> 0x%.2x\n", prefix, prefix_length, code);
            // printf("    [0x%.2x] = { 0x%.2x, %d, 0x%.4x },\n", code, code, prefix_length, prefix);
        }
    }
}

static void
create_huffman_lut(struct define_huffman_table *dht, struct huffman_lut *lut)
{
    int short_prefix_length = 0;         // How many bits we need for short prefixes.
    int longest_prefix_length = 0;       // The length of the longest prefix.
    int shortest_long_prefix_length = 0; // The length of the first long prefix we encounter.
    int shortest_long_prefix_count = 0;  // How many shortest long prefixes there are.

    for (int i = 0; i < 16; i++) {
        if (dht->lengths[i] != 0) {
            log_spew("dht->lengths[%d] = %u", i, dht->lengths[i]);
            longest_prefix_length = i + 1;
            // We can't assume the shortest prefix will be length SHORT_PREFIX_LENGTH_MAX since the
            // table might skip prefixes of that length.
            if (i + 1 <= SHORT_PREFIX_LENGTH_MAX) {
                short_prefix_length = i + 1;
            } else if (shortest_long_prefix_length == 0) {
                shortest_long_prefix_length = i + 1;
                shortest_long_prefix_count = dht->lengths[i] + 1;
            }
        }
    }

    log_debug("longest_prefix_length = %d, short_prefix_length = %d, shortest_long_prefix_length = %d",
              longest_prefix_length, short_prefix_length, shortest_long_prefix_length);

    if (short_prefix_length == 0) {
        short_prefix_length = SHORT_PREFIX_LENGTH_MAX;
    }

    lut->longest_prefix_length = longest_prefix_length;
    lut->shortest_long_prefix_length = shortest_long_prefix_length;

    // How many bits wide are short prefixes? Depends on the hard-coded max and the longest one that
    // we actually have in this table.
    lut->short_prefix_length = longest_prefix_length > SHORT_PREFIX_LENGTH_MAX
                                ? short_prefix_length
                                : longest_prefix_length;

    // Shift prefixes by this many bits to isolate the short prefix.
    lut->short_prefix_shift = lut->longest_prefix_length - lut->short_prefix_length;

    // The largest short prefix (just a placeholder -- will be overwritten).
    lut->short_prefix_max = (1 << lut->short_prefix_length) - 1;

    lut->shift_amount = 16 - lut->longest_prefix_length;

    lut->short_code_count = 1 << lut->short_prefix_length;
    lut->short_codes = (uint8_t *)malloc((unsigned)lut->short_code_count * sizeof(uint8_t));
    lut->short_lengths = (uint8_t *)malloc((unsigned)lut->short_code_count * sizeof(uint8_t));

    // We'll set these up later.
    lut->long_code_count = 0;
    lut->long_codes = NULL;
    lut->long_lengths = NULL;

    log_debug("Longest prefix is %d bits -> shamt = %d", lut->longest_prefix_length, lut->shift_amount);
    log_debug(
        "Short prefix length is %d bits (0x%x) -> table has %d entries",
        lut->short_prefix_length,
        lut->short_prefix_max,
        lut->short_code_count
    );
}

static void
fill_lut_entries(int prefix, int length, uint8_t code, struct huffman_lut *lut)
{
    if (length <= lut->short_prefix_length) {
        // Use the short table.
        int shift_width = lut->short_prefix_length - length;
        int entry_count = 1 << shift_width;
        int start_index = prefix << shift_width;
        memset(&lut->short_codes[start_index], code, entry_count);
        memset(&lut->short_lengths[start_index], length, entry_count);
        log_spew(
            "SHORT TABLE: Code 0x%.2x / prefix 0x%x (length %d) gets %d entries in the LUT starting at 0x%x",
            code,
            prefix,
            length,
            entry_count,
            start_index
        );
    } else {
        // Use the long table.
        int shift_width = lut->longest_prefix_length - length;
        int entry_count = 1 << shift_width;
        // Discard the top `lut->shortest_prefix_length` bits when indexing.
        int start_index = (prefix << shift_width) & lut->trim_mask;
        // log_debug("Code 0x%.2x, prefix 0x%x, sw %d, entries %d, start 0x%x\n", code, prefix, shift_width, entry_count, start_index);
        memset(&lut->long_codes[start_index], code, entry_count);
        memset(&lut->long_lengths[start_index], length, entry_count);
        log_spew(
            "LONG TABLE: Code 0x%.2x / prefix 0x%x (length %d) gets %d entries in the LUT starting at 0x%x, sw = %d",
            code,
            prefix,
            length,
            entry_count,
            start_index,
            shift_width
        );
    }
}

void
huffman_get_code(
    struct huffman_lut *lut,
    uint16_t            raw_bits,
    uint8_t            *code,
    uint8_t            *length
)
{
    // raw_bits is just a stream of bits from the file. Shift it so that the least-significant
    // lut->longest_prefix_length bits are at the bottom.
    uint16_t prefix = raw_bits >> lut->shift_amount;

    // The short prefix will be the higher lut->short_prefix_length bits.
    uint16_t short_prefix = prefix >> (lut->longest_prefix_length - lut->short_prefix_length);

    log_spew("prefix = 0x%.4x, short_prefix = 0x%.4x, max = 0x%.4x, shamt = %d, longest length = %d",
             prefix, short_prefix, lut->short_prefix_max, lut->shift_amount, lut->long_prefix_length);

    if (lut->long_code_count == 0 && short_prefix > lut->short_prefix_max) {
        log_error("Short prefix 0x%.2x is larger than max 0x%.2x, but there are no long codes",
                  short_prefix, lut->short_prefix_max);
        exit(EXIT_FAILURE);
    }

    if (short_prefix > lut->short_prefix_max) {
        // Use the long prefix + long code table. The long prefix is just the bottom
        // lut->trim_length bits since the upper short prefix bits are all ones.
        uint16_t long_prefix = prefix & lut->trim_mask;
        log_spew("Using long prefix 0x%.2x (trim mask = 0x%x)", long_prefix, lut->trim_mask);
        *code = lut->long_codes[long_prefix];
        *length = lut->long_lengths[long_prefix];
    } else {
        *code = lut->short_codes[short_prefix];
        *length = lut->short_lengths[short_prefix];
    }
}

static inline int
count_top_ones(int n)
{
    int top_count = 0;

    while (n) {
        if ((n & 1) == 0) {
            top_count = 0;
        } else {
            top_count += 1;
        }
        n >>= 1;
    }

    return top_count;
}


static inline void
compute_trim_mask(struct huffman_lut *lut, int shortest_long_prefix)
{
    // The number of bits to keep for long prefixes. Note that this is NOT just (16 - short length)
    // or something like that. It actually depends on the structure of the prefixes we
    // have. Consider this example:
    //
    // |--------+---------------------+-------+---------------------+--------|
    // | Prefix |              Binary | Shift |             Shifted | Length |
    // |--------+---------------------+-------+---------------------+--------|
    // |   0xfa |           1111.1010 |       |                     |      8 |
    // |   0xfb |           1111.1011 |       |                     |      8 |
    // |--------+---------------------+-------+---------------------+--------|
    // |  0x1f8 |         1.1111.1000 |     7 | 1111.1100.0000.0000 |      9 |
    // |  0x1f9 |         1.1111.1001 |     7 | 1111.1100.1000.0000 |      9 |
    // |  0x1fa |         1.1111.1010 |     7 | 1111.1101.0000.0000 |      9 |
    // |  0x1fb |         1.1111.1011 |     7 | 1111.1101.1000.0000 |      9 |
    // |  0x3f8 |        11.1111.1000 |     6 | 1111.1110.0000.0000 |     10 |
    // |  0x3f9 |        11.1111.1001 |     6 | 1111.1110.0100.0000 |     10 |
    // |    ... |                 ... |       |                 ... |    ... |
    // |  0x7f8 |       111.1111.1000 |     5 | 1111.1111.0000.0000 |     11 |
    // |    ... |                 ... |       |                 ... |    ... |
    // | 0xfffe | 1111.1111.1111.1110 |     0 | 1111.1111.1111.1110 |     16 |
    // |--------+---------------------+-------+---------------------+--------|
    //
    // Since we have 4 prefixes of length 9, they always start with 6 ones and a single zero. That
    // zero will become a one for the length 10 prefixes. Thus, we can discard the top 6 bits: our
    // long prefix length is 10.
    //
    // However, if we only had 2 prefixes of length 9, they would be 0x1fc and 0x1fd (1.1111.1100
    // and 1.1111.1101). In this case, we would discard the top 7 bits and have a long prefix length
    // of 16 - 7 = 9. If we only had 1 length-9 prefix, it would be 0x1fe, and the long prefix
    // length would be 16 - 8 = 8.
    //
    // Generally, for:
    //   - Longest prefix has L bits
    //   - Shortest long prefix has S bits
    //   - K different prefixes with S bits
    //   - Number of bits we discard for long prefixes B
    //
    //     B = L - (S - log2(K))
    //
    // If we have the first of the shortest long prefixes, we can simply count how many contiguous
    // bits are 1 at the top of the prefix and subtract that from the longest prefix length. That's
    // what we'll do here.

    // unsigned prefix = (unsigned)shortest_long_prefix;

    log_debug("Prefix = 0x%x, longest prefix length = %d", shortest_long_prefix, lut->longest_prefix_length);

    // lut->long_prefix_trim_length = lut->longest_prefix_length - __builtin_popcount(prefix);
    lut->long_prefix_trim_length = lut->longest_prefix_length - count_top_ones(shortest_long_prefix);

    // Compute the mask used to trim bits
    lut->trim_mask = 0;
    // for (int i = 0; i < lut->short_prefix_length; i++) {
    for (int i = 0; i < lut->long_prefix_trim_length; i++) {
        lut->trim_mask = (lut->trim_mask << 1) | 1;
    }

    log_debug("Trim length = %d, trim mask = 0x%x", lut->long_prefix_trim_length, lut->trim_mask);

    lut->long_code_count = 1 << lut->long_prefix_trim_length;
    lut->long_codes = (uint8_t *)malloc((unsigned)lut->long_code_count * sizeof(uint8_t));
    lut->long_lengths = (uint8_t *)malloc((unsigned)lut->long_code_count * sizeof(uint8_t));
    log_debug(
        "Long prefix trim length is %d bits -> table has %d entries",
        lut->long_prefix_trim_length,
        lut->long_code_count
    );
}

void
huffman_expand_table(
    struct define_huffman_table *dht,
    struct huffman_lut          *lut
)
{
    if (!known_class_id(dht->class_id)) {
        log_error("Unknown class/ID: 0x%.2x", dht->class_id);
        exit(EXIT_FAILURE);
    }

    log_info("Expanding Huffman table 0x%.2x (%s)", dht->class_id, DhtClasses[dht->class_id]);

    // Initialize the lookup table.
    create_huffman_lut(dht, lut);

    int root_prefix = 0;
    int root_level = 0;

    for (int i = 0; i < 16; i++) {
        int count = dht->lengths[i];
        if (count == 0) {
            // No prefixes with this length.
            continue;
        }
        int prefix_length = i + 1;
#if LOGGING_LEVEL >= LEVEL_SPEW
        printf("%d-bit prefix (count %d):\n", prefix_length, count);
#endif // LOGIGNG_LEVEL >= LEVEL_SPEW
        int prefix = 0;
        for (int j = 0; j < count; j++) {
            uint8_t code = dht->values[i][j];
            prefix = get_next_prefix(prefix_length - root_level, root_prefix);
            find_next_root(prefix, prefix_length, &root_prefix, &root_level);

            if (i + 1 == lut->shortest_long_prefix_length && j == 0) {
                // This is the first long code, so now we need to figure out the trim mask.
                compute_trim_mask(lut, prefix);
            }

            fill_lut_entries(prefix, prefix_length, code, lut);
#if LOGGING_LEVEL >= LEVEL_SPEW
            printf("  ");
            print_binary_length(prefix, prefix_length);
            printf(" -> 0x%.2x\n", dht->values[i][j]);
#endif // LOGIGNG_LEVEL >= LEVEL_SPEW
        }

        // See what the largest short code actually is. It's not just 1 << lut->short_prefix_max
        // because there might be unused codes at the end of the short code table.
        if (i + 1 == lut->short_prefix_length) {
            lut->short_prefix_max = prefix;
            log_debug("Largest short prefix is 0x%x", lut->short_prefix_max);
        }
    }

#if LOGGING_LEVEL >= 6
    // Print out the prefix mappings
    for (int i = 0; i < lut->short_code_count; i++) {
        printf("short[");
        print_binary_length(i, lut->short_prefix_length);
        printf("] = 0x%.2x\n", lut->short_codes[i]);
    }

    printf("Long codes: %d\n", lut->long_code_count);

    int discarded_prefix_bits = (1 << lut->short_prefix_length) - 1;
    for (int i = 0; i < lut->long_code_count; i++) {
        print_binary_length(discarded_prefix_bits, lut->short_prefix_length);
        print_binary_length(i, lut->long_prefix_trim_length);
        printf(" -> long[");
        print_binary_length(i, lut->longest_prefix_length - lut->short_prefix_length);
        printf("] = 0x%.2x\n", lut->long_codes[i]);
    }
#endif // LOGIGNG_LEVEL >= LEVEL_DEBUG
}
