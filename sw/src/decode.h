// decode.h: Decode a JFIF file.
//
// Copyright (c) 2021 Trey Boehm & Jordan Pamatmat
//
// This file is part of the JFIF encoder/decoder for EE 382N-4 at UT Austin
// with Dr. Mark McDermott.

#ifndef __DECODE_H__
#define __DECODE_H__

#include "jfif.h"
#include "jpeg.h"

/**
 * Decode a JFIF file.
 *
 * @return 0 on success, otherwise 1.
 */
int decode_file(
    struct jfif_file *file,      ///< File to decode.
    uint8_t          *image_data ///< Raw pixel data for a bitmap.
);

/**
 * Decode on MCU from a JFIF file.
 *
 * Currently, this just prints it out and exits.
 */
void decode_mcu(
    int               mcu[MCU_DIM], ///< MCU to decode.
    struct jfif_file *file          ///< File to tell us how to decode.
);
#endif // __DECODE_H__
