// jfif.c: Read and parse JFIF headers
//
// Copyright (c) 2021 Trey Boehm & Jordan Pamatmat
//
// This file is part of the JFIF encoder/decoder for EE 382N-4 at UT Austin
// with Dr. Mark McDermott.

#include "jfif.h"

#include <fcntl.h>    // open() and flags
#include <stdint.h>   // integer types
#include <stdio.h>    // printf()
#include <stdlib.h>   // malloc()
#include <string.h>   // memset()
#include <sys/stat.h> // struct stat, fstat()
#include <unistd.h>   // read()

#include "dct.h"
#include "dqt.h"
#include "huffman.h"
#include "logging.h"
#include "parse.h"
#include "qtz.h"
#include "scan.h"

int
jfif_print_app0_header(struct jfif_app0_marker_segment *header)
{
    static const char *density_units[] = {
        "no units",
        "pixels per inch",
        "pixels per centimeter",
    };

    char identifier[5] = {
        (char)((header->identifier >> 24) & 0xff),
        (char)((header->identifier >> 16) & 0xff),
        (char)((header->identifier >> 8) & 0xff),
        (char)((header->identifier >> 0) & 0xff),
        '\0',
    };


    printf(
        "JFIF APP0 marker segment:\n"
        "  Marker:          0x%.4x\n"
        "  Length:          %u\n"
        "  Identifier:      0x%.8x ('%s')\n"
        "  Version:         %.4x\n"
        "  Density units:   %d (%s)\n"
        "  X density:       %u\n"
        "  Y density:       %u\n"
        "  X thumbnail:     %u\n"
        "  Y thumbnail:     %u\n"
        "  Thumbnail bytes: %d\n",
        header->marker,
        header->length,
        header->identifier,
        identifier,
        header->version,
        header->density_units,
        density_units[header->density_units],
        header->x_density,
        header->y_density,
        header->x_thumbnail,
        header->y_thumbnail,
        3 * header->x_thumbnail * header->y_thumbnail
    );

    return 0;
}

int
jfif_read_file(int fd, struct jfif_file *file, int verbose)
{
    int rc;

    memset(file, 0, sizeof(struct jfif_file));

    file->fd = fd;

    if (lseek(fd, 0, SEEK_SET) != 0) {
        log_perror("lseek(JFIF file)");
        return 1;
    }

    struct stat st_buf;

    if (fstat(fd, &st_buf) != 0) {
        log_perror("fstat(JFIF file)");
        return 1;
    }

    size_t file_size = (size_t)st_buf.st_size;

    log_info("File is %lu bytes", file_size);

    file->data = (uint8_t *)malloc(file_size * sizeof(uint8_t));
    if (!file->data) {
        log_perror("malloc(JFIF file data)");
        return 1;
    }

    ssize_t br = read(fd, file->data, file_size);

    if (br != st_buf.st_size) {
        log_error("read returned %ld", br);
        log_perror("read(JFIF file data)");
        return 1;
    }

    ////////////////////
    // Start-Of-Image //
    ////////////////////

    // if (read(fd, &file->soi, 2) != 2) {
    //     log_perror("read(SOI)");
    //     return 1;
    // }
    // file->soi = htobe16(file->soi);

    file->soi = NEXT_2_BYTES(&file->data);

    if (file->soi != JFIF_SOI_MARKER) {
        log_error("SOI: read 0x%.4x != expected 0x%.4x", file->soi, JFIF_SOI_MARKER);
        return 1;
    }

    uint16_t next_marker;
    int      app_number;
    int      tables_read;

    struct define_quantization_table *dqt;
    struct frame_header              *sof0 = &file->sof0;;
    struct scan_header               *shdr = &file->shdr;
    struct define_restart_interval   *dri = &file->dri;
    struct define_huffman_table      *dht;

    do {
        next_marker = NEXT_2_BYTES(&file->data);

        // The next function wants to validate the marker.
        file->data -= 2;

        switch (next_marker) {
        case JFIF_APP0_MARKER:
            rc = jfif_read_app0_header(&file->data, &file->jfif_app0);
            if (rc) {
                log_error("Failed to read JFIF APP0 marker segment");
                return 1;
            }
            if (verbose) {
                jfif_print_app0_header(&file->jfif_app0);
            }
            break;
        case DRI_MARKER:
            if (scan_read_dri(&file->data, dri)) {
                return 1;
            }
            if (verbose) {
                scan_print_dri(dri);
            }
            break;
        case SOS_MARKER:
            if (scan_read_header(&file->data, shdr)) {
                return 1;
            }
            if (verbose) {
                scan_print_header(shdr);
            }
            if (scan_create_dht_mapping(file)) {
                return 1;
            }
            break;
        case DQT_MARKER:
            if (file->dqt_count == DQT_TABLES_MAX) {
                log_error("Not supported: More than %d DQT segments\n", DQT_TABLES_MAX);
                return 1;
            }
            dqt = &file->dqt[file->dqt_count];
            if (dqt_read_table(&file->data, dqt, file)) {
                return 1;
            }
            if (verbose) {
                dqt_print_table(dqt);
            }
            if (verbose && dqt->length == 132) {
                // Concatenated tables. Print the other, too.
                dqt_print_table(&file->dqt[dqt->identifier + 1]);
            }
            file->dqt_count++;
            break;
        case SOF0_MARKER:
            if (jfif_read_sof0_header(&file->data, sof0)) {
                return 1;
            }
            if (verbose) {
                jfif_print_sof0_header(sof0);
            }
            break;
        case DHT_MARKER:
            if (file->dht_count == DHT_TABLES_MAX) {
                log_error("Not supported: More than %d DHT segments\n", DHT_TABLES_MAX);
                return 1;
            }
            dht = &file->dht[file->dht_count];
            if (huffman_read_table(file, &tables_read)) {
                return 1;
            }
            for (int i = 0; i < tables_read; i++) {
                dht = &file->dht[file->dht_count];
                if (verbose) {
                    huffman_print_table(dht);
                }
                huffman_expand_table(dht, &dht->lut);
                file->dht_count++;
            }
            break;
        default:
            if ((next_marker & (~0xf)) == JFIF_APP0_MARKER) {
                app_number = next_marker & 0x3;
                log_warning("Unuspported segment: APP%d. Skipping.", app_number);
                jfif_skip_header(&file->data);
            } else {
                log_error("Unknown marker: 0x%.4x", next_marker);
                return 1;
            }
            break;
        }
    } while (next_marker != SOS_MARKER);

    log_info(
        "Finished reading JFIF headers/tables (%u quantization tables, %u Huffman tables)",
        file->dqt_count,
        file->dht_count
    );

    return 0;
}

int
jfif_read_app0_header(uint8_t **file_data, struct jfif_app0_marker_segment *header)
{
    log_info("Reading JFIF APP0 header");

    header->marker = NEXT_2_BYTES(file_data);
    if (header->marker != JFIF_APP0_MARKER) {
        log_error("Marker: read 0x%.4x != expected 0x%.4x", header->marker, JFIF_APP0_MARKER);
        return 1;
    }

    header->length = NEXT_2_BYTES(file_data);
    header->identifier = NEXT_4_BYTES(file_data);
    if (header->identifier != JFIF_APP0_IDENTIFIER) {
        log_error(
            "Identifier: read 0x%.8x != expected 0x%.8x",
            header->identifier,
            JFIF_APP0_IDENTIFIER
        );
        return 1;
    }

    header->identifier_null = NEXT_BYTE(file_data);
    if (header->identifier_null != 0) {
        log_error("Null: read 0x%.2x != expected 0x00", header->identifier_null);
        return 1;
    }

    header->version = NEXT_2_BYTES(file_data);
    header->density_units = NEXT_BYTE(file_data);
    header->x_density = NEXT_2_BYTES(file_data);
    if (header->x_density == 0) {
        log_error("X density value must be nonzero");
        return 1;
    }

    header->y_density = NEXT_2_BYTES(file_data);
    if (header->y_density == 0) {
        log_error("Y density value must be nonzero");
        return 1;
    }

    header->x_thumbnail = NEXT_BYTE(file_data);
    header->y_thumbnail = NEXT_BYTE(file_data);

    // See if there is a thumbnail.
    size_t thumbnail_bytes = 3 * header->x_thumbnail * header->y_thumbnail;

    if (thumbnail_bytes) {
        log_error("Thumbnail bytes: not supported");
        return 1;
    } else {
        log_debug("No thumbnail bytes");
    }

    return 0;
}

void
jfif_skip_header(uint8_t **file_data)
{
    log_info("Skipping a JFIF header");

    unsigned marker;
    unsigned length = 0;

    marker = NEXT_2_BYTES(file_data);

    log_debug("Header marker: 0x%.2x", marker);

    length = NEXT_2_BYTES(file_data);

    // The length field counts itself towards the total.
    length -= 2;

    log_debug("Header length: 0x%x bytes", length);

    **file_data += length;
}

void
jfif_print_sof0_header(struct frame_header *sof0)
{
    printf(
        "Start of frame 0 (SOF0):\n"
        "  Marker:           0x%.4x\n"
        "  Length:           %u\n"
        "  Precision:        %u\n"
        "  Number of lines:  %u\n"
        "  Samples per line: %u\n"
        "  Components:       %u\n",
        sof0->marker,
        sof0->length,
        sof0->precision,
        sof0->lines,
        sof0->samples_per_line,
        sof0->component_count
    );

    // Read the frame component specifications
    for (int i = 0; i < sof0->component_count; i++) {
        struct frame_component *comp = &sof0->components[i];

        printf(
            "  Component id %u\n"
            "    Horizontal sampling factor:    %u\n"
            "    Vertical sampling factor:      %u\n"
            "    Quantization table dst select: %u\n",
            comp->identifier,
            comp->h_i,
            comp->v_i,
            comp->qt_select
        );
    }
}

int
jfif_read_sof0_header(uint8_t **file_data, struct frame_header *sof0)
{
    log_info("Parsing SOF0");

    sof0->marker = NEXT_2_BYTES(file_data);

    if (sof0->marker != SOF0_MARKER) {
        log_error("Marker: read 0x%.4x != expected 0x%.4x", sof0->marker, SOF0_MARKER);
        return 1;
    }

    sof0->length = NEXT_2_BYTES(file_data);
    sof0->precision = NEXT_BYTE(file_data);
    sof0->lines = NEXT_2_BYTES(file_data);
    sof0->samples_per_line = NEXT_2_BYTES(file_data);
    sof0->component_count = NEXT_BYTE(file_data);

    if (sof0->component_count > SOF0_COMPONENTS) {
        log_error(
            "Component count %d is greater than the maximum %d",
            sof0->component_count,
            SOF0_COMPONENTS
        );
        return 1;
    }

    // Read the frame component specifications
    for (int i = 0; i < sof0->component_count; i++) {
        struct frame_component *comp = &sof0->components[i];

        comp->identifier = NEXT_BYTE(file_data);

        uint8_t sampling_factors;

        sampling_factors = NEXT_BYTE(file_data);

        comp->h_i = sampling_factors & 0xf;
        comp->v_i = (sampling_factors >> 4) & 0xf;

        comp->qt_select = NEXT_BYTE(file_data);
    }

    return 0;
}

// TODO: Add option to select quality (i.e. quantization tables)
void
jfif_new_file(uint16_t image_width, uint16_t image_height, struct jfif_file *file, int verbose)
{
    struct jfif_app0_marker_segment  *app0 = &file->jfif_app0;
    struct frame_header              *sof0 = &file->sof0;;
    struct define_quantization_table *dqt;
    struct define_huffman_table      *dht;
    struct scan_header               *shdr = &file->shdr;

    file->soi = JFIF_SOI_MARKER;

    // Set up the application 0 header.
    app0->marker = JFIF_APP0_MARKER;
    app0->length = 16; // Length of this header, excluding the marker (always 16?).
    app0->identifier = JFIF_APP0_IDENTIFIER;
    app0->identifier_null = 0;
    app0->version = 0x0101;
    // Hard-code 144 ppi.
    app0->density_units = 1; // Pixels-per-inch.
    app0->x_density = 144;
    app0->y_density = 144;
    // No thumbnail
    app0->x_thumbnail = 0;
    app0->y_thumbnail = 0;
    if (verbose) {
        jfif_print_app0_header(app0);
    }

    // Set up Start-Of-Frame-0 (SOF0)
    sof0->marker = SOF0_MARKER;
    sof0->length = 17;   // Length of this header (always 17).
    sof0->precision = 8; // 8 bits per component per pixel.
    sof0->lines = image_height;
    sof0->samples_per_line = image_width;
    sof0->component_count = SOF0_COMPONENTS;

    // Configure the frame components (set subsampling and quantization tables).
    for (uint8_t comp = 0; comp < sof0->component_count; comp++) {
        struct frame_component *component = &sof0->components[comp];
        component->identifier = comp + 1;
        // No subsampling
        component->h_i = 1;
        component->v_i = 1;
        component->qt_select = comp != 0; // 0 for component 0 (Y), otherwise 1 (Cb/Cr).
    }

    if (verbose) {
        jfif_print_sof0_header(sof0);
    }

    // Define 2 quantization tables: one for Y, another for Cb/Cr
    for (file->dqt_count = 0; file->dqt_count < 2; file->dqt_count++) {
        dqt = &file->dqt[file->dqt_count];
        dqt->marker = DQT_MARKER;
        dqt->length = 67;
        dqt->precision = 0;
        dqt->identifier = file->dqt_count;
        memcpy(dqt->elements, QtzMat100, 64);
        // memcpy(dqt->elements, QtzMat50, 64); // FIXME: Test accelerator with non-trivial quantization.
        if (verbose) {
            dqt_print_table(dqt);
        }
    }

    // Define 4 Huffman tables: DC luma, DC chroma, AC luma, AC chroma
    for (int i = 0; i < 4; i++) {
        dht = &file->dht[i];
        // Must call this first: it memset's the struct to 0.
        huffman_generate_dht(HuffmanStandardTables[i], dht);
        dht->marker = DHT_MARKER;
        // Non-value fields (header length, class/id, and lengths) are 19 bytes total.
        dht->length = HuffmanStandardTableLengths[i] + 19;
        dht->class_id = HuffmanStandardTableClassIds[i];
        if (verbose) {
            huffman_print_table(dht);
        }
    }
    file->dht_count = 4;

    // Set up the scan header.
    shdr->marker = SOS_MARKER;
    shdr->length = 12; // Length of this header (always 12).
    shdr->n_components = sof0->component_count;
    // These next four fields are unused (in this implementation), but we set them anyway.
    shdr->spectral_start = 0;
    shdr->spectral_end = 63;
    shdr->approx_hi = 0;
    shdr->approx_lo = 0;

    for (uint8_t comp = 0; comp < shdr->n_components; comp++) {
        struct scan_component *component = &shdr->components[comp];
        component->selector = comp + 1;
        component->dc_table = comp != 0; // 0 for component 0 (Y), otherwise 1 (Cb/Cr).
        component->ac_table = component->dc_table;
    }

    if (verbose) {
        scan_print_header(shdr);
    }

    scan_create_dht_mapping(file);
}

// #define DUMMY

#ifdef DUMMY
static const uint8_t dummy_data[] = {
    0xff, 0xc4, 0x00, 0x14, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x09, 0xff, 0xc4, 0x00, 0x23, 0x10, 0x00, 0x01, 0x02, 0x04,
    0x06, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0x06, 0x01, 0x04,
    0x16, 0x02, 0x11, 0x03, 0x09, 0x15, 0x17, 0x18, 0x05, 0x13, 0x19, 0x27, 0xff, 0xc4, 0x00, 0x15,
    0x01, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x07, 0x08, 0xff, 0xc4, 0x00, 0x22, 0x11, 0x00, 0x01, 0x02, 0x06, 0x02, 0x03, 0x01, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x05, 0x06, 0x04, 0x15, 0x14, 0x03, 0x07, 0x16,
    0x13, 0x01, 0x02, 0x11, 0x12, 0x17, 0x24, 0xff, 0xda, 0x00, 0x0c, 0x03, 0x01, 0x00, 0x02, 0x11,
    0x03, 0x11, 0x00, 0x3f, 0x00, 0x09, 0x11, 0xca, 0x7a, 0xb0, 0x38, 0xb9, 0x72, 0x49, 0xb2, 0x03,
    0x4f, 0xca, 0xef, 0x32, 0x7e, 0xa1, 0xf4, 0x91, 0xe6, 0x34, 0x74, 0xb3, 0x54, 0x6f, 0xb8, 0x7b,
    0x20, 0x90, 0x36, 0x49, 0x44, 0xcd, 0xe1, 0x05, 0x77, 0x9d, 0xb7, 0x6c, 0xba, 0xd3, 0x63, 0x0a,
    0xe6, 0xd3, 0x5f, 0xda, 0x42, 0x5b, 0x23, 0x4f, 0x16, 0xa9, 0x05, 0x35, 0x86, 0x46, 0x77, 0xfa,
    0x2c, 0x9a, 0x80, 0x30, 0xcf, 0x7b, 0x96, 0x27, 0xa8, 0x3d, 0x5c, 0x58, 0x56, 0x38, 0xfa, 0x59,
    0xc6, 0x55, 0x1a, 0x05, 0x42, 0x7a, 0xe1, 0x34, 0x80, 0xf5, 0xe6, 0x28, 0x4b, 0x42, 0x58, 0xb4,
    0x81, 0x12, 0xf8, 0xd0, 0x8d, 0x94, 0x59, 0xcc, 0xa9, 0x31, 0x26, 0x92, 0xd7, 0xe1, 0x64, 0xd3,
    0x2a, 0x9c, 0x49, 0x0f, 0xaf, 0x20, 0xaf, 0x61, 0x48, 0xdf, 0x85, 0x8d, 0xf9, 0x71, 0x10, 0x54,
    0x31, 0x8a, 0xc1, 0x4b, 0x7e, 0x12, 0x23, 0xe2, 0x55, 0x19, 0x62, 0x23, 0x74, 0x1a, 0x15, 0x03,
    0x1c, 0x37, 0x9a, 0x0c, 0x75, 0x80, 0x73, 0x24, 0x9c, 0x86, 0xff, 0x00, 0xff, 0xd9,
};
#endif // DUMMY

int
jfif_write_file(const char *fname, struct jfif_file *file)
{
    log_info("Writing JFIF to '%s'", fname);

    int fd = open(fname, O_WRONLY | O_CREAT | O_TRUNC, 0644);

    if (fd < 0) {
        log_perror("open(JFIF file)");
        return 1;
    }

    // Manipulate the buffer in-memory rather than writing small amounts of data to the file.
    uint8_t *jfif_headers = (uint8_t *)calloc(4096, sizeof(uint8_t));
    if (!jfif_headers) {
        log_perror("calloc");
        return 1;
    }

    uint8_t *buf = jfif_headers;

    // Start-of-image
    WRITE_2_BYTES(&buf, JFIF_SOI_MARKER);

    // App0 header
    struct jfif_app0_marker_segment *app0 = &file->jfif_app0;
    WRITE_2_BYTES(&buf, app0->marker);
    WRITE_2_BYTES(&buf, app0->length);
    WRITE_4_BYTES(&buf, app0->identifier);
    WRITE_BYTE(&buf, app0->identifier_null);
    WRITE_2_BYTES(&buf, app0->version);
    WRITE_BYTE(&buf, app0->density_units);
    WRITE_2_BYTES(&buf, app0->x_density);
    WRITE_2_BYTES(&buf, app0->y_density);
    WRITE_BYTE(&buf, app0->x_thumbnail);
    WRITE_BYTE(&buf, app0->y_thumbnail);

    // Define quantization tables
    struct define_quantization_table *dqt;
    for (unsigned i = 0; i < file->dqt_count; i++) {
        dqt = &file->dqt[i];
        WRITE_2_BYTES(&buf, dqt->marker);
        WRITE_2_BYTES(&buf, dqt->length);
        uint8_t precision_identifier = (uint8_t)(dqt->precision << 4) | dqt->identifier;
        WRITE_BYTE(&buf, precision_identifier);
        memcpy(buf, dqt->elements, 64);
        buf += 64;
    }

    // Frame 0 definition.
    struct frame_header *sof0 = &file->sof0;
    WRITE_2_BYTES(&buf, sof0->marker);
    WRITE_2_BYTES(&buf, sof0->length);
    WRITE_BYTE(&buf, sof0->precision);
    WRITE_2_BYTES(&buf, sof0->lines);
    WRITE_2_BYTES(&buf, sof0->samples_per_line);
    WRITE_BYTE(&buf, sof0->component_count);
    for (unsigned i = 0; i < sof0->component_count; i++) {
        struct frame_component *comp = &sof0->components[i];
        WRITE_BYTE(&buf, comp->identifier);
        uint8_t sampling_factors = (uint8_t)(comp->h_i << 4) | comp->v_i;
        WRITE_BYTE(&buf, sampling_factors);
        WRITE_BYTE(&buf, comp->qt_select);
    }

#ifndef DUMMY
    // Define Huffman tables
    for (unsigned i = 0; i < file->dht_count; i++) {
        struct define_huffman_table *dht = &file->dht[i];
        // uint8_t *orig_buf = buf;
        WRITE_2_BYTES(&buf, dht->marker);
        WRITE_2_BYTES(&buf, dht->length);
        WRITE_BYTE(&buf, dht->class_id);
        memcpy(buf, dht->lengths, 16);
        buf += 16;
        log_info("DHT %u: length = %d, class/id = 0x%.2x", i, dht->length, dht->class_id);
        for (unsigned length = 0; length < 16; length++) {
            uint8_t count = dht->lengths[length];
            if (!count) {
                continue;
            }
            memcpy(buf, dht->values[length], count);
            buf += count;
        }
        // log_info("Write %ld bytes for DHT %u", (intptr_t)buf - (intptr_t)orig_buf, i);
    }

    // Scan data.
    struct scan_header *shdr = &file->shdr;
    WRITE_2_BYTES(&buf, shdr->marker);
    WRITE_2_BYTES(&buf, shdr->length);
    WRITE_BYTE(&buf, shdr->n_components);
    for (unsigned i = 0; i < shdr->n_components; i++) {
        struct scan_component *comp = &shdr->components[i];
        WRITE_BYTE(&buf, comp->selector);
        uint8_t table_select = (uint8_t)(comp->dc_table << 4) | comp->ac_table;
        WRITE_BYTE(&buf, table_select);
    }
    WRITE_BYTE(&buf, shdr->spectral_start);
    WRITE_BYTE(&buf, shdr->spectral_end);
    uint8_t approx = (uint8_t)(shdr->approx_hi << 4) | shdr->approx_lo;
    WRITE_BYTE(&buf, approx);
#endif // not-def DUMMY

    // The header is complete. Write it to the file.

    size_t jfif_headers_length = (uintptr_t)buf - (uintptr_t)jfif_headers;
    log_debug("Writing %lu bytes of JFIF headers", jfif_headers_length);

    ssize_t bytes_written = write(fd, jfif_headers, jfif_headers_length);
    if (bytes_written != (ssize_t)jfif_headers_length) {
        log_perror("write");
        return 1;
    }

#ifndef DUMMY
    log_debug("Writing %lu bytes of scan data", file->data_length);

    bytes_written = write(fd, file->data, file->data_length);
    if (bytes_written != (ssize_t)file->data_length) {
        log_perror("write");
        return 1;
    }
#endif // not-def DUMMY

#ifdef DUMMY
    size_t dummy_data_length = sizeof(dummy_data) / sizeof(dummy_data[0]);

    log_debug("Writing %lu bytes of dummy data", dummy_data_length);

    bytes_written = write(fd, dummy_data, dummy_data_length);
    if (bytes_written != (ssize_t)dummy_data_length) {
        log_perror("write");
        return 1;
    }
#endif // DUMMY

    if (close(fd)) {
        log_perror("close");
        return 1;
    }

    return 0;
}
