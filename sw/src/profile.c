#include "profile.h"

#include "logging.h"

#if LOGGING_LEVEL >= LEVEL_TIME
#include <stdio.h>    // printf()
#include <sys/time.h> // gettimeofday(), timersub(), struct timeval

static struct timeval LastTimestamp;

void
profile_start_time(void)
{
    gettimeofday(&LastTimestamp, NULL);
}

long
profile_timestamp(const char *event)
{
    struct timeval t_end, t_diff;

    // Get the elapsed time.
    gettimeofday(&t_end, NULL);
    timersub(&t_end, &LastTimestamp, &t_diff);
    long elapsed_usec = t_diff.tv_usec + (long)1e6 * t_diff.tv_sec;

    // Update the last timestamp values.
    LastTimestamp.tv_usec = t_end.tv_usec;
    LastTimestamp.tv_sec = t_end.tv_sec;

    if (event != NULL) {
        log_timestamp(event, elapsed_usec);
    }

    return elapsed_usec;
}
#endif // LOGGING_LEVEL >= LEVEL_TIEM
