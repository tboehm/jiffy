// jfif.h: Read and parse JFIF headers
//
// Copyright (c) 2021 Trey Boehm & Jordan Pamatmat
//
// This file is part of the JFIF encoder/decoder for EE 382N-4 at UT Austin
// with Dr. Mark McDermott.

#ifndef __JFIF_H__
#define __JFIF_H__

#include <stdint.h>
#include <stdio.h>

#include "dqt.h"
#include "huffman.h"
#include "scan.h"

/// Start-of-image marker bytes.
#define JFIF_SOI_MARKER 0xffd8

/// End-of-image marker bytes.
#define JFIF_EOI_MARKER 0xffd9

/// Padding in scan data to accomodate 0xff bytes.
#define PADDING 0xff00

/// Restart in scan data.
#define RESTART 0xffdd

/// Code that defines the end of a block.
#define EOB 0x00

/// Code that defines a run of 16 zeros.
#define ZRL 0xf0

/// Marker for JFIF APP0 header
#define JFIF_APP0_MARKER 0xffe0

/// Identifier bytes for JFIF APP0 header
#define JFIF_APP0_IDENTIFIER 0x4a464946

/**
 * APP0 marker segment.
 */
struct jfif_app0_marker_segment {
    /// Always 0xffe0.
    uint16_t marker;
    /// Length of segment, excluding APP0 marker.
    uint16_t length;
    /// 5 bytes, always 0x4a46494600 (ascii "JFIF").
    uint32_t identifier;
    uint8_t  identifier_null;
    /// First byte for major version, second for minor.
    uint16_t version;
    /// Units for the following pixel density fields:
    ///  - 00: No units; width:height pixel aspect ratio = y_density:x_density
    ///  - 01: Pixels per inch (2.54 cm)
    ///  - 02: Pixels per centimeter
    uint8_t density_units;
    /// Horizontal pixel density. Must be nonzero.
    uint16_t x_density;
    /// Vertical pixel density. Must be nonzero.
    uint16_t y_density;
    /// Horizontal pixel count of the following embedded RGB thumbnail. May be zero.
    uint8_t x_thumbnail;
    /// Vertical pixel count of the following embedded RGB thumbnail. May be zero.
    uint8_t y_thumbnail;
};

/// Marker for SOF0
#define SOF0_MARKER 0xffc0

/// Maximum number of components for frame (hard-code 3 for now)
#define SOF0_COMPONENTS 3

/**
 * Frame component specification for frame headers.
 */
struct frame_component {
    /// Component identifier
    uint8_t identifier;
    /// Horizontal sampling factor
    uint8_t h_i : 4;
    /// Vertical sampling factor
    uint8_t v_i : 4;
    /// Quantization table destination selector
    uint8_t qt_select;
};

/**
 * Frame header (markers SOFn).
 */
struct frame_header {
    /// Always 0xffcN where N is 0-3, 5-7, 9-B, D-F.
    uint16_t marker;
    /// Quantization table length.
    uint16_t length;
    /// Sample precision
    uint8_t precision;
    /// Number of lines
    uint16_t lines;
    /// Number of samples per lines
    uint16_t samples_per_line;
    /// Number of components in frame
    uint8_t component_count;
    /// Frame component specifications
    struct frame_component components[SOF0_COMPONENTS];
};

/**
 * Print the SOF0 header.
 */
void
jfif_print_sof0_header(
    struct frame_header *sof0 ///< Header to print
);

/**
 * Read the SOF0 header.
 *
 * @return 0 on success, otherwise 1.
 */
int
jfif_read_sof0_header(
    uint8_t            **file_data, ///< Buffer containing file data
    struct frame_header *sof0       ///< Table to read into
);

/**
 * Overall JFIF file structure.
 */
struct jfif_file {
    /// Start of image. Always 0xffd8.
    uint16_t soi;
    /// JFIF APP0 header.
    struct jfif_app0_marker_segment jfif_app0;
    /// Define quantization table[s].
    struct define_quantization_table dqt[DQT_TABLES_MAX];
    /// Number of quantization tables.
    uint32_t dqt_count;
    /// Frame header 0 parameters.
    struct frame_header sof0;
    /// Huffman tables.
    struct define_huffman_table dht[DHT_TABLES_MAX];
    /// Number of Huffman tables.
    uint32_t dht_count;
    /// Component and AC/DC to DHT index mapping (at most 8, usually 2 or 6).
    unsigned dht_for_component[8];
    /// Additional marker segments may follow (e.g. SOF, DHT, COM).
    uint32_t unused_additional_segment_count;
    /// Restart interval.
    struct define_restart_interval dri;
    /// Scan header.
    struct scan_header shdr;
    /// Start of scan. Always 0xffda.
    uint16_t sos;
    /// Compressed image data
    uint8_t *image;
    /// End of image. Always 0xffd9.
    uint16_t eoi;
    /// File descriptor, if applicable.
    int fd;
    /// Length of raw data, in bytes.
    size_t data_length;
    /// Raw file data.
    uint8_t *data;
};

/**
 * Print the JFIF APP0 segment header.
 *
 * @return 0 on success, otherwise 1.
 */
int
jfif_print_app0_header(
    struct jfif_app0_marker_segment *header
);

/**
 * Read in a JFIF header from a file.
 *
 * @return 0 on success, otherwise 1.
 */
int
jfif_read_file(
    int               fd,     ///< File descriptor to read from
    struct jfif_file *file,   ///< Struct to read into
    int               verbose ///< Print headers after reading them
);

/**
 * Read in a JFIF APP0 segment header from a file.
 *
 * @return 0 on success, otherwise 1.
 */
int
jfif_read_app0_header(
    uint8_t                        **file_data, ///< Buffer containing file data
    struct jfif_app0_marker_segment *header     ///< Struct to read into
);

/**
 * Skip a JFIF header.
 */
void
jfif_skip_header(
    uint8_t **file_data ///< Buffer containing file data
);

/**
 * Create a new JFIF file (in memory).
 */
void
jfif_new_file(
    uint16_t          image_width,  ///< Width of the image in pixels
    uint16_t          image_height, ///< Hegiht of the image in pixels
    struct jfif_file *file,         ///< JFIF struct to create
    int               verbose       ///< Print headers after creating them
);

/**
 * Write a JFIF file.
 *
 * @return 0 on success, otherwise 1.
 */
int
jfif_write_file(
    const char       *fname, ///< File name
    struct jfif_file *file   ///< JFIF file info
);

#endif // __JFIF_H__
