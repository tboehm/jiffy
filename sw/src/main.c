// main.c: Main file for the JFIF frontend.
//
// Copyright (c) 2021 Trey Boehm & Jordan Pamatmat
//
// This file is part of the JFIF encoder/decoder for EE 382N-4 at UT Austin
// with Dr. Mark McDermott.

#include <fcntl.h>  // open(), close()
#include <getopt.h> // getopt_long() and macros
#include <stdio.h>  // printf()
#include <string.h> // strdup()

extern int optind; // from <getopt.h>

#include "bmp.h"
#include "decode.h"
#include "encode.h"
#ifdef ACCELERATOR
#include "interface.h"
#include "qtz.h"
#endif // ACCELERATOR
#include "jfif.h"
#include "logging.h"
#include "profile.h"

#include <unistd.h> // sleep() REMOVE BEFORE COMMIT

static void
usage(const char *fname)
{
    fprintf(
        stderr,
        "Usage: %s [OPTIONS] <input file> [<output file>]\n\n"
        "Options:\n"
        "  -d, --decode    Decode a JFIF file to BMP\n"
        "  -e, --encode    Encode a BMP file to JFIF\n"
        "  -v, --verbose   Enable more verbose output\n"
        "  -h, --help      Print this message and exit\n"
        "\n"
        "The default output file name is the input file name with the proper extension.\n",
        fname
    );
}

static int
decode_jfif(int fd, const char *output_fname, int verbose)
{
    static struct jfif_file file;

    if (jfif_read_file(fd, &file, verbose)) {
        return 1;
    }

    profile_timestamp("Read JFIF data");

    uint32_t           image_width = file.sof0.samples_per_line;
    uint32_t           image_height = file.sof0.lines;
    struct bmp_header *bmp;
    struct dib_header *dib;
    uint8_t           *image_data;

    bmp_new_file(image_width, image_height, &bmp, &dib, &image_data);

    profile_timestamp("Create BMP in memory");

#ifdef ACCELERATOR
    if (interface_open()) {
        return 1;
    }

    profile_timestamp("Open HW interface");

    int mode = JIFFY_MODE_DECODE;
    int mcu_count = image_width * image_height / 64;

    int y_qtz_idx = file.sof0.components[Y_IDX].qt_select;
    int cb_qtz_idx = file.sof0.components[CB_IDX].qt_select;
    int cr_qtz_idx = file.sof0.components[CR_IDX].qt_select;

    interface_initialize_accelerator(
        mcu_count,
        mode,
        file.dqt[y_qtz_idx].elements,
        file.dqt[cb_qtz_idx].elements,
        file.dqt[cr_qtz_idx].elements
    );

    profile_timestamp("Initialize accelerator");
#endif // ACCELERATOR

    // Don't time this function with profile_timestamp() -- it calls it internally.
    if (decode_file(&file, image_data)) {
        return 1;
    }

#ifdef ACCELERATOR
    if (interface_close()) {
        return 1;
    }

    profile_timestamp("Close interface");
#endif // ACCELERATOR

    if (bmp_write_file(output_fname, bmp, dib, image_data)) {
        return 1;
    }

    profile_timestamp("Write BMP to disk");

    return 0;
}

static int
encode_jfif(int fd, const char *output_fname, int verbose)
{
    static struct bmp_file  bmpf;
    static struct jfif_file jfiff;

    if (bmp_read_file(fd, &bmpf, verbose)) {
        return 1;
    }

    profile_timestamp("Read BMP data");

    uint16_t image_width = (uint16_t)bmpf.dib.width;
    uint16_t image_height = (uint16_t)bmpf.dib.height;
    uint8_t *image_data = bmpf.data;

    jfif_new_file(image_width, image_height, &jfiff, verbose);

    for (unsigned i = 0; i < jfiff.dht_count; i++) {
        struct define_huffman_table *dht = &jfiff.dht[i];
        huffman_expand_table(dht, &dht->lut);
    }

    profile_timestamp("Create JFIF in memory");

#ifdef ACCELERATOR
    if (interface_open()) {
        return 1;
    }

    profile_timestamp("Open HW interface");

    int mode = JIFFY_MODE_ENCODE;
    int mcu_count = image_width * image_height / 64;

    int y_qtz_mat_idx = jfiff.sof0.components[Y_IDX].qt_select;
    int cb_qtz_mat_idx = jfiff.sof0.components[CB_IDX].qt_select;
    int cr_qtz_mat_idx = jfiff.sof0.components[CR_IDX].qt_select;

    interface_initialize_accelerator(
        mcu_count,
        mode,
        jfiff.dqt[y_qtz_mat_idx].elements,
        jfiff.dqt[cb_qtz_mat_idx].elements,
        jfiff.dqt[cr_qtz_mat_idx].elements
    );

    profile_timestamp("Initialize accelerator");
#endif // ACCELERATOR

    // Don't time this function with profile_timestamp() -- it calls it internally.
    if (encode_file(&jfiff, image_data)) {
        return 1;
    }

#ifdef ACCELERATOR
    if (interface_close()) {
        return 1;
    }

    profile_timestamp("Close interface");
#endif // ACCELERATOR

    jfif_write_file(output_fname, &jfiff);

    profile_timestamp("Write JFIF to disk");

    printf("output file = '%s'\n", output_fname);

    return 0;
}

int
main(int argc, char *argv[])
{
    profile_start_time();

    int rc = 0;

    char *input_fname = NULL;

    int opt_decode = 0;
    int opt_encode = 0;
    int opt_verbose = 0;

    static struct option jiffy_options[] = {
        { "decode",  no_argument,  0,  'd'        }, // decode JFIF -> BMP
        { "encode",  no_argument,  0,  'e'        }, // encode BMP -> JFIF
        { "verbose", no_argument,  0,  'v'        }, // print headers in jfif_read_file()
    };

    int option_index = 0;
    int c;

    while ((c = getopt_long(argc, argv, "devh", jiffy_options, &option_index)) != -1) {
        switch (c) {
        case 'd':
            opt_decode = 1;
            break;
        case 'e':
            opt_encode = 1;
            break;
        case 'v':
            opt_verbose = 1;
            break;
        case 'h':
            usage(argv[0]);
            return 0;
        case '?':
            break;
        default:
            printf("?? getopt returned character code 0%o ??\n", c);
        }
    }

    argc -= optind;

    if (argc < 1 || argc > 2) {
        usage(argv[0]);
        return 1;
    }

    if (opt_decode == opt_encode) {
        fprintf(stderr, "Must specify exactly one of [--decode|--encode]\n");
        return 1;
    }

#ifdef ACCELERATOR
    const char *jpeg_resource = "\033[35;1mhardware acceleration\033[0m";
#else
    const char *jpeg_resource = "\033[36;1msoftware\033[0m";
#endif
    printf("Welcome to Jiffy! You are using %s to process JPEG data.\n", jpeg_resource);
    printf("[If this is incorrect, please recompile with/without -DACCELERATOR.]\n");

    input_fname = argv[optind++];

    char *output_fname;

    if (argc == 2) {
        output_fname = argv[optind];
    } else {
        output_fname = strdup(input_fname);

        if (opt_decode) {
            char *extension = strstr(output_fname, ".jpg");
            if (!extension) {
                extension = strstr(output_fname, ".jpeg");
            }
            if (!extension) {
                fprintf(stderr, "Please supply a file name ending with '.jpg' or '.jpeg'\n");
                return 1;
            }
            strcpy(extension, ".bmp");
        } else {
            char *extension = strstr(output_fname, ".bmp");
            if (!extension) {
                fprintf(stderr, "Please supply a file name ending with '.bmp'\n");
                return 1;
            }
            strcpy(extension, ".jpg");
        }
    }

    int fd = open(input_fname, O_RDONLY);
    if (fd < 0) {
        log_perror("open");
        return 1;
    }

    profile_timestamp("Parse arguments");

    if (opt_decode) {
        rc = decode_jfif(fd, output_fname, opt_verbose);
    } else {
        rc = encode_jfif(fd, output_fname, opt_verbose);
    }

#if defined(ACCELERATOR) && (LOGGING_LEVEL >= LEVEL_TIMING)
    interface_print_interrupt_stats();
#endif

    return rc;
}
