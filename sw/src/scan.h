// scan.h: Scan data from the image.
//
// Copyright (c) 2021 Trey Boehm & Jordan Pamatmat
//
// This file is part of the JFIF encoder/decoder for EE 382N-4 at UT Austin
// with Dr. Mark McDermott.

#ifndef __SCAN_H__
#define __SCAN_H__

#include "jpeg.h"

/// Start-of-scan marker bytes.
#define SOS_MARKER 0xffda

/// Maximum number of components in a scan.
#define SCAN_COMPONENTS_MAX 3

/// Define-restart-interval marker
#define DRI_MARKER 0xffdd

/**
 * Scan component specification.
 */
struct scan_component {
    /// Scan component selector (Cs).
    uint8_t selector;
    /// DC entropy coding table selector.
    uint8_t dc_table : 4;
    /// AC entropy coding table selector.
    uint8_t ac_table : 4;
};

/**
 * Scan header (marker 0xffda).
 */
struct scan_header {
    /// Always 0xffda.
    uint16_t marker;
    /// Scan header length (Ls).
    uint16_t length;
    /// Number of components in scan (Ns).
    uint8_t n_components;
    /// Scan component specification
    struct scan_component components[SCAN_COMPONENTS_MAX];
    /// Start of spectral selection or predictor selection (Ss).
    uint8_t spectral_start;
    /// End of spectral selection (Se).
    uint8_t spectral_end;
    /// Successive approximation bit position high (Ah).
    uint8_t approx_hi : 4;
    /// Successive approximation bit position low or point transform (Al).
    uint8_t approx_lo : 4;
};

/**
 * Define restart interval (marker 0xffdd).
 */
struct define_restart_interval {
    /// Always 0xffdd.
    uint16_t marker;
    /// Always 4.
    uint16_t length;
    /// Restart interval in units of MCU blocks.
    uint16_t restart_interval;
};

/**
 * Print a scan header.
 */
void
scan_print_header(
    struct scan_header *shdr ///< Header to print
);

/**
 * Read a scan header.
 *
 * @return 0 on success, otherwise 1.
 */
int
scan_read_header(
    uint8_t           **file_data, ///< Buffer containing file data
    struct scan_header *shdr       ///< Header to read into
);

// Forward-declaration for the JFIF file struct.
struct jfif_file;

/**
 * Compute the component-to-DHT mapping.
 *
 * @return 0 on success, otherwise 1.
 */
int
scan_create_dht_mapping(
    struct jfif_file *file ///< JFIF file to modify
);

/**
 * Print a define-restart-interval header.
 */
void
scan_print_dri(
    struct define_restart_interval *dri ///< Header to print
);

/**
 * Read a define-restart-interval header.
 *
 * @return 0 on success, otherwise 1.
 */
int
scan_read_dri(
    uint8_t                       **file_data, ///< Buffer containing file data
    struct define_restart_interval *dri        ///< Header to read into
);
#endif // __SCAN_H__
