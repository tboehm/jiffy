// parse.h: Read bytes from a buffer and convert from big Endian to host.
//
// Copyright (c) 2021 Trey Boehm & Jordan Pamatmat
//
// Source for the endian shims: https://gist.github.com/yinyin/2027912#gistcomment-1489595
//
// This file is part of the JFIF encoder/decoder for EE 382N-4 at UT Austin
// with Dr. Mark McDermott.

#ifndef __PARSE_H__
#define __PARSE_H__

#include <stdint.h> // uint8_t

#ifdef __APPLE__
#include <libkern/OSByteOrder.h>
#define htobe16(x) OSSwapHostToBigInt16(x)
#define htole16(x) OSSwapHostToLittleInt16(x)
#define be16toh(x) OSSwapBigToHostInt16(x)
#define le16toh(x) OSSwapLittleToHostInt16(x)
#define htobe32(x) OSSwapHostToBigInt32(x)
#define htole32(x) OSSwapHostToLittleInt32(x)
#define be32toh(x) OSSwapBigToHostInt32(x)
#define le32toh(x) OSSwapLittleToHostInt32(x)
#define htobe64(x) OSSwapHostToBigInt64(x)
#define htole64(x) OSSwapHostToLittleInt64(x)
#define be64toh(x) OSSwapBigToHostInt64(x)
#define le64toh(x) OSSwapLittleToHostInt64(x)
#else // Not __APPLE__
#include <endian.h>
#endif // __APPLE__

#define NEXT_BYTE(buffer_ptr) **(uint8_t **)buffer_ptr; *buffer_ptr += 1
#define NEXT_2_BYTES(buffer_ptr) htobe16(**(uint16_t **)buffer_ptr); *buffer_ptr += 2
#define NEXT_4_BYTES(buffer_ptr) htobe32(**(uint32_t **)buffer_ptr); *buffer_ptr += 4

#define WRITE_BYTE(buffer_ptr, data) **(uint8_t **)buffer_ptr = data; *buffer_ptr += 1
#define WRITE_2_BYTES(buffer_ptr, data) **(uint16_t **)buffer_ptr = htobe16(data); *buffer_ptr += 2
#define WRITE_4_BYTES(buffer_ptr, data) **(uint32_t **)buffer_ptr = htobe32(data); *buffer_ptr += 4

#endif // __PARSE_H__
