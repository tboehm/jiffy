// dwn.c: Downsampling.
//
// Copyright (c) 2021 Trey Boehm & Jordan Pamatmat
//
// This file is part of the JFIF encoder/decoder for EE 382N-4 at UT Austin
// with Dr. Mark McDermott.

#include "dwn.h"

#include <stdio.h>

#include "jpeg.h"

void
dwn_downsample(const int block[MCU_DIM], int downsampled[MCU_DIM], enum DownsampleRatio ratio)
{
    int samples_per_row = DownsampleRatios[ratio].samples_per_row;
    int col_stride = DWN_REGION_W / samples_per_row;
    int changes_between_rows = DownsampleRatios[ratio].changes_between_rows;

    for (int channel = 0; channel < 3; channel++) {
        for (int row = 0; row < MCU_W; row += DWN_REGION_H) {
            for (int col = 0; col < MCU_W; col += DWN_REGION_W) {
                if (channel == 0) {
                    // Skip luma
                    downsampled[row][col][channel] = block[row][col][channel];
                    continue;
                }
                // Downsample this region
                for (int i = 0; i < DWN_REGION_H; i++) {
                    for (int j = 0; j < DWN_REGION_W; j++) {
                        int col_offs = (j / col_stride) * col_stride;
                        int row_offs = changes_between_rows == 0 ? 0 : i;
                        int new_value = block[row + row_offs][col + col_offs][channel];
                        downsampled[row + i][col + j][channel] = new_value;
                    }
                }
            }
        }
    }
}
