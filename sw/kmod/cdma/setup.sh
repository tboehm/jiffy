#!/bin/sh

KMOD=cdma_int
MAJOR=241

if [ $(id -u) -ne 0 ]; then
    echo "Must be root"
    exit 1
fi

# [Re]create the character device
DEV=/dev/$KMOD
[ -e $DEV ] && rm $DEV
/bin/mknod $DEV c $MAJOR 0

# If the module is already loaded: unload it.
lsmod | grep $KMOD >/dev/null && /sbin/rmmod $KMOD

# Load the module.
/sbin/insmod $KMOD.ko
