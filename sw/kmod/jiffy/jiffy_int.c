/* ===================================================================
 *  jiffy_int.c
 *
 *  AUTHOR:     Mark McDermott
 *  CREATED:    March 12, 2009
 *  UPDATED:    May 2, 2017     Updated for ZED Board
 *  UPDATED:    Feb 5, 2021     Updated for the ULTRA96
 *  UPDATED:    Apr 23, 2021    Updated for Lab 3 (Keccak)
 *  UPDATED:    May 14, 2021    Updated for project (Jiffy)
 *
 *  DESCRIPTION: This kernel module registers interrupts from the
 *               Keccak-512 accelerator and uses them to control
 *               the user-facing program's execution flow.
 *
 *  DEPENDENCIES: Works on the AVNET Ultra96 Board
 */

#include <asm/uaccess.h>
#include <linux/bitops.h>
#include <linux/clk.h>
#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/gpio/driver.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/io.h>
#include <linux/ioport.h>
#include <linux/kernel.h>
#include <linux/mm.h>
#include <linux/mman.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/version.h>
#include <linux/vmalloc.h>

#include <linux/of.h>
#include <linux/pm_runtime.h>

#define MODULE_VER  "1.0"

#define JIFFY_MAJOR 239 // Need to mknod /dev/jiffy_int c 239 0
#define MODULE_NM   "jiffy_int"

#define DEBUG
// #undef DEBUG

#define MSG_BUF_SIZE 256

int          interruptcount = 0;
int          temp = 0;
int          len = 0;
char        *msg = NULL;
unsigned int gic_interrupt;

static struct fasync_struct *fasync_jiffy_queue;

/* ===================================================================
 * function: jiffy_int_handler
 *
 * This function counts interrupts from the Jiffy unit and passes
 * them along to interested processes.
 */
irq_handler_t
jiffy_int_handler(int irq, void *dev_id, struct pt_regs *regs)
{
    interruptcount++;

#ifdef DEBUG
    printk(KERN_INFO "jiffy_int: Interrupt detected in kernel\n"); // DEBUG
#endif

    /* Signal the user application that an interupt occured */
    kill_fasync(&fasync_jiffy_queue, SIGUSR1, POLL_IN);

#ifdef DEBUG
    printk(KERN_INFO "jiffy_int: Signaled application\n");
#endif

    return (irq_handler_t)IRQ_HANDLED;
}

static struct proc_dir_entry *proc_jiffy_int;

/* ===================================================================
 *    function: read_proc   --- Example code
 */
ssize_t
read_proc(struct file *filp, char *buf, size_t count, loff_t *offp)
{
    if (count > temp) {
        count = temp;
    }

    temp = temp - count;

    if (count == 0) {
        temp = len;
    }
    printk("read_proc count value = %ld\n", count);

    return count;
}

/* ===================================================================
 *    function: write_proc   --- Example code
 */
ssize_t
write_proc(struct file *filp, const char *buf, size_t count, loff_t *offp)
{
    len = count;
    temp = len;

    printk("write_proc count value = %ld\n", count);

    return count;
}

/* ===================================================================
 * function: jiffy_open
 *
 * This function is called when the jiffy_int device is opened
 */
static int
jiffy_open(struct inode *inode, struct file *file)
{
#ifdef DEBUG
    printk(KERN_INFO "jiffy_int: Inside jiffy_open\n"); // DEBUG
#endif
    return 0;
}

/* ===================================================================
 * function: jiffy_release
 *
 * This function is called when the jiffy_int device is
 * released
 */
static int
jiffy_release(struct inode *inode, struct file *file)
{
#ifdef DEBUG
    printk(KERN_INFO "jiffy_int: Inside jiffy_release\n"); // DEBUG
#endif
    return 0;
}

/* ===================================================================
 * function: jiffy_fasync
 *
 * This is invoked by the kernel when the user program opens this
 * input device and issues fcntl(F_SETFL) on the associated file
 * descriptor. fasync_helper() ensures that if the driver issues a
 * kill_fasync(), a SIGUSR1 is dispatched to the owning application.
 */
static int
jiffy_fasync(int fd, struct file *filp, int on)
{
#ifdef DEBUG
    printk(KERN_INFO "jiffy_int: Inside jiffy_fasync\n"); // DEBUG
#endif

    return fasync_helper(fd, filp, on, &fasync_jiffy_queue);
};

/* ===================================================================
 *
 *  Define which file operations are supported
 *
 */

struct file_operations jiffy_fops = {
    .owner          = THIS_MODULE,
    .llseek         = NULL,
    .read           = NULL,
    .write          = NULL,
    .poll           = NULL,
    .unlocked_ioctl = NULL,
    .mmap           = NULL,
    .open           = jiffy_open,
    .flush          = NULL,
    .release        = jiffy_release,
    .fsync          = NULL,
    .fasync         = jiffy_fasync,
    .lock           = NULL,
    .read           = NULL,
    .write          = NULL,
};

struct file_operations proc_fops = {
    read : read_proc,
    write : write_proc
};

static const struct of_device_id zynq_jiffy_of_match[] = {
    { .compatible = "xlnx,accelerator-Lab-3" },
    { /* end of table */ }
};

MODULE_DEVICE_TABLE(of, zynq_jiffy_of_match);

/* ===================================================================
 *
 * zynq_jiffy_probe - Initialization method for a zynq_jiffy device
 *
 * Return: 0 on success, negative error otherwise.
 */
static int
zynq_jiffy_probe(struct platform_device *pdev)
{
    struct resource *res;

    printk("In probe funtion\n");

    // This code gets the IRQ number by probing the system.

    res = platform_get_resource(pdev, IORESOURCE_IRQ, 0);

    if (!res) {
        printk("No IRQ found\n");
        return -1;
    }

    // Get the IRQ number
    gic_interrupt = res->start;

    printk("Probe IRQ # = %lld\n", res->start);

    return 0;
}

/* ===================================================================
 *
 * zynq_jiffy_remove - Driver removal function
 *
 * Return: 0 always
 */
static int
zynq_jiffy_remove(struct platform_device *pdev)
{
    //struct zynq_jiffy *jiffy = platform_get_drvdata(pdev)

    return 0;
}

static struct platform_driver zynq_jiffy_driver = {
    .driver             = {
        .name           = MODULE_NM,
        .of_match_table = zynq_jiffy_of_match,
    },
    .probe              = zynq_jiffy_probe,
    .remove             = zynq_jiffy_remove,
};

/* ===================================================================
 * function: init_jiffy_int
 *
 * This function creates the /proc directory entry jiffy_int.
 */
static int __init
init_jiffy_int(void)
{
    int rv = 0;
    int err = 0;

    //platform_driver_unregister(&zynq_jiffy_driver);


    printk("ZED Interrupt Module\n");
    printk("ZED Interrupt Driver Loading.\n");
    printk("Using Major Number %d on %s\n", JIFFY_MAJOR, MODULE_NM);

    err = platform_driver_register(&zynq_jiffy_driver);

    if (err != 0) {
        printk("Driver register error with number %d\n", err);
    } else {
        printk("Driver registered with no error\n");
    }

    if (register_chrdev(JIFFY_MAJOR, MODULE_NM, &jiffy_fops)) {
        printk("jiffy_int: unable to get major %d. ABORTING!\n", JIFFY_MAJOR);
        goto no_jiffy_interrupt;
    }

    proc_jiffy_int = proc_create(MODULE_NM, 0444, NULL, &proc_fops);
    msg = kmalloc(MSG_BUF_SIZE * sizeof(char), GFP_KERNEL);

    if (proc_jiffy_int == NULL) {
        printk("jiffy_int: create /proc entry returned NULL. ABORTING!\n");
        goto no_jiffy_interrupt;
    }

    // Request interrupt

    printk(
        "Requesting IRQ with gic_interrupt=%u, handler=%p, flags=0x%x",
        gic_interrupt,
        (void *)jiffy_int_handler,
        IRQF_TRIGGER_RISING
    );

    rv = request_irq(
        gic_interrupt,
        (irq_handler_t)jiffy_int_handler,
        IRQF_TRIGGER_RISING,
        MODULE_NM,
        NULL
    );

    if (rv) {
        printk("Can't get interrupt %d (rv = %d)\n", gic_interrupt, rv);
        goto no_jiffy_interrupt;
    }

    printk(KERN_INFO "%s %s Initialized\n", MODULE_NM, MODULE_VER);

    return 0;

    // remove the proc entry on error

no_jiffy_interrupt:
    free_irq(gic_interrupt, NULL);             // Release IRQ
    unregister_chrdev(JIFFY_MAJOR, MODULE_NM); // Release character device
    platform_driver_unregister(&zynq_jiffy_driver);
    remove_proc_entry(MODULE_NM, NULL);
    return -EBUSY;
};

/* ===================================================================
 * function: cleanup_jiffy_interrupt
 *
 * This function frees interrupt then removes the /proc directory entry
 * jiffy_interrupt.
 */
static void __exit
cleanup_jiffy_interrupt(void)
{
    free_irq(gic_interrupt, NULL);                  // Release IRQ
    unregister_chrdev(JIFFY_MAJOR, MODULE_NM);      // Release character device
    platform_driver_unregister(&zynq_jiffy_driver); // Unregister the driver
    remove_proc_entry(MODULE_NM, NULL);             // Remove process entry
    if (msg) {
        kfree(msg);
    }
    printk(KERN_INFO "%s %s removed\n", MODULE_NM, MODULE_VER);
}

module_init(init_jiffy_int);
module_exit(cleanup_jiffy_interrupt);

MODULE_AUTHOR("Mark McDermott");
MODULE_AUTHOR("Trey Boehm");
MODULE_DESCRIPTION("jiffy_int proc module");
MODULE_LICENSE("GPL");
