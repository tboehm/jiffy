// test_util.c: Testing helper functions and macros.
//
// Copyright (c) 2021 Trey Boehm & Jordan Pamatmat
//
// This file is part of the JFIF encoder/decoder for EE 382N-4 at UT Austin
// with Dr. Mark McDermott.

#include "test_util.h"

#include <math.h>   // fabs()
#include <stdio.h>  // fprintf(), stderr
#include <stdlib.h> // abs()

#include "jpeg.h"

/**
 * Verify the contents of a block of integers against a reference value.
 *
 * @return 0 if the blocks match, otherwise 1.
 */
int
test_verify_int_block(
    const int actual[MCU_DIM],
    const int expected[MCU_DIM],
    int       tolerance,
    int       channels
)
{
    int rc = 0;

    for (int i = 0; i < MCU_W; i++) {
        for (int j = 0; j < MCU_W; j++) {
            for (int c = 0; c < 3; c++) {
                if (((1 << c) & channels) == 0) {
                    continue;
                }
                int out = actual[i][j][c];
                int ref = expected[i][j][c];
                int diff = out - ref;
                if (abs(diff) > tolerance) {
                    fprintf(stderr, "Mismatch at [%d][%d][%d]: %d != %d\n", i, j, c, out, ref);
                    rc = 1;
                }
            }
        }
    }

    return rc;
}

/**
 * Verify the contents of a block of floats against a reference value.
 *
 * @return 0 if the blocks match, otherwise 1.
 */
int
test_verify_float_block(
    const float actual[MCU_DIM],
    const float expected[MCU_DIM],
    float       tolerance,
    int         channels
)
{
    int rc = 0;

    for (int i = 0; i < MCU_W; i++) {
        for (int j = 0; j < MCU_W; j++) {
            for (int c = 0; c < 3; c++) {
                if (((1 << c) & channels) == 0) {
                    continue;
                }
                float out = actual[i][j][c];
                float ref = expected[i][j][c];
                float diff = out - ref;
                if (fabs(diff) > tolerance) {
                    fprintf(stderr, "Mismatch at [%d][%d][%d]: %f != %f\n", i, j, c, out, ref);
                    rc = 1;
                }
            }
        }
    }

    return rc;
}

/**
 * Print the contents of a block of ints.
 */
void
test_print_int_block(const int block[MCU_DIM], int channels)
{
    int multiple_channels = channels != 1 && channels != 2 && channels != 4;

    for (int i = 0; i < MCU_W; i++) {
        printf("  { ");
        for (int j = 0; j < MCU_W; j++) {
            if (multiple_channels) {
                printf("{ ");
            }
            for (int c = 0; c < 3; c++) {
                if (((1 << c) & channels) == 0) {
                    continue;
                }
                printf("%3d ", block[i][j][c]);
            }
            if (multiple_channels) {
                printf("} ");
            }
        }
        printf("}\n");
    }
}

/**
 * Print the contents of a block of floats..
 */
void
test_print_float_block(const float block[MCU_DIM], int channels)
{
    int multiple_channels = channels != 1 && channels != 2 && channels != 4;

    for (int i = 0; i < MCU_W; i++) {
        printf("  { ");
        for (int j = 0; j < MCU_W; j++) {
            if (multiple_channels) {
                printf("{ ");
            }
            for (int c = 0; c < 3; c++) {
                if (((1 << c) & channels) == 0) {
                    continue;
                }
                printf("%7.2f ", block[i][j][c]);
            }
            if (multiple_channels) {
                printf("} ");
            }
        }
        printf("}\n");
    }
}
