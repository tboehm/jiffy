#include <stdio.h>    // fprintf()
#include <stdlib.h>   // calloc()

#include "csx.h"
#include "dct.h"
#include "decode.h"
#include "encode.h"
#include "huffman.h"
#include "jfif.h"
#include "jpeg.h"
#include "qtz.h"

#include "test_util.h"

static int rgb_array[MCU_DIM] = {
    { {255, 255, 255}, {254, 0, 0}, {255, 255, 255}, {254, 0, 0}, {255, 255, 255}, {254, 0, 0}, {255, 255, 255}, {254, 0, 0} },
    { {255, 255, 255}, {254, 0, 0}, {255, 255, 255}, {254, 0, 0}, {255, 255, 255}, {254, 0, 0}, {255, 255, 255}, {254, 0, 0} },
    { {255, 255, 255}, {254, 0, 0}, {255, 255, 255}, {254, 0, 0}, {255, 255, 255}, {254, 0, 0}, {255, 255, 255}, {254, 0, 0} },
    { {255, 255, 255}, {254, 0, 0}, {255, 255, 255}, {254, 0, 0}, {255, 255, 255}, {254, 0, 0}, {255, 255, 255}, {254, 0, 0} },
    { {255, 255, 255}, {254, 0, 0}, {255, 255, 255}, {254, 0, 0}, {255, 255, 255}, {254, 0, 0}, {255, 255, 255}, {254, 0, 0} },
    { {255, 255, 255}, {254, 0, 0}, {255, 255, 255}, {254, 0, 0}, {255, 255, 255}, {254, 0, 0}, {255, 255, 255}, {254, 0, 0} },
    { {255, 255, 255}, {254, 0, 0}, {255, 255, 255}, {254, 0, 0}, {255, 255, 255}, {254, 0, 0}, {255, 255, 255}, {254, 0, 0} },
    { {255, 255, 255}, {254, 0, 0}, {255, 255, 255}, {254, 0, 0}, {255, 255, 255}, {254, 0, 0}, {255, 255, 255}, {254, 0, 0} },
};

static int expected_output[MCU_DIM] = {
    { { 300, -172, 508 }, { 129, 31, -92 }, { 0, 0, 0 }, { 152, 37, -108 }, { 0, 0, 0 }, { 228, 55, -162 }, { 0, 0, 0 }, { 649, 156, -460 } },
    { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } },
    { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } },
    { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } },
    { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } },
    { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } },
    { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } },
    { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } },
};

static int csx_output[MCU_DIM] = { 0 };
static float dct_output[MCU_DIM] = { 0 };
static int qtz_output[MCU_DIM] = { 0 };

int
main(void)
{
    int rc = 0;

    // All three channels.
    int channel_mask = 0x7;

    // First, just ensure csx/dct/qtz all work as expected.

    // 1. Colorspace transformation.
    csx_rgb_to_ycbcr(rgb_array, csx_output);

    // 2. Discrete cosine transform.
    dct_shift(csx_output, csx_output, -128);
    dct_clamp(csx_output, csx_output, -128, 127);
    dct_transform(csx_output, dct_output);

    // 3. Quantization.
    qtz_quantize(dct_output, QtzMat100, QtzMat100, qtz_output);

    rc = test_verify_int_block(qtz_output, expected_output, 1, channel_mask);
    if (rc) {
        fprintf(stderr, "\033[31;1mFailed to convert RGB array\033[0m\n");
    } else {
        fprintf(stderr, "\033[32;1mSuccessfully converted RGB array\033[0m\n");
    }

    // Now, actually test the full process (i.e. add Huffman encoding).

    struct jfif_file file;
    jfif_new_file(8, 8, &file, 0);
    file.data = (uint8_t *)calloc(1024, sizeof(uint8_t));

    for (unsigned i = 0; i < file.dht_count; i++) {
        struct define_huffman_table *dht = &file.dht[i];
        huffman_expand_table(dht, &dht->lut);
    }

    int prev_dc[3] = { 0 };

    encode_mcu(rgb_array, prev_dc, &file);

    uint8_t *decoded_data = (uint8_t *)calloc(1024, sizeof(uint8_t));

    decode_file(&file, decoded_data);

    return rc;
}
