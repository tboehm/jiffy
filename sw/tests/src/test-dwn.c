#include <stdio.h> // fprintf

#include "dwn.h"
#include "jpeg.h"

#include "test_util.h"

static int Input[MCU_DIM] = {
    { { 0,  0,  0  }, { 1,  1,  1  }, { 2,  2,  2  }, { 3,  3,  3  },
        { 4, 4, 4 },{ 5,  5,  5  }, { 6,  6,  6  }, { 7,  7,  7  } },
    { { 8,  8,  8  }, { 9,  9,  9  }, { 10, 10, 10 }, { 11, 11, 11 },
        { 12, 12, 12 },{ 13, 13, 13 }, { 14, 14, 14 }, { 15, 15, 15 } },
    { { 16, 16, 16 }, { 17, 17, 17 }, { 18, 18, 18 }, { 19, 19, 19 },
        { 20, 20, 20 },{ 21, 21, 21 }, { 22, 22, 22 }, { 23, 23, 23 } },
    { { 24, 24, 24 }, { 25, 25, 25 }, { 26, 26, 26 }, { 27, 27, 27 },
        { 28, 28, 28 },{ 29, 29, 29 }, { 30, 30, 30 }, { 31, 31, 31 } },
    { { 32, 32, 32 }, { 33, 33, 33 }, { 34, 34, 34 }, { 35, 35, 35 },
        { 36, 36, 36 },{ 37, 37, 37 }, { 38, 38, 38 }, { 39, 39, 39 } },
    { { 40, 40, 40 }, { 41, 41, 41 }, { 42, 42, 42 }, { 43, 43, 43 },
        { 44, 44, 44 },{ 45, 45, 45 }, { 46, 46, 46 }, { 47, 47, 47 } },
    { { 48, 48, 48 }, { 49, 49, 49 }, { 50, 50, 50 }, { 51, 51, 51 },
        { 52, 52, 52 },{ 53, 53, 53 }, { 54, 54, 54 }, { 55, 55, 55 } },
    { { 56, 56, 56 }, { 57, 57, 57 }, { 58, 58, 58 }, { 59, 59, 59 }, { 60, 60, 60 },
        { 61, 61, 61 },{ 62, 62, 62 }, { 63, 63, 63 } }
};

static int Output[MCU_DIM] = { 0 };

static int Expected[DWN_COUNT][MCU_DIM] = {
    [DWN_411] = {
                { { 0  }, { 0  }, { 0  }, { 0  }, { 4  }, { 4  }, { 4  }, { 4  } },
                { { 8  }, { 8  }, { 8  }, { 8  }, { 12 }, { 12 }, { 12 }, { 12 } },
                { { 16 }, { 16 }, { 16 }, { 16 }, { 20 }, { 20 }, { 20 }, { 20 } },
                { { 24 }, { 24 }, { 24 }, { 24 }, { 28 }, { 28 }, { 28 }, { 28 } },
                { { 32 }, { 32 }, { 32 }, { 32 }, { 36 }, { 36 }, { 36 }, { 36 } },
                { { 40 }, { 40 }, { 40 }, { 40 }, { 44 }, { 44 }, { 44 }, { 44 } },
                { { 48 }, { 48 }, { 48 }, { 48 }, { 52 }, { 52 }, { 52 }, { 52 } },
                { { 56 }, { 56 }, { 56 }, { 56 }, { 60 }, { 60 }, { 60 }, { 60 } } },

    [DWN_420] = {
                { { 0  }, { 0  }, { 2  }, { 2  }, { 4  }, { 4  }, { 6  }, { 6  } },
                { { 0  }, { 0  }, { 2  }, { 2  }, { 4  }, { 4  }, { 6  }, { 6  } },
                { { 16 }, { 16 }, { 18 }, { 18 }, { 20 }, { 20 }, { 22 }, { 22 } },
                { { 16 }, { 16 }, { 18 }, { 18 }, { 20 }, { 20 }, { 22 }, { 22 } },
                { { 32 }, { 32 }, { 34 }, { 34 }, { 36 }, { 36 }, { 38 }, { 38 } },
                { { 32 }, { 32 }, { 34 }, { 34 }, { 36 }, { 36 }, { 38 }, { 38 } },
                { { 48 }, { 48 }, { 50 }, { 50 }, { 52 }, { 52 }, { 54 }, { 54 } },
                { { 48 }, { 48 }, { 50 }, { 50 }, { 52 }, { 52 }, { 54 }, { 54 } } },

    [DWN_422] = {
                { { 0  }, { 0  }, { 2  }, { 2  }, { 4  }, { 4  }, { 6  }, { 6  } },
                { { 8  }, { 8  }, { 10 }, { 10 }, { 12 }, { 12 }, { 14 }, { 14 } },
                { { 16 }, { 16 }, { 18 }, { 18 }, { 20 }, { 20 }, { 22 }, { 22 } },
                { { 24 }, { 24 }, { 26 }, { 26 }, { 28 }, { 28 }, { 30 }, { 30 } },
                { { 32 }, { 32 }, { 34 }, { 34 }, { 36 }, { 36 }, { 38 }, { 38 } },
                { { 40 }, { 40 }, { 42 }, { 42 }, { 44 }, { 44 }, { 46 }, { 46 } },
                { { 48 }, { 48 }, { 50 }, { 50 }, { 52 }, { 52 }, { 54 }, { 54 } },
                { { 56 }, { 56 }, { 58 }, { 58 }, { 60 }, { 60 }, { 62 }, { 62 } } },

    [DWN_444] = {
                { { 0  }, { 1  }, { 2  }, { 3  }, { 4  }, { 5  }, { 6  }, { 7  } },
                { { 8  }, { 9  }, { 10 }, { 11 }, { 12 }, { 13 }, { 14 }, { 15 } },
                { { 16 }, { 17 }, { 18 }, { 19 }, { 20 }, { 21 }, { 22 }, { 23 } },
                { { 24 }, { 25 }, { 26 }, { 27 }, { 28 }, { 29 }, { 30 }, { 31 } },
                { { 32 }, { 33 }, { 34 }, { 35 }, { 36 }, { 37 }, { 38 }, { 39 } },
                { { 40 }, { 41 }, { 42 }, { 43 }, { 44 }, { 45 }, { 46 }, { 47 } },
                { { 48 }, { 49 }, { 50 }, { 51 }, { 52 }, { 53 }, { 54 }, { 55 } },
                { { 56 }, { 57 }, { 58 }, { 59 }, { 60 }, { 61 }, { 62 }, { 63 } } },

    [DWN_440] = {
                { { 0  }, { 1  }, { 2  }, { 3  }, { 4  }, { 5  }, { 6  }, { 7  } },
                { { 0  }, { 1  }, { 2  }, { 3  }, { 4  }, { 5  }, { 6  }, { 7  } },
                { { 16 }, { 17 }, { 18 }, { 19 }, { 20 }, { 21 }, { 22 }, { 23 } },
                { { 16 }, { 17 }, { 18 }, { 19 }, { 20 }, { 21 }, { 22 }, { 23 } },
                { { 32 }, { 33 }, { 34 }, { 35 }, { 36 }, { 37 }, { 38 }, { 39 } },
                { { 32 }, { 33 }, { 34 }, { 35 }, { 36 }, { 37 }, { 38 }, { 39 } },
                { { 48 }, { 49 }, { 50 }, { 51 }, { 52 }, { 53 }, { 54 }, { 55 } },
                { { 48 }, { 49 }, { 50 }, { 51 }, { 52 }, { 53 }, { 54 }, { 55 } } },
};

int
main(void)
{
    int rc = 0;

    // Only verify channel 1.
    int channels = 0;
    int tolerance = 0;

    for (enum DownsampleRatio ratio = 0; ratio < DWN_COUNT; ratio++) {
        dwn_downsample(Input, Output, ratio);

        int ratio_rc = test_verify_int_block(Output, Expected[ratio], tolerance, channels);
        if (ratio_rc) {
            const struct downsample_ratio *dr = &DownsampleRatios[ratio];
            printf("4%d%d: failed.", dr->samples_per_row, dr->changes_between_rows);
            rc = 1;
        }
    }

    return rc;
}
