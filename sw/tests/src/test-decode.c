#include <math.h>  // fabs()
#include <stdio.h> // fprintf()

#include "jpeg.h"
#include "csx.h"
#include "dct.h"
#include "dwn.h"
#include "qtz.h"

#include "test_util.h"

// Expected:
//  Y:    300  129  0   152  0   228  0   649
//  Cb:  -172   31  0    37  0    55  0   156
//  Cr:   508  -92  0  -108  0  -162  0  -460

// Actual:
//  Y:    300  129  0   152  0   228  0  649
//  Cb:  -172   31  0    37  0    55  0  156
//  Cr:   508   35  0    19  0    93  0   51


static int dct_array[MCU_DIM] = {
    { {300, -172, 508}, {129,  31, -92}, {0,   0,   0}, {152,  37, -108}, {0,   0,   0}, {228,  55, -162}, {0,   0,   0}, {649, 156, -460} },
    { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} },
    { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} },
    { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} },
    { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} },
    { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} },
    { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} },
    { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} },
};

static int qtz_output[MCU_DIM] = { 0 };
static int idct_output[MCU_DIM] = { 0 };
static int csx_output[MCU_DIM] = { 0 };

int
main(void)
{
    int rc = 0;

    // Print all channels.
    int channel_mask = 0x7;

    printf("Initial DCT:\n");
    test_print_int_block(dct_array, channel_mask);

    // Reverse-quantization
    qtz_dequantize(dct_array, QtzMat100, QtzMat100, qtz_output);
    printf("De-quantized:\n");
    test_print_int_block(qtz_output, channel_mask);

    // Inverse DCT
    dct_inverse_transform(qtz_output, idct_output);
    dct_shift(idct_output, idct_output, 128);
    dct_clamp(idct_output, idct_output, 0, 255);
    printf("Inverse DCT:\n");
    test_print_int_block(idct_output, channel_mask);

    // Colorspace transformation
    csx_ycbcr_to_rgb(idct_output, csx_output);
    printf("YCbCr -> RGB:\n");
    test_print_int_block(csx_output, channel_mask);

    return rc;
}
