// test_util.h: Testing helper functions and macros.
//
// Copyright (c) 2021 Trey Boehm & Jordan Pamatmat
//
// This file is part of the JFIF encoder/decoder for EE 382N-4 at UT Austin
// with Dr. Mark McDermott.

#ifndef __TEST_UTIL_H__
#define __TEST_UTIL_H__

#include "jpeg.h"

/**
 * Verify the contents of a block of integers against a reference value.
 *
 * @return 0 if the blocks match, otherwise 1.
 */
int
test_verify_int_block(
    const int actual[MCU_DIM],   ///< Output from a function
    const int expected[MCU_DIM], ///< Expected values
    int       tolerance,         ///< Acceptable difference between values
    int       channels           ///< Bit mask of channels to verify
);

/**
 * Verify the contents of a block of floats against a reference value.
 *
 * @return 0 if the blocks match, otherwise 1.
 */
int
test_verify_float_block(
    const float actual[MCU_DIM],   ///< Output from a function
    const float expected[MCU_DIM], ///< Expected values
    float       tolerance,         ///< Acceptable difference between values
    int         channels           ///< Bit mask of channels to verify
);


/**
 * Print the contents of a block of ints.
 */
void
test_print_int_block(
    const int block[MCU_DIM], ///< Block to print
    int       channels        ///< Bit mask of channels to print
);

/**
 * Print the contents of a block of floats..
 */
void
test_print_float_block(
    const float block[MCU_DIM], ///< Block to print
    int         channels        ///< Bit mask of channels to print
);

#endif // __TEST_UTIL_H__
