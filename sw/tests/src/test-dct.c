#include <math.h>  // fabs()
#include <stdio.h> // fprintf()

#include "dct.h"
#include "jpeg.h"

#include "test_util.h"

static int Block[MCU_DIM] = {
    { { 52 }, { 55 }, { 61 }, { 66  }, { 70  }, { 61  }, { 64 }, { 73 } },
    { { 63 }, { 59 }, { 55 }, { 90  }, { 109 }, { 85  }, { 69 }, { 72 } },
    { { 62 }, { 59 }, { 68 }, { 113 }, { 144 }, { 104 }, { 66 }, { 73 } },
    { { 63 }, { 58 }, { 71 }, { 122 }, { 154 }, { 106 }, { 70 }, { 69 } },
    { { 67 }, { 61 }, { 68 }, { 104 }, { 126 }, { 88  }, { 68 }, { 70 } },
    { { 79 }, { 65 }, { 60 }, { 70  }, { 77  }, { 68  }, { 58 }, { 75 } },
    { { 85 }, { 71 }, { 64 }, { 59  }, { 55  }, { 61  }, { 65 }, { 83 } },
    { { 87 }, { 79 }, { 69 }, { 68  }, { 65  }, { 76  }, { 78 }, { 94 } },
};

static float Transformed[MCU_DIM] = { 0 };

static const float Expected[MCU_DIM] = {
    { { -415.38f }, { -30.19f }, { -61.20f }, { 27.24f  }, { 56.12f  }, { -20.10f }, { -2.39f }, { 0.46f  } },
    { { 4.47f    }, { -21.86f }, { -60.76f }, { 10.25f  }, { 13.15f  }, { -7.09f  }, { -8.54f }, { 4.88f  } },
    { { -46.83f  }, { 7.37f   }, { 77.13f  }, { -24.56f }, { -28.91f }, { 9.93f   }, { 5.42f  }, { -5.65f } },
    { { -48.53f  }, { 12.07f  }, { 34.10f  }, { -14.76f }, { -10.24f }, { 6.30f   }, { 1.83f  }, { 1.95f  } },
    { { 12.12f   }, { -6.55f  }, { -13.20f }, { -3.95f  }, { -1.88f  }, { 1.75f   }, { -2.79f }, { 3.14f  } },
    { { -7.73f   }, { 2.91f   }, { 2.38f   }, { -5.94f  }, { -2.38f  }, { 0.94f   }, { 4.30f  }, { 1.85f  } },
    { { -1.03f   }, { 0.18f   }, { 0.42f   }, { -2.42f  }, { -0.88f  }, { -3.02f  }, { 4.12f  }, { -0.66f } },
    { { -0.17f   }, { 0.14f   }, { -1.07f  }, { -4.19f  }, { -1.17f  }, { -0.10f  }, { 0.50f  }, { 1.68f  } },
};

static int Rounded[MCU_DIM] = { 0 };

static int DeTransformed[MCU_DIM] = { 0 };

int
main(void)
{
    int rc = 0;

    // Only verify the first channel.
    int channel_mask = 0x1;
    float tolerance = 0.01f;
    int inv_dct_tolerance = 1;

    // First, make sure the regular DCT works.
    dct_shift(Block, Block, -128);

    dct_transform(Block, Transformed);

    rc = test_verify_float_block(Transformed, Expected, tolerance, channel_mask);
    if (rc) {
        return rc;
    }

    // If regular DCT works, check that inverse does, too.
    for (int i = 0; i < MCU_W; i++) {
        for (int j = 0; j < MCU_W; j++) {
            Rounded[i][j][0] = (int)lroundf(Transformed[i][j][0]);
        }
    }

    dct_inverse_transform(Rounded, DeTransformed);

    // Skip the shift since the original Block is already shifted.
    rc = test_verify_int_block(Block, DeTransformed, inv_dct_tolerance, channel_mask);

    return rc;
}
