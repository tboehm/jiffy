#include <stdio.h> // fprintf()
#include <math.h> // fabs()

#include "csx.h"
#include "jpeg.h"

// csx_array.h is auto-generated and only defines rgb_array[MCU_DIM]. It must be
// included after jpeg.h.
#include "csx_array.h"

#include "test_util.h"

static int ycbcr_array[MCU_DIM] = { 0 };

static int rgb_array_reverted[MCU_DIM] = { 0 };

int
main(void)
{
    int rc = 0;

    // Verify all 3 channels. Allow some tolerance since we have int -> float -> int with rounding.
    int channel_mask = 0x7;
    int tolerance = 1;

    csx_rgb_to_ycbcr(rgb_array, ycbcr_array);

    csx_ycbcr_to_rgb(ycbcr_array, rgb_array_reverted);

    rc = test_verify_int_block(rgb_array_reverted, rgb_array, tolerance, channel_mask);

    return rc;
}
