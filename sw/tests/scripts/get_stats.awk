#!/usr/bin/awk -f

BEGIN {
    FS = ":"
    header = "Parse,Read,Create,Open,Initialize,Math,Huffman,Close,Write,Min,Max,Mean,Stddev,Count"
    csv_line = ""
    print(header)
}

/^(Minimum|Maximum|Average) latency:/ {
    csv_line = csv_line $2 ","
}

/^(Number of samples):/ {
    csv_line = csv_line $2 ","
}

/(Parse arguments|Read|Create|Open|Initialize|Math|Huffman|Close|Write)/ {
    csv_line = csv_line $2 ","
}

/^(Interrupts detected):/ {
    csv_line = csv_line $2
    print(csv_line)
    csv_line = ""
}

