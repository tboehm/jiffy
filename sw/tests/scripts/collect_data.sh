#!/usr/bin/env bash

SAMPLES=20

# IMAGES=( images/swiz8.jpg images/swiz16.jpg images/swiz128.jpg images/swiz1200.jpg )
IMAGES=( bmps/swiz8.bmp bmps/swiz16.bmp bmps/swiz128.bmp bmps/swiz1200.bmp )

for image in ${IMAGES[*]}; do
    for i in $(seq 0 $SAMPLES); do
        # sudo ./jiffy -d $image 2>&1
        sudo ./jiffy -e $image 2>&1
    done
done | tee results.txt
