#!/usr/bin/env python3

import numpy as np
import sys

try:
    fname = sys.argv[1]
except IndexError:
    print(f"Usage: {sys.argv[0]} <filename> [<seed>]")
    exit(1)

try:
    seed = int(sys.argv[2])
except IndexError:
    seed = 2112

np.random.seed(seed)

rgb_array = 'static const int rgb_array[MCU_DIM] = {\n'

for i in range(8):
    rgb_array += '    { '
    for j in range(8):
        rgb_array += ' { '
        for c in range(3):
            value = np.random.randint(0, 256)
            rgb_array += f'{value}'
            rgb_array += ', ' if c < 2 else ' '
        rgb_array += '}'
        rgb_array += ', ' if j < 7 else ' '
    rgb_array += '}'
    rgb_array += ',\n' if i < 7 else '\n'
rgb_array += '};\n'

with open(fname, 'w') as fp:
    fp.write(rgb_array)
